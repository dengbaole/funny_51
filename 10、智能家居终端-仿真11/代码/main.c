#include<reg52.h> //包含头文件，一般情况不需要改动，头文件包含特殊功能寄存器的定义
#include<stdio.h>
#include "18b20.h"
#include "18b20a.h"
#include "lcd1602.h"
#include "delay.h"

bit ReadTempFlag;//定义读时间标志

sbit A1 = P0 ^ 0; //定义步进电机连接端口
sbit B1 = P0 ^ 1;
sbit C1 = P0 ^ 2;
sbit D1 = P0 ^ 3;

sbit window_check = P2 ^ 6; //窗户检查
sbit light_check = P2 ^ 7;  //灯 检查
sbit window = P3 ^ 4;    //窗户按键
sbit pan = P3 ^ 5;       //电饭锅
sbit heater = P3 ^ 6;    //热水器
sbit light = P3 ^ 7;     //电灯

sbit pan_io = P0 ^ 5;    // 电饭锅 继电器
sbit heater_io = P0 ^ 6; //热水器  继电器
sbit light_io = P0 ^ 7;  //电灯   继电器

#define Coil_AB1 {A1=1;B1=1;C1=0;D1=0;}//AB相通电，其他相断电
#define Coil_BC1 {A1=0;B1=1;C1=1;D1=0;}//BC相通电，其他相断电
#define Coil_CD1 {A1=0;B1=0;C1=1;D1=1;}//CD相通电，其他相断电
#define Coil_DA1 {A1=1;B1=0;C1=0;D1=1;}//D相通电，其他相断电
#define Coil_OFF {A1=0;B1=0;C1=0;D1=0;}//全部断电

void Init_Timer0(void);//定时器初始化
/*------------------------------------------------
              串口通讯初始化
  ------------------------------------------------*/
void UART_Init(void)
{
  SCON  = 0x50;           // SCON: 模式 1, 8-bit UART, 使能接收
  TMOD |= 0x20;               // TMOD: timer 1, mode 2, 8-bit 重装
  TH1   = 0xFD;               // TH1:  重装值 9600 波特率 晶振 11.0592MHz
  TR1   = 1;                  // TR1:  timer 1 打开
  //EA    = 1;                  //打开总中断
  //ES    = 1;                  //打开串口中断
  TI = 1;
}
void step_shun(int num, int speed) {    //转几圈  每步延时多少ms  假设顺时针转关窗
  int i;
  for (i = 0; i < num; i++) {
    Coil_AB1                //遇到Coil_AB1  用{A1=1;B1=1;C1=0;D1=0;}代替
    DelayMs(speed);         //改变这个参数可以调整电机转速 ,                     //数字越小，转速越大,力矩越小
    Coil_BC1
    DelayMs(speed);
    Coil_CD1
    DelayMs(speed);
    Coil_DA1
    DelayMs(speed);
  }
}
void step_ni(int num, int speed) {    //转几圈  每步延时多少ms
  int i;
  for (i = 0; i < num; i++) {
    Coil_DA1                //遇到Coil_AB1  用{A1=1;B1=1;C1=0;D1=0;}代替
    DelayMs(speed);         //改变这个参数可以调整电机转速 ,                    //数字越小，转速越大,力矩越小
    Coil_CD1
    DelayMs(speed);
    Coil_BC1
    DelayMs(speed);
    Coil_AB1
    DelayMs(speed);
  }
}

/*------------------------------------------------
                    主函数
  ------------------------------------------------*/
void main (void)
{
  int temp;
  char show_flag = 0;  //  为1   表示此时错误
  unsigned int i = 512;
  unsigned char Speed;
  float temperature;

  window_check = 1;		//引脚初始化
  light_check = 1;
  window = 1;
  pan = 1;
  heater = 1;
  light = 1;
  pan_io = 1;
  heater_io = 1;
  light_io = 1;

  DelayMs(20);          //延时有助于稳定

  Init_Timer0();
  //UART_Init();
  lcd_init();             //lcd1602初始化

  Init_DS18B20();
  write_com(0x80);        //lcd第一行位置
  print_string("welcome");//lcd显示内容
  write_com(0xc0);      //lcd第二行位置

  Speed = 200;
  Coil_OFF
  ReadTemperature();	 //读取温度，
  ReadTemperature_a();
  while (1)         //主循环
  {

    if (window == 0) {		 //关窗
      if (window_check == 1) {
        step_shun(1, 100);
      }
    }
    if (window == 1) {		  //开窗
      if (window_check == 0) {
        step_ni(1, 100);
      }
    }

    if (light == 0) {		 //开灯
      light_io = 0;
      if (light_check == 1) {
        show_flag = 1;
        write_com(0xc0);      //lcd第二行位置
        print_string("light on error  ");
      }
      else if (show_flag == 1) {
        show_flag = 0;
        write_com(0xc0);      //lcd第二行位置
        print_string("light on ok     ");
      }
    }
    if (light == 1) {		   //光灯
      light_io = 1;
      if (light_check == 0) {
        show_flag = 1;
        write_com(0xc0);      //lcd第二行位置
        print_string("light off error  ");
      }
      else if (show_flag == 1) {
        show_flag = 0;
        write_com(0xc0);      //lcd第二行位置
        print_string("light off ok     ");
      }
    }


    if (pan == 0) {			  //开电饭锅
      pan_io = 0;
      temp = (int)((float)ReadTemperature() * 0.625); //温度的10倍
      write_com(0xc0);      //lcd第二行位置
      print_string("temp:");
      write_data(temp / 100 % 10 + 48);
      write_data(temp / 10 % 10 + 48);
      write_data('.');
      write_data(temp % 10 + 48);
      if (temp / 10 <= 25) { //不正常
        print_string(" error1");
      }
      else print_string("    ok1");

    }
    if (pan == 1) {			 //关电饭锅
      pan_io = 1;
    }

    if (heater == 0) {		 //开热水器  同上
      heater_io = 0;
      temp = (int)((float)ReadTemperature_a() * 0.625); //温度的10倍
      write_com(0xc0);        //lcd第二行位置
      print_string("temp:");
      write_data(temp / 100 % 10 + 48);
      write_data(temp / 10 % 10 + 48);
      write_data('.');
      write_data(temp % 10 + 48);
      if (temp / 10 <= 25) {     //不正常的温度
        print_string(" error2");
      }
      else     print_string("    ok2");
    }
    if (heater == 1) {		//关热水器
      heater_io = 1;
    }



    /*
      if(ReadTempFlag==1)	   //定时器  的等时间运行
      {
         write_com(0xc0);      //lcd第二行位置
      ReadTempFlag=0;
      temp=ReadTemperature();
      temperature=(float)temp*0.0625;
      temp=(int)temperature;

      write_data(temp/100%10+48);
      write_data(temp/10%10+48);
      write_data(temp%10+48);
      write_data(' ');

      temp=ReadTemperature_a();
      temperature=(float)temp*0.0625;
      temp=(int)temperature;
      write_data(temp/100%10+48);
      write_data(temp/10%10+48);
      write_data(temp%10+48);

      }
    */
  }
}







/*------------------------------------------------
                    定时器初始化子程序	 串口的
  ------------------------------------------------*/
void Init_Timer0(void)
{
  TMOD |= 0x01;    //使用模式1，16位定时器，使用"|"符号可以在使用多个定时器时不受影响
  //TH0=0x00;        //给定初值
  //TL0=0x00;
  EA = 1;          //总中断打开
  ET0 = 1;         //定时器中断打开
  TR0 = 1;         //定时器开关打开
}
/*------------------------------------------------
                 定时器中断子程序
  ------------------------------------------------*/
void Timer0_isr(void) interrupt 1
{
  static unsigned int num;
  TH0 = (65536 - 2000) / 256; //重新赋值 2ms
  TL0 = (65536 - 2000) % 256;

  num++;
  if (num == 100)     //
  {
    num = 0;
    ReadTempFlag = 1; //读标志位置1
  }
}


