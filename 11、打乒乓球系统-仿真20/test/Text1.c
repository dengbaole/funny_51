#include  <REG51.H>
#include  <INTRINS.H>
#include  <stdio.h>

#define   uchar unsigned char
#define   uint unsigned int
#define DataPort P0	 //数码管段选 引脚
//引脚设置	  照着仿真图来
sbit L1 =  P2 ^ 4 ;
sbit L2 =  P2 ^ 5 ;
sbit R1 =  P2 ^ 6 ;
sbit R2 =  P2 ^ 7 ;
sbit buzzer =  P2 ^ 1 ;
sbit led9 =	P2 ^ 0 ;

sbit time4 =P3 ^ 4 ;
sbit time3 =P3 ^ 5 ;
sbit time2 =P3 ^ 6 ;
sbit time1 =P3 ^ 7 ;

sbit led_play1 =P2 ^ 2 ;
sbit led_play2 =P2 ^ 3 ;

uchar code dofly_table[10]={0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xf8,0x80,0x90,};   //段选 码
uchar L_num=0,R_num=0;	      //左右数码管显示的 值
uchar key1_flag=0,key2_flag=0; 	//是否允许拍球生效
uint delay_time=10000;  	//有关球速 两灯之间延时
uchar who_first=1;       //1 左边发球  2 右边发球
uchar play_continue=0;    //是否在打的标志
uchar play_times=0;    //玩的总次数   用于判断是否换一边 发球
uchar buzzer_flag=0;   //蜂鸣器状态   1为打一球结束时的鸣叫  用于限制按键发球

void Delay(uint t)
{
 while(--t);
}
delay_ms(uint x)
{
	uint i,j;
	for(j=0;j<x;j++)
		for(i=0;i<123;i++);	
}
void Init_Timer0(void)	//定时器0  5ms 中断 用于数码管显示
{
 TMOD |= 0x01;	  //使用模式1，16位定时器，使用"|"符号可以在使用多个定时器时不受影响		     
 TH0=(65536-2000)/256;	      //给定初值，
 TL0=(65536-2000)%256;
 EA=1;            //总中断打开
 ET0=1;           //定时器中断打开
 TR0=1;           //定时器开关打开
}


void LED_Lgo(void)	 //LED灯从左往右走，包含最后一颗灯的判断 蜂鸣器
{
 uint i=0;
 P1=0xff; 
 for(i=0;i<8;i++){	 //流水灯
   P1=~(1<<i);
   Delay(delay_time);
 } 
 P1=0xff;  
 led9=0;key2_flag=1; //最后一颗灯 允许按键2反应
 play_continue=0;	 //预先不继续  当右方按键按下 其值为1，继续
   Delay(delay_time);
 led9=1;key2_flag=0;  //最后一颗灯灭 屏蔽按键2反应

 if(play_continue==0){	 //如果按键没按下 不继续了，代表这次打球结束
   L_num++;buzzer_flag=1; //左方+1 分  蜂鸣器 响两声 3s
   buzzer=0;
   delay_ms(1000);
   buzzer=1;
   delay_ms(1000);
   buzzer=0;
   delay_ms(1000);
   buzzer=1;buzzer_flag=0;	//关闭蜂鸣器  关闭其状态 按键不被屏蔽
 } 
}
void LED_Rgo(void) //同上   最后一灯 开启检测  预结束标志
{
 uint i=0;
 P1=0xff;  
 led9=0;
   Delay(delay_time);
 led9=1;
 for(i=0;i<8;i++){
   P1=~(0x80>>i);
   if(i==7){ key1_flag=1; play_continue=0;  }
   Delay(delay_time);
 } 
 key1_flag=0;
 P1=0xff;

 if(play_continue==0){
  R_num++;
   buzzer=0; buzzer_flag=1;
   delay_ms(1000);
   buzzer=1;
   delay_ms(1000);
   buzzer=0;
   delay_ms(1000);
   buzzer=1;buzzer_flag=0;
 } 
}


//主函数：
void main()
{
  uint i;

  P1=0xff;
  buzzer=1;		//蜂鸣器不响
  P3=0xff;
  EA=1;          //全局中断开
  EX0=1;         //外部中断0开
  IT0=1;         //边沿触发
  EX1=1;         //外部中断1开
  IT1=1;         //IT1=1表示边沿触发

  Init_Timer0(); //定时器0  5ms 中断 用于数码管显示
  play_continue=0;  //未开始

  while (1)        //大循环
  {

   delay_time=0;      //有关打球速度的  时间间隔由四个开关决定
   if(time4==0)   delay_time+=8;
   if(time3==0)   delay_time+=4;
   if(time2==0)   delay_time+=2;
   if(time1==0)   delay_time+=1;
   delay_time=delay_time*4000;
   if(delay_time==0) delay_time=65000;	  //四个都没关上  就按照最长的时间算



  if(who_first==1){				//左边开球
    led_play1=0;led_play2=1;   	//发球灯亮
    while(play_continue==1){	//判断在玩 的标志 打到了就一直循环打
	  LED_Lgo();					//球左边发出
	  if(play_continue==0) break;	//任意一方没打到  退出循环
	  LED_Rgo();
	}
  }
  if(who_first==2){			   //右边开球  同时上
    led_play1=1;led_play2=0;   
    while(play_continue==1){
	  LED_Rgo();
	  if(play_continue==0) break;
	  LED_Lgo();
	}  
  }
  if((R_num+L_num)%5==0){		//如果打了5的倍数次的话，切换另一方发球的标志
  	if(play_times!=R_num+L_num){
  	   if(who_first==1) who_first=2;
	   else who_first=1;
	   play_times=R_num+L_num;
	}
  }


  if((R_num>=21)||(L_num>=21)){  //满21分为一局
    buzzer_flag=1;
  	for(i=0;i<6;i++){
	  delay_ms(100);
	  buzzer=0;
	  delay_ms(100);
	  buzzer=1;
	}
	buzzer_flag=0;
	L_num=0,R_num=0;	 //变量清零
  	who_first=1;
	play_times=0; 
  }


  }
}

void Timer0_isr(void) interrupt 1 using 1	 //定时器0中断服务函数   用于数码管的显示
{
 static uchar times_flag=0;
 TH0=(65536-5000)/256;	      //给定初值，
 TL0=(65536-5000)%256;

 if(times_flag>=3)  times_flag=0;
 else   times_flag++;

 if(times_flag==0){
   DataPort=0xff;
   L1=1;L2=0;R1=0;R2=0;
   DataPort=dofly_table[L_num/10%10];
 }
 if(times_flag==1){
   DataPort=0xff;
   L1=0;L2=1;R1=0;R2=0;
   DataPort=dofly_table[L_num%10];
 }
 if(times_flag==2){
   DataPort=0xff;
   L1=0;L2=0;R1=1;R2=0;
   DataPort=dofly_table[R_num/10];
 }
 if(times_flag==3){
   DataPort=0xff;
   L1=0;L2=0;R1=0;R2=1;
   DataPort=dofly_table[R_num%10];
 }

} 
void Key1(void) interrupt 0 using 1		//外部中断服务函数  按键1按下执行
{
  if(key1_flag==1){						//如果在最左边的灯亮的时候
  	play_continue=1; //打中了就继续
  }
  if(who_first==1){	//左边先  开始
    if(buzzer_flag==0)     play_continue=1;	  //判断是蜂鸣器不响的时候  发球
  } 


}
void key2(void) interrupt 2 		  //外部中断2   对应按键2 同上
{
  if(key2_flag==1){
  	play_continue=1; //打中了就继续
  }
  if(who_first==2){	//右边先  开始
    if(buzzer_flag==0)  play_continue=1;
  } 

}
