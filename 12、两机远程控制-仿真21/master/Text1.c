#include  <REG51.H>
#include  <INTRINS.H>
#include  <stdio.h>
#include "uart.h"
#include "lcd1602.h"

#define   uchar unsigned char
#define   uint unsigned int

uchar order = 0;       //发送的命令
uchar get_data = 0;    //发回来的数据

uchar code KeyCodeTable[] =   //关于矩阵按键的
{
  0x11, 0x12, 0x14, 0x18, 0x21, 0x22, 0x24, 0x28, 0x41, 0x42, 0x44, 0x48, 0x81, 0x82, 0x84, 0x88
};
void Delay()
{
  uchar i;
  for (i = 0; i < 200; i++);
}
uchar Keys_Scan()  //矩阵按键  识别按键
{
  uchar sCode, kCode, i, k;
  P0 = 0xf0;
  if ((P0 & 0xf0) != 0xf0)
  {
    Delay();
    if ((P0 & 0xf0) != 0xf0)
    {
      sCode = 0xfe;
      for (k = 0; k < 4; k++)
      {
        P0 = sCode;
        if ((P0 & 0xf0) != 0xf0)
        {
          kCode = ~P0;
          for (i = 0; i < 16; i++)
          {
            if (kCode == KeyCodeTable[i])
              //return i;
              if (i <= 3)  return i + 1;
              else if (i <= 7)  return i;
              else if (i <= 11)  return i - 1;
              else if (i <= 15)  return i - 2;
          }
        }
        else
          sCode = _crol_(sCode, 1);
      }
    }
  }
  return -1;
}

//主函数：
void main()
{
  //uint i;
  //uint tuntimes = 0;
  uchar key_num = 0;    //对应按下按键的数字

  uart_init();            //串口初始化  波特率 9600
  lcd_init();             //lcd1602初始化

  delay_ms(100);
  write_com(0x80);
  print_string("Hello  ");//lcd显示内容

  while (1)        //大循环
  {


    key_num = Keys_Scan();     //获取按键 情况

    //print_string("Hello  ");//lcd显示内容
    if (key_num != -1)     //如果按键有效
    {
      write_com(0x88);
      if (key_num != 10) {  //如果按键不是‘*’
        order = key_num;
        write_data(key_num / 10 + 48); //显示在lcd上
        write_data(key_num % 10 + 48);
      }
      else {          //按下‘*’时
        uart_send_char(order + 48); //发送上一次按下的命令
        //write_com(0x80+11);
        //print_string("ok");
        write_com(0xc0);        //清空lcd第二行显示
        print_string("                ");
        order = 0;
      }
    }


    //print_string("slave:");
    if ((get_data > '0') && (get_data < '9')) { //判断  接收到从机的  状态信息
      write_com(0xc0);
      switch (get_data) { //判断 发来的数据
        case '1': print_string("positive 1 work ");    break;
        case '2': print_string("positive 2 work ");    break;
        case '3': print_string("positive 3 work ");    break;
        case '4': print_string("reverse 1  work ");    break;
        case '5': print_string("reverse 2  work ");    break;
        case '6': print_string("reverse 3  work ");    break;
        case '7': print_string("        waiting ");    break;  //待机
        case '8': print_string("          stop  ");    break;  //紧急停止

        default:  break;
      }
      get_data = 0;  //清空  只显示一次  保证主循环高速
    }

    //delay_ms(10);
  }
}


void RX_int(void) interrupt 4     //串口接收中断服务函数   存放接收到的数据
{
  uchar data1;
  if (RI)    //串口中断
  {
    RI = 0;
    data1 = SBUF;
    get_data = data1;  //接收   从机发来的信息

  }
}
