#include  <REG51.H>
#include "uart.h"

#define   uchar unsigned char
#define   uint unsigned int

uchar one_move[] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80}; //三种情况的亮灯码
uchar two_move[] = {0x01, 0x03, 0x06, 0x0c, 0x18, 0x30, 0x60, 0xc0, 0x80};
uchar three_move[] = {0x01, 0x03, 0x07, 0x0e, 0x1c, 0x38, 0x70, 0xe0, 0xc0, 0x80};


uchar stop_flag = 0;  //外部中断0  用于
uchar step_allnum = 0;  //led执行的总步数
uchar step_num = 0; //led执行的步数  第几步
uchar order = 0;      //对应命令
uchar sec = 0;        //秒数
uchar sec_last = 0;      //上一秒数


void delay_ms(uint t) {   //延时函数   大概的1ms
  int j;
  for (; t != 0; t--)
    for (j = 0; j < 255; j++);
}
void Init_Timer0(void)  //定时器0  10ms 中断 用于数码管显示
{
  TMOD |= 0x01;    //使用模式1，16位定时器，使用"|"符号可以在使用多个定时器时不受影响
  TH0 = (65536 - 9216) / 256;  //给定初值，  晶振  11.0592Mhz
  TL0 = (65536 - 9216) % 256;
  EA = 1;          //总中断打开
  ET0 = 1;         //定时器中断打开
  TR0 = 1;         //定时器开关打开
}


//主函数：
void main()
{
  //  uint i;
  //  uint tuntimes = 0;
  EA = 1;        //全局中断开
  EX0 = 1;       //外部中断0开
  IT0 = 1;       //边沿触发
  uart_init();            //串口初始化  波特率 9600
  Init_Timer0();      //定时器0初始化
  delay_ms(100);

  while (1)        //大循环
  {
    if (stop_flag == 1) {   //如果是紧急停止按钮
      delay_ms(100);
      uart_send_char(8 + 48); //发送‘8’  给主机
      P0 = 0xff;       //关闭
      delay_ms(1000);
      stop_flag = 0;
    }

    if (sec_last != sec) {     //秒数变了    一秒后
      if (step_num == step_allnum) { //命令执行结束
        P0 = 0xff;
        uart_send_char(7 + 48);   //发送‘7’ 给主机  waiting
      }
      if (order == 1) {      //命令式1  正向1
        if (step_num < step_allnum) {
          uart_send_char(1 + 48); //发回给主机  状态
          P0 = ~one_move[step_num];
          step_num++;
        }
      }
      if (order == 4) {       //命令4  反向1
        if (step_num < step_allnum) {
          uart_send_char(4 + 48);
          P0 = ~one_move[step_allnum - step_num - 1];
          step_num++;
        }
      }
      if (order == 2) {
        if (step_num < step_allnum) {
          uart_send_char(2 + 48);
          P0 = ~two_move[step_num];
          step_num++;
        }
      }
      if (order == 5) {
        if (step_num < step_allnum) {
          uart_send_char(5 + 48);
          P0 = ~two_move[step_allnum - step_num - 1];
          step_num++;
        }
      }
      if (order == 3) {
        if (step_num < step_allnum) {
          uart_send_char(3 + 48);
          P0 = ~three_move[step_num];
          step_num++;
        }
      }
      if (order == 6) {
        if (step_num < step_allnum) {
          uart_send_char(6 + 48);
          P0 = ~three_move[step_allnum - step_num - 1];
          step_num++;
        }
      }


      sec_last = sec;       //更新  以便每秒执行
    }



  }
}


void RX_int(void) interrupt 4     //串口接收中断服务函数   存放接收到的数据
{
  uchar data1;
  if (RI)    //串口中断
  {
    RI = 0;
    data1 = SBUF;
    order = data1 - 48;

    if ((order > 0) && (order < 7)) { //解析主机发来的命令
      step_num = 0;
      if (order % 3 == 1) step_allnum = 8;
      if (order % 3 == 2) step_allnum = 9;
      if (order % 3 == 0) step_allnum = 10;
    }
  }
}
void Timer0_isr(void) interrupt 1 using 1  //定时器0中断服务函数
{
  static uchar num_10ms = 0;
  TH0 = (65536 - 9216) / 256;  //给定初值，  晶振  11.0592Mhz
  TL0 = (65536 - 9216) % 256;
  num_10ms++;
  if (num_10ms >= 100) {    //1s    提供秒数
    num_10ms = 0;
    sec++;
    if (sec >= 60) sec = 0;
  }
}
void ISR_Key(void) interrupt 0 using 1    //外部中断0
{
  stop_flag = 1;
  step_num = step_allnum;
  //P0=0xff;
  uart_send_char(8 + 48);  //发送 停止 状态
  //delay_ms(1000);
}