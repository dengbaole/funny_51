#include <reg52.h>
#include "uart.h"

#define uchar unsigned char
#define uint  unsigned int
        
uchar order_flag=0;	   //命令 是否允许读取
uchar order_come=0;	   //用于 表示 命令真正到来的标志
uchar order=0;		   //读取到的命令

sbit led1=P2^0;		   //led灯引脚

void delay_ms(uint t)	//ms延时函数
{
  uchar i;   
 while(t--)
 {
     //大致延时1mS
	  i=220;
      while(--i);
	  i=220; 
      while(--i);
 }
}

void main(void)
{
	order_flag=0;
	led1=0;			 //亮灯

	delay_ms(100);
	uart_init();	//波特率9600  esp8266已改为9600
	delay_ms(200);
	uart_send_str("AT+CWMODE=2\r\n");  //配置esp8266为AP模式
	delay_ms(100);
	uart_send_str("AT+CWSAP=\"esp8266\",\"123456123456\",1,3\r\n");	  //开启热点  对应名字和密码
	delay_ms(100);
	uart_send_str("AT+CIPMODE=0\r\n"); //关闭透传
	delay_ms(100);
	uart_send_str("AT+CIPMUX=1\r\n");  //开启多连接模式
	delay_ms(100);
	uart_send_str("AT+CIPSERVER=1,8080\r\n");  //开启服务器  IP为192.168.4.1  端口号8080
	delay_ms(100);
	led1=1;			 //光灯
	order_flag=1;
	while(1){		 //循环中 根据接收到的命令 执行开灯还是关灯
		if(order=='0')   led1=1;
		if(order=='1')   led1=0;
	}	
}
void RX_int(void) interrupt 4     //串口中断服务函数   用于接收wifi模块发回来的数据
{
	char i;
	if(RI)
	{
		RI = 0;
		i = SBUF;
		if(order_flag==1){		   //当允许接收数据时
			if(order_come==1){	   //如果数据来了 标志位1
				 order_come=0;	   //清零
				 order=i;		   //将接收到的命令传给变量  以便在主函数中执行
				 //uart_send_char(order);
			}
		    if(i==':')	order_come=1;	//如果接收到‘：’  说明下一个是最主要的命令数据	
		}

	}
}