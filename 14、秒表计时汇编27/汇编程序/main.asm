KEY1 BIT P1.0		;按键1 IO口定义--启动/暂停定时器
KEY2 BIT P1.1		;按键2 IO口定义--定时器停止，清零
ORG 0000H
LJMP MAIN 			;跳转至MAIN
ORG 000BH
LJMP T0_INT			;跳转至定时器0中断服务函数
ORG 0100H			;代码存放首地址0100H
COUNT EQU 40H		;定时器计数
SEC EQU 41H			;秒表计数
AX EQU 42H
SEC1 EQU 44H			
MAIN:
	MOV SP,#60H		;堆栈设置
	MOV TMOD,#01H	;定时器0设置为工作方式1
	MOV TL0,#0B0H	;赋初值，每50ms进一次中断
	MOV TH0,#3CH
	SETB ET0		;开定时器0中断
	SETB EA			;开总中断
	CLR TR0			;定时器0不运行
	MOV COUNT,#0	;清零COUNT
	MOV SEC,#0		;清零SEC
	MOV AX,#0		;清零SEC
M_LOOP:
	JNB KEY1,PK1	;当KEY1按下时，跳转至PK1
	JNB KEY2,PK2	;当KEY2按下时，跳转至PK2
	ACALL DISPLAY	;数码管显示秒表时间
	LJMP M_LOOP		;跳转至M_LOOP

PK1:ACALL DISPLAY	;数码管显示秒表时间	 
	JNB KEY1,PK1	;按键松开检测，如果按键未松开，则跳转至PK1，如果按键松开，则向下执行
	CPL TR0			;TR0取反
	LJMP M_LOOP		;跳转至M_LOOP
PK2:ACALL DISPLAY	;数码管显示秒表时间
	JNB KEY2,PK2	;按键松开检测，如果按键未松开，则跳转至PK2，如果按键松开，则向下执行
	CLR TR0			;定时器0停止运行
	MOV SEC,#00H	;秒表数据清零
	MOV AX,#00H
	LJMP M_LOOP		
DISPLAY:            
	MOV A,#01B		
	MOV P2,A		
	MOV DPTR,#TAB	
	MOV A,AX		
	MOV B,#10		
	DIV	AB			
	MOVC A,@A+DPTR	
	MOV P0,A		
	ACALL DELAY		
	MOV P0,#0FFH	
	MOV A,#10B		
	MOV P2,A
	MOV DPTR,#TAB	
	MOV A,AX		
	MOV B,#10	
	DIV AB			
	MOV A,B			
	MOVC A,@A+DPTR	
	MOV P0,A		
	CALL DELAY		
	MOV P0,#0FFH	
	MOV A,#100B		
	MOV P2,A		
	MOV A,SEC		
	MOV B,#10		
	DIV	AB			
	MOVC A,@A+DPTR	
	MOV P0,A		
	ACALL DELAY		
	MOV P0,#0FFH	
	MOV A,#1000B
	MOV P2,A
	MOV DPTR,#TAB	
	MOV A,SEC		
	MOV B,#10		
	DIV AB			
	MOV A,B			
	MOVC A,@A+DPTR	
	MOV P0,A		
	CALL DELAY		
	MOV P0,#0FFH	
	RET				

T0_INT:			
	MOV TL0,#0B0H	;定时器0赋值，50ms进一次中断
	MOV TH0,#3CH	
	PUSH PSW		;程序状态字进栈
	MOV A,COUNT		;A获取COUNT的值
	INC A			;A自加1
	MOV COUNT,A		;将A的值存入COUNT
	CJNE A,#20,T0RET;当计数为20次（1s）时，向下运行，否则跳转至T0RET
	MOV A,#00H;
	MOV COUNT,A		;将COUNT清零
	MOV A,SEC		;A获取SEC的值
	INC A			;A自加1
	MOV SEC,A		;将A的值存入SEC中
	CJNE A,#60,T0RET;当SEC为60时，向下运行，否则跳转至T0RET
	MOV A,#00H
	MOV SEC,A		;SEC清零
	INC AX 
T0RET: POP PSW     	;程序状态字出栈
	   RETI 		;返回
DELAY: 				;延时子程序数码管显示用
	   MOV R1,#40	;赋值R1为40
DELAY1:NOP			
	   DJNZ R1,DELAY1	;当R1自减1后，如果R1不为0，则跳转至DELAY1，否则向下运行
	   RET				
TAB:
     DB 0C0H,0F9H,0A4H,0B0H,99H,92H,82H,0F8H,080H,090H
END