#include<reg52.h> //包含头文件，一般情况不需要改动，头文件包含特殊功能寄存器的定义
#include "uart.h"
#define uchar unsigned char
#define uint unsigned int

sbit SPK = P3 ^ 7;      //定义喇叭接口

uchar High, Low; //定时器预装值的高8位和低8位
uchar order=0;	 //接收到的命令
uchar code freq[][2] = {	 //有关定时器0的 设置值
  0xD8, 0xF7, //00440HZ 1
  0xBD, 0xF8, //00494HZ 2
  0x87, 0xF9, //00554HZ 3
  0xE4, 0xF9, //00587HZ 4
  0x90, 0xFA, //00659HZ 5
  0x29, 0xFB, //00740HZ 6
  0xB1, 0xFB, //00831HZ 7
  0xEF, 0xFB, //00880HZ `1
};
uchar code Song[][100] =	//3首音乐
{
  {1, 2, 3, 1, 1, 2, 3, 1, 3, 4, 5, 3, 4, 5,  5, 6, 5, 4, 3, 1, 5, 6, 5, 4, 3, 1, 1, 5, 1, 1, 5, 1, -1},
  {1, 1, 5, 5, 6, 6, 5,  4, 4, 3, 3, 2, 2, 1,  5, 5, 4, 4, 3, 3, 2,  5, 5, 4, 4, 3, 3, 1  , -1},
  {3, 2, 1, 3, 2, 1, 1, 2, 3, 1, 3, 5, 1, 5, 4, 6, 2, 6, 3, 4, 2, 3, 6, 2, 3, 4, 2, 1, 1, 1, 2, 2, 2, 3, 3, 2, 6, 3, 5, 3, -1},//5, 1, 2, 6, 4, 5, -1}
  {3,2,3,  2,3,3,2,1,  6,1,2,2,3,2,1,6,6,1,5,  3,2,3,  2,3,3,2,1,  6,1,2,2,3,2,1,6,1,2,   3,2,3,  2,3,3,2,1,  6,1,2,2,3,2,1,6,6,1,5,   3,5,5,  5,6,5,3,  2,2,2,2,3,2,1,6,6,1,1,1  }
};
uchar code Len[][100] =	//对应每首歌 的每个音调持续长度
{
  {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, -1},
  {1, 1, 1, 1, 1, 1, 2,  1, 1, 1, 1, 1, 1, 2,  1, 1, 1, 1, 1, 1, 2,  1, 1, 1, 1, 1, 1, 2  , -1},
  {1, 1, 2, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 1, 2, 1, 2, 1, 1, 1, 1, 1, 2, 2, -1},
  {1,1,2,  1,1,1,1,2,  1,1,1,1,1,1,1,1,1,1,2,  1,1,2,  1,1,1,1,2,  1,1,1,1,1,1,1,1,1,2,   1,1,2,  1,1,1,1,2,  1,1,1,1,1,1,1,1,1,1,2,   1,1,2,  1,1,1,2,  1,1,1,1,1,1,1,1,1,1,1,2}
};



void delay(uint ms)				//不精确延时
{
  uchar i;
  while (ms--)
  {
    for (i = 0; i < 120; i++);
  }
}
/*------------------------------------------------
                 函数声明
  ------------------------------------------------*/
void Init_Timer0(void);//定时器初始化



  char tone_value;	   //音调值
  uint tone_num = 0;   //曲子中第几个调
  int which_music=0;   //哪一首曲子
  char stop_flag=1;    //暂停播放的标志
  char play_tone=0;    //键盘弹琴 的 音调值


void main (void)
{
  Init_Timer0();    //初始化定时器0
  uart_init();		//串口初始化
  SPK = 0;          //在未按键时，喇叭低电平，防止长期高电平损坏喇叭

  
  while (1)         //主循环
  {
	if(stop_flag==1){ 		//如果此时是 停止标志的话
		tone_value=-1;		//可以使后边定时器0停止
		
		if(play_tone!=0){	//如果 键盘有输入 1-8  的音调
		High = freq[play_tone - 1][1];		 //对应输入的音调响
    	Low = freq[play_tone - 1][0];
    	TR0 = 1;
    	delay(400);
		TR0 = 0;			   //延时后不响
		SPK = 0;
		play_tone=0;
		}
	}
	else{					 //如果是播放状态
	 tone_value = Song[which_music][tone_num];	   //获取哪首歌的第几个音符
    if (tone_value == -1)						   //如果音符是-1（结束了）
    {
      TR0 = 0; //关闭定时器						   
      SPK = 0; //在未按键时，喇叭低电平，防止长期高电平损坏喇叭
    }
    else										   //如果不是还没播放完
    {
      High = freq[tone_value - 1][1];			  //按照音符设置定时器的值，让其响对应的音调
      Low = freq[tone_value - 1][0];
      TR0 = 1;
      if (Len[which_music][tone_num] == 2) delay(400);	 //如果那个音符 长度为2  多响一会
      tone_num++;								  //索引下一个音符
    }
    delay(400);
    TR0 = 0; //关闭定时器    不响，每个音符之间停顿一小会儿
    SPK = 0;
    delay(100);


   }
  }
}
void RX_int(void) interrupt 4     //串口接收中断服务函数
{
        if(RI)
        {
                RI = 0;
                order = SBUF;		//收到的字符
				TR0 = 0;
				if((order>'0')&&(order<'9')) {	   //判断是否是键盘弹 音乐
					stop_flag=1;
					play_tone=order-'0';
				}
				else switch(order){				   //其他命令
					/*case '1': stop_flag=1;play_tone=1;  break;
					case '2': stop_flag=1;play_tone=2;  break;
					case '3': stop_flag=1;play_tone=3;  break;
					case '4': stop_flag=1;play_tone=4;  break;
					case '5': stop_flag=1;play_tone=5;  break;
					case '6':  stop_flag=1;play_tone=6;  break;
					case '7':  stop_flag=1;play_tone=7;  break;
					case '8':  stop_flag=1;play_tone=8;  break;	*/

					case 'a': stop_flag=0; which_music=0;tone_num=0;   break;	 //第一曲播放
					case 'b': stop_flag=0; which_music=1;tone_num=0;   break;	 //第二曲播放
					case 'c': stop_flag=0; which_music=2;tone_num=0;   break;	 //第三曲播放
					case 'e': stop_flag=0; which_music=2;tone_num=0;   break;	 //第四曲播放

					case 's': stop_flag=1; tone_num=0;play_tone=0;  break;		 //停止播放
					case 'u':if(which_music==0)which_music=3;  else which_music--;  stop_flag=0; tone_num=0;  break;	//上一曲播放
					case 'd':if(which_music==3)which_music=0;  else which_music++;  stop_flag=0; tone_num=0;  break;	//下一曲播放
					case 'p':stop_flag=0;  break;								 //播放或继续播放
					case 'w':stop_flag=1; break;								 //暂停播放

					default:   	break;
				}

	   }
}




void Init_Timer0(void)   //定时器0初始化子程序
{
  TMOD |= 0x01;    //使用模式1，16位定时器，使用"|"符号可以在使用多个定时器时不受影响
  EA = 1;          //总中断打开
  ET0 = 1;         //定时器中断打开
  //TR0=1;           //定时器开关打开
}
void Timer0_isr(void) interrupt 1  //定时器0中断子程序
{
  TH0 = High;		 //重新赋值
  TL0 = Low;
  SPK = !SPK;		 //蜂鸣器引脚  取反   以制造出pwm波
}


