#include  <REG51.H>
//#include  <INTRINS.H>
//#include  <stdio.h>
#include "uart.h"
#include "18b20.h"

#define   uchar unsigned char
#define   uint unsigned int
#define DataPort P0   //数码管段选 引脚

//引脚设置    照着仿真图来
sbit seg1=P2 ^ 0 ;	  //数码管
sbit seg2=P2 ^ 1 ;
sbit seg3=P2 ^ 2 ;
sbit buzzer =  P3 ^ 7 ;     //蜂鸣器引脚

sbit key_mode= P3 ^ 2 ;	  //5个按键
sbit key_2= P3 ^ 3 ;
sbit key_3= P3 ^ 4 ;
sbit key_jia= P3 ^ 5 ;
sbit key_jian= P3 ^ 6 ;

uchar code dofly_table[10] = {0xc0, 0xf9, 0xa4, 0xb0, 0x99, 0x92, 0x82, 0xf8, 0x80, 0x90,}; //段选 码
uchar hour = 12, min = 12, sec = 0; //时间  时分秒
uchar buzzer_flag = 0;             //蜂鸣器状态
uchar seg_show[8]={0xc0, 0xf9, 0xa4, 0xb0, 0x99, 0x92, 0x82, 0xf8};	   //用于缓存即将显示的 数码管值
uchar seg_num=0;        //用于在全局中  定时器中刷新
uchar setting_flag=0;   //非设置 暂停       设置 继续
uchar sec_temp = 0;		//用于定时器计算 时间的
uchar naozhong_hour=12,naozhong_min=0,naozhong_flag=0;	  //关于闹钟的

uchar jishi_min=0,jishi_sec=0,jishi_5ms=0;	 //关于计时功能的
uchar jishi_flag=0;     //是否开始计时

void seg_switch(uchar num){		 //让138译码器选中位选
	seg3=num/4%2;
	seg2=num/2%2;
	seg1=num%2;	
}
delay_ms(uint x)    //ms延时函数
{
  uint i, j;
  for (j = 0; j < x; j++)
    for (i = 0; i < 123; i++);
}
void Init_Timer0(void)  //定时器0  初始化   用于提供时间 10ms中断一次
{
  TMOD |= 0x01;    //使用模式1，16位定时器，使用"|"符号可以在使用多个定时器时不受影响
  TH0 = (65536 - 9216) / 256;  //给定初值，
  TL0 = (65536 - 9216) % 256;
  EA = 1;          //总中断打开
  ET0 = 1;         //定时器中断打开
  TR0 = 1;         //定时器开关打开
}





//主函数：
void main()
{
  uint i;
  uint temp;			//温度
  uchar mode_num=0;		//模式的值
  uint run_times=0;     //主循环中运行多少次
  uchar wei_num=0;     //时分秒位数的
  DataPort = 0xff;	   //不显示
  buzzer = 1;    //蜂鸣器不响

  //uart_init();
  Init_Timer0(); //定时器0  中断 用于数码管显示
  Init_DS18B20();		 //初始化ds18b20
  ReadTemperature();	 //读取温度，
    delay_ms(20);
  while (1)        //大循环
  {
   	if(key_mode==0){	//模式切换	按钮
	  delay_ms(10);
	  if(key_mode==0){
		 mode_num++;
		 if(mode_num>=3) mode_num=0;	//对应3种模式

		 setting_flag=0;  //切换模式的时候关闭设置
		 while(key_mode==0);
	     delay_ms(10);
	  }
	}
   	if(key_2==0){		 //设置	按钮
	  delay_ms(10);
	  if(key_2==0){
		 setting_flag=1-setting_flag;	//取反
		 if(mode_num==2){		//计时模式下  开始  暂停
			jishi_flag=1-jishi_flag;
		 }
		 while(key_2==0);
	     //delay_ms(10);
	  }
	}
   	if(key_3==0){		 //时分秒  位数选择	 复位计时
	  delay_ms(10);
	  if(key_3==0){
	    if(mode_num==0){	 //会闪烁
		 wei_num++;			 //即将设置的那两位数  会闪烁
		 if(wei_num>=3) wei_num=0;
		}
	    if(mode_num==1){
		  wei_num++;
		 if(wei_num>=3) wei_num=0;
		}
	    if(mode_num==2){	 //复位计时
		  jishi_min=0;
		  jishi_sec=0;
		  jishi_5ms=0;
		  jishi_flag=0;
		}

		 while(key_3==0);
	     delay_ms(10);
	  }
	}
   	if(key_jian==0){		 //--  时钟、闹钟的减
	  delay_ms(10);
	  if(key_jian==0){
		 if(mode_num==0){
		 	if(wei_num==0){
			   if(hour==0) hour=23;
			   else hour--;
			}
			if(wei_num==1){
			   if(min==0) min=59;
			   else min--;
			}
			if(wei_num==2){
			   if(sec==0) sec=59;
			   else sec--;
			}
		 }
		 if(mode_num==1){
		 	if(wei_num==0){
			   if(naozhong_hour<=0) naozhong_hour=23;
			   else naozhong_hour--;
			}
			if(wei_num==1){
			   if(naozhong_min<=0) naozhong_min=59;
			   else naozhong_min--;
			}
			if(wei_num==2){
			   if(naozhong_flag<=0) naozhong_flag=1;
			   else  naozhong_flag--;
			}
		 }
		 while(key_jian==0);
	     delay_ms(10);
	  }
	}
   	if(key_jia==0){		 //++    时钟、闹钟的  加
	  delay_ms(10);
	  if(key_jia==0){
		 if(mode_num==0){
		 	if(wei_num==0){
			   hour++;
			   if(hour>=24) hour=0;
			}
			if(wei_num==1){
			   min++;
			   if(min>=60) min=0;
			}
			if(wei_num==2){
			   sec++;
			   if(sec>=60) sec=0;
			}
		 }
		 if(mode_num==1){
		 	if(wei_num==0){
			   naozhong_hour++;
			   if(naozhong_hour>=24) naozhong_hour=0;
			}
			if(wei_num==1){
			   naozhong_min++;
			   if(naozhong_min>=60) naozhong_min=0;
			}
			if(wei_num==2){
			   naozhong_flag++;
			   if(naozhong_flag>=2) naozhong_flag=0;
			}
		 }
		 while(key_jia==0);
	     delay_ms(10);
	  }
	}
	if(naozhong_flag==1){    //闹钟开启	  响1分钟
	   if((naozhong_hour==hour)&&(naozhong_min==min)) buzzer=0;
	   else buzzer=1;
	}
	else buzzer=1;

	if(mode_num==0){    //显示时间模式
	seg_show[2]=0xbf;   //“-”
	seg_show[5]=0xbf;   //“-”
		seg_show[0]=dofly_table[hour / 10 % 10];
		seg_show[1]=dofly_table[hour  % 10];
		seg_show[3]=dofly_table[min / 10 % 10];
		seg_show[4]=dofly_table[min  % 10];
		seg_show[6]=dofly_table[sec / 10 % 10];
		seg_show[7]=dofly_table[sec  % 10];
	  if((setting_flag==1)&&(sec_temp<100)){   //是否为设置情况
		seg_show[3*wei_num]=0xff;
		seg_show[3*wei_num+1]=0xff;
	  }
	}

	if(mode_num==1){				 //闹钟模式
	seg_show[2]=0xbf;   //“-”
	seg_show[5]=0xff;   //“ ”
	seg_show[6]=0xff;	//‘ ’
		seg_show[0]=dofly_table[naozhong_hour / 10 % 10];
		seg_show[1]=dofly_table[naozhong_hour  % 10];
		seg_show[3]=dofly_table[naozhong_min / 10 % 10];
		seg_show[4]=dofly_table[naozhong_min  % 10];

		seg_show[7]=dofly_table[naozhong_flag  % 10];
	if((setting_flag==1)&&(sec_temp<100)){	  //是否设置情况
		seg_show[3*wei_num]=0xff;
		seg_show[3*wei_num+1]=0xff;
	   }
	}

	if(mode_num==2){	  //温度+秒表  模式
	run_times++;
	if(run_times>=50){	 //温度不能太高频的获取
	   run_times=0;
       temp = (uint)((float)ReadTemperature() * 0.0625); //温度
	   if(temp>=1){		 //防止因为中断出错
	   seg_show[0]=dofly_table[temp / 10 % 10];
	   seg_show[1]=dofly_table[temp /1 % 10];
   	   }
	}		   //刷新秒表计时的数值
	seg_show[2]=0xff;   //“ ”
	seg_show[3]=dofly_table[jishi_min / 10 % 10];
	seg_show[4]=dofly_table[jishi_min  % 10]&0x7f;
	seg_show[5]=dofly_table[jishi_sec / 10 % 10];
	seg_show[6]=dofly_table[jishi_sec  % 10]&0x7f;
	seg_show[7]=dofly_table[jishi_5ms%10];
	}
	

	delay_ms(10);
  }
}

void Timer0_isr(void) interrupt 1 using 1  //定时器0中断服务函数   用于数码管的显示
{
  TH0 = (65536 - 4608) / 256;  //给定初值， 5ms
  TL0 = (65536 - 4608) % 256;
  seg_num++;	     //数码管刷新
  if(seg_num>=8)  seg_num=0;
    DataPort = 0xff;       //这里用于显示时间  时分秒
	seg_switch(seg_num);   //位选 
	DataPort = seg_show[seg_num];  //段选

  //有关计时功能的
  if(jishi_flag==1){    //计时  开始 继续
   	jishi_5ms++;
	if(jishi_5ms>=200){
	  jishi_5ms=0;
	  jishi_sec++;
	}
    if (jishi_sec >= 60) {
      jishi_sec = 0;
      jishi_min++;
    }
    if (jishi_min >= 60) {
      jishi_min = 0;
    }
   }
  //有关时钟功能的
  sec_temp++;
  if (sec_temp >= 200) {	   //200次得1秒
    sec_temp = 0;
    sec++;
  }
  if (sec >= 60) {
    sec = 0;
    min++;
  }
  if (min >= 60) {
    min = 0;
    hour++;
  }
  if (hour >= 24) {
    hour = 0;
  }
}
