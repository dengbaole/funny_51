#include<reg52.h> //包含头文件，一般情况不需要改动，头文件包含特殊功能寄存器的定义
#include<stdio.h>
#include "18b20.h"
#include "lcd1602.h"
#include "delay.h"
#include "uart.h"

//bit ReadTempFlag;//定义读时间标志
//引脚设定
sbit temp_red = P2 ^ 4;     //温度 红灯
sbit temp_green = P2 ^ 5;   //温度 绿灯
sbit temp_jidianqi = P2 ^ 6; //继电器
sbit temp_motor = P2 ^ 7;    //电机

sbit alarm_buzzer = P3 ^ 7;    //闹钟 蜂鸣器
sbit alarm_motor = P3 ^ 6;    // 喂养 电机
sbit alarm_led = P3 ^ 5;    //    喂养 灯

uchar show_temp[17];     //有关lcd显示的
uchar show_temp_num = 0;   //关于数组的 第几个

uchar year = 20, month = 6, day = 22;     //当前年月日
uchar hour = 12, min = 00, sec = 0;       //时间  时分秒
uchar naozhong_hour = 12, naozhong_min = 10; //关于闹钟的

uchar RXD_num = 0;          //串口读取的字符数  用于判断长命令的
uchar uart_flag = 0;        //串口读取长数据 是否完成
uchar uart_data[20];        //串口数据存放的数组

uchar order = 0;  //接收到的命令
int temp;     //当前温度
#define KeyPort P0     //矩阵键盘在P0
void Init_Timer0(void);//定时器初始化
uchar KeyScan(void);  //键盘扫描函数，使用行列逐级扫描法
char Key_getnum(void);  //解析矩阵键盘的值




/*------------------------------------------------
                    主函数
  ------------------------------------------------*/
void main (void)
{

  uint mode = 0;  //模式值  0正常显示  1设置温度上下限  2闹钟
  uint i = 0;
  char key_num = 0; //矩阵键盘数值
  uint temp_low = 205, temp_high = 305; //上下限的温度  10倍
  uchar setup_flag = 0; //是否 是设置模式


  DelayMs(20);          //延时有助于稳定

  Init_Timer0();      //定时器0初始化
  uart_init();        //串口初始化   9600
  lcd_init();             //lcd1602初始化

  Init_DS18B20();     //温度 初始化
  write_com(0x80);        //lcd第一行位置
  print_string("welcome");//lcd显示内容
  //write_com(0xc0);      //lcd第二行位置
  ReadTemperature();   //读取温度，
  DelayMs(300);

  while (1)         //主循环
  {
    key_num = Key_getnum(); //获取矩阵键盘情况
    if (key_num != -1) {    //有按下
      //write_com(0xc0);
      //write_data(key_num/10%10+48);
      //write_data(key_num%10+48);
      if (key_num == 11) { //温度限制 按键
        mode = 1; write_com(0x80); print_string("  Temp limit"); //lcd显示  字符串  温度上下限
        show_temp[0] = 'L';
        show_temp[1] = ':';
        show_temp[2] = temp_low / 100 % 10 + '0';
        show_temp[3] = temp_low / 10 % 10 + '0';
        show_temp[4] = '.';
        show_temp[5] = temp_low % 10 + '0';
        show_temp[6] = 'C';
        show_temp[7] = ' ';
        show_temp[8] = 'H';
        show_temp[9] = ':';
        show_temp[10] = temp_high / 100 % 10 + '0';
        show_temp[11] = temp_high / 10 % 10 + '0';
        show_temp[12] = '.';
        show_temp[13] = temp_high % 10 + '0';
        show_temp[14] = 'C';

        show_temp[15] = 0;
        write_com(0xc0);
        print_string(show_temp);

      }
      if (key_num == 12) {  //闹钟    显示字符串  此时时间+闹钟值
        mode = 2;
        write_com(0x80); print_string("   Alarm clock  ");
        write_com(0xc0);
        write_data(hour / 10 % 10 + 48);
        write_data(hour % 10 + 48);
        write_data('-');
        write_data(min / 10 % 10 + 48);
        write_data(min % 10 + 48);
        write_data('-');
        write_data(sec / 10 % 10 + 48);
        write_data(sec % 10 + 48);
        print_string(" [");
        write_data(naozhong_hour / 10 % 10 + 48);
        write_data(naozhong_hour % 10 + 48);
        write_data(':');
        write_data(naozhong_min / 10 % 10 + 48);
        write_data(naozhong_min % 10 + 48);
        write_data(']');


      }
      if (key_num == 13) { //设置按钮     在闹钟或温度模式下的设置
        setup_flag = 1;
        if (mode == 1) {    //温度
          show_temp_num = 2;
          show_temp[2] = '_';
          show_temp[3] = '_';
          show_temp[5] = '_';
          show_temp[10] = '_';
          show_temp[11] = '_';
          show_temp[13] = '_';
          write_com(0xc0);
          print_string(show_temp);
        }
        if (mode == 2) {   //闹钟
          show_temp_num = 0;
          show_temp[0] = '_';
          show_temp[1] = '_';
          show_temp[2] = ':';
          show_temp[3] = '_';
          show_temp[4] = '_';
          show_temp[5] = 0;
          write_com(0xca);
          print_string(show_temp);
        }


      }

      if ((key_num > 0) && (key_num <= 10) && (setup_flag == 1)) { //数字键   用于设置温度上下限 或 闹钟
        if (key_num == 10) key_num = 0;
        if (mode == 1) {    //温度
          show_temp[show_temp_num] = key_num + '0';
          if (show_temp_num == 3)show_temp_num = 4;
          if (show_temp_num == 5)show_temp_num = 9;
          if (show_temp_num == 11)show_temp_num = 12;

          show_temp_num++;
          write_com(0xc0);
          print_string(show_temp);
        }
        if (mode == 2) {    //闹钟
          show_temp[show_temp_num] = key_num + '0';
          if (show_temp_num == 1)show_temp_num = 2;

          show_temp_num++;
          write_com(0xca);
          print_string(show_temp);
        }

      }
      //else show_temp_num=0;    //?

      if (key_num == 14) { //确认按钮   显示设置完成  并且把设置后的数据更新到变量
        setup_flag = 0;
        write_com(0xc0);
        print_string("   Setting OK   ");
        if (mode == 1) {
          temp_low = (show_temp[2] - 48) * 100 + (show_temp[3] - 48) * 10 + (show_temp[5] - 48);
          temp_high = (show_temp[10] - 48) * 100 + (show_temp[11] - 48) * 10 + (show_temp[13] - 48);
        }
        if (mode == 2) {
          naozhong_hour = (show_temp[0] - 48) * 10 + (show_temp[1] - 48);
          naozhong_min = (show_temp[3] - 48) * 10 + (show_temp[4] - 48);
        }

        DelayMs(300);
        setup_flag = 0;
        mode = 0;
      }
      if (key_num == 15) { //取消按钮   退出设置  正常显示
        setup_flag = 0;
        mode = 0;
      }
    }

    if (temp < temp_low) { //温度过低  继电器+红灯 工作
      temp_green = 1;
      temp_red = 0;
      temp_jidianqi = 0;
      temp_motor = 1;
    }
    else if (temp > temp_high) { //温度过高   电机+红灯  工作
      temp_green = 1;
      temp_red = 0;
      temp_jidianqi = 1;
      temp_motor = 0;
    }
    else {     //正常时候    绿灯 亮
      temp_green = 0;
      temp_red = 1;
      temp_jidianqi = 1;
      temp_motor = 1;
    }
    if ((naozhong_hour == hour) && (naozhong_min == min)) { //闹钟  时间到
      if (sec <= 30) {   //响30s   喂 灯 亮  电机动  蜂鸣器响
        alarm_led = 0;
        alarm_motor = 0;
        alarm_buzzer = 0;
      }
      if (sec == 31) {          //后30s恢复不工作
        alarm_led = 1;
        alarm_motor = 1;
        alarm_buzzer = 1;
      }
    }
    else {             //非闹钟 时间下
      alarm_led = 1;            //灯 不亮
      if (order == 'a') alarm_motor = 0; //根据串口命令 做动作
      if (order == 'b') alarm_motor = 1;
      if (order == 'e') alarm_buzzer = 0;
      if (order == 'f') alarm_buzzer = 1;
    }



	if(sec % 2 == 0){
        temp = ReadTemperature();
        temp = (int)((float)temp * 0.625); //温度的10倍
	
	}


    if (mode == 0) { //正常显示状态

      if (sec % 10 == 0) //显示间隔时间5s   这5s显示年月日时分秒  和闹钟时间
        //if(sec%2==0)
      {
        write_com(0x80);      //显示
        print_string("20");
        write_data(year / 10 % 10 + 48);
        write_data(year % 10 + 48);
        write_data('-');
        write_data(month / 10 % 10 + 48);
        write_data(month % 10 + 48);
        write_data('-');
        write_data(day / 10 % 10 + 48);
        write_data(day % 10 + 48);
        print_string("    ");

        write_com(0xc0);
        write_data(hour / 10 % 10 + 48);
        write_data(hour % 10 + 48);
        write_data('-');
        write_data(min / 10 % 10 + 48);
        write_data(min % 10 + 48);
        write_data('-');
        write_data(sec / 10 % 10 + 48);
        write_data(sec % 10 + 48);
        print_string(" [");
        write_data(naozhong_hour / 10 % 10 + 48);
        write_data(naozhong_hour % 10 + 48);
        write_data(':');
        write_data(naozhong_min / 10 % 10 + 48);
        write_data(naozhong_min % 10 + 48);
        write_data(']');

      }

      if (sec % 10 == 5) //后5s显示  温度值  和上下限的温度值
      {
        //ReadTempFlag=0;


        temp = ReadTemperature();
        temp = (int)((float)temp * 0.625); //温度的10倍
        write_com(0x80);
        print_string("  Temp: ");
        write_data(temp / 100 % 10 + 48);
        write_data(temp / 10 % 10 + 48);
        write_data('.');
        write_data(temp % 10 + 48);
        write_data('C');
        show_temp[0] = 'L';
        show_temp[1] = ':';
        show_temp[2] = temp_low / 100 % 10 + '0';
        show_temp[3] = temp_low / 10 % 10 + '0';
        show_temp[4] = '.';
        show_temp[5] = temp_low % 10 + '0';
        show_temp[6] = 'C';
        show_temp[7] = ' ';
        show_temp[8] = 'H';
        show_temp[9] = ':';
        show_temp[10] = temp_high / 100 % 10 + '0';
        show_temp[11] = temp_high / 10 % 10 + '0';
        show_temp[12] = '.';
        show_temp[13] = temp_high % 10 + '0';
        show_temp[14] = 'C';

        show_temp[15] = 0;
        write_com(0xc0);
        print_string(show_temp);
        print_string("    ");
      }

    }

	if (uart_flag == 1) {	 //如果数据已经接收完成      数组里："#2*"
      if (uart_data[13] == '*') {  // 设置时间 如“#200622120000*”
		 year= (uart_data[1]-48)*10+uart_data[2]-48;
		 month=(uart_data[3]-48)*10+uart_data[4]-48;
		 day=  (uart_data[5]-48)*10+uart_data[6]-48;
		 hour= (uart_data[7]-48)*10+uart_data[8]-48;
		 min=  (uart_data[9]-48)*10+uart_data[10]-48;
		 sec=  (uart_data[11]-48)*10+uart_data[12]-48;
		 uart_send_str("  Set time ok!");
    	 uart_send_char(13);   //过行
      }
      for (i = 0; i < 20; i++) uart_data[i] = 0;   //清空接收数组缓存
      uart_flag = 0;
	}


  }
}

void RX_int(void) interrupt 4     //串口接收中断服务函数   存放接收到的数据
{
  uchar data1;
  if (RI)    //串口中断
  {
    RI = 0;
    data1 = SBUF;
    if (data1 == '#') {
      RXD_num = 0;    //数据以‘#’开头 ，以‘*’结尾，不符合的不存放在数组里
    }
    uart_data[RXD_num] = data1;
    RXD_num++;
    if (data1 == '*') {     //判断结尾
      RXD_num = 0;
      uart_flag = 1;      //完成数据接收 标志
    }



    order = data1;
    switch (order) {     //根据命令判断
      case 't': uart_send_str("Time: 20");   //接收到‘t’就发回此时的时间年月日时分秒
        uart_send_char(year / 10 % 10 + 48);
        uart_send_char(year % 10 + 48);
        uart_send_char('-');
        uart_send_char(month / 10 % 10 + 48);
        uart_send_char(month % 10 + 48);
        uart_send_char('-');
        uart_send_char(day / 10 % 10 + 48);
        uart_send_char(day % 10 + 48);
        uart_send_char(',');
        uart_send_char(hour / 10 % 10 + 48);
        uart_send_char(hour % 10 + 48);
        uart_send_char(':');
        uart_send_char(min / 10 % 10 + 48);
        uart_send_char(min % 10 + 48);
        uart_send_char(':');
        uart_send_char(sec / 10 % 10 + 48);
        uart_send_char(sec % 10 + 48);  
    	uart_send_char(13);   //过行
		break;
      case 'n': uart_send_str("Alarm:");     //收到‘n’就发回 闹钟的时间
        uart_send_char(naozhong_hour / 10 % 10 + 48);
        uart_send_char(naozhong_hour % 10 + 48);
        uart_send_char(':');
        uart_send_char(naozhong_min / 10 % 10 + 48);
        uart_send_char(naozhong_min % 10 + 48);   
    	uart_send_char(13);   //过行
		break;
      case 'c': uart_send_str("Temp:");    //收到‘c’就发回此时的温度值
        uart_send_char(temp / 100 % 10 + 48);
        uart_send_char(temp / 10 % 10 + 48);
        uart_send_char('.');
        uart_send_char(temp % 10 + 48);
        uart_send_char('C');
    	uart_send_char(13);   //过行
		break;
    }


  }
}





/*------------------------------------------------
                    定时器初始化子程序
  ------------------------------------------------*/
void Init_Timer0(void)
{
  TMOD |= 0x01;    //使用模式1，16位定时器，使用"|"符号可以在使用多个定时器时不受影响
  //TH0=0x00;        //给定初值
  //TL0=0x00;
  EA = 1;          //总中断打开
  ET0 = 1;         //定时器中断打开
  TR0 = 1;         //定时器开关打开
}
/*------------------------------------------------
                 定时器中断子程序
  ------------------------------------------------*/
void Timer0_isr(void) interrupt 1
{
  static uint num;
  TH0 = (65536 - 46080) / 256; //重新赋值 50ms
  TL0 = (65536 - 46080) % 256;

  num++;
  //  if(num==1)  ReadTempFlag=0;   //读标志位置1
  //  if(num==11)  ReadTempFlag=1;

  if (num >= 20) {     //200次得1秒    计算此时时间
    num = 0;
    sec++;
  }
  if (sec >= 60) {
    sec = 0;
    min++;
  }
  if (min >= 60) {
    min = 0;
    hour++;
  }
  if (hour >= 24) {
    hour = 0;
    day++;
  }
  if (day >= 30) {
    day = 0;
    month++;
  }
  if (month >= 12) {
    month = 0;
    year++;
  }


}
uchar KeyScan(void)  //键盘扫描函数，使用行列逐级扫描法
{
  unsigned char Val;

  KeyPort = 0xf0; //高四位置高，低四位拉低
  if (KeyPort != 0xf0) //表示有按键按下
  {
    delay_ms(5);  //去抖
    if (KeyPort != 0xf0)
    { //表示有按键按下
      KeyPort = 0xfe; //检测第一行
      if (KeyPort != 0xfe)
      {
        Val = KeyPort & 0xf0;
        Val += 0x0e;
        while (KeyPort != 0xfe);
        delay_ms(5); //去抖
        while (KeyPort != 0xfe);
        return Val;
      }
      KeyPort = 0xfd; //检测第二行
      if (KeyPort != 0xfd)
      {
        Val = KeyPort & 0xf0;
        Val += 0x0d;
        while (KeyPort != 0xfd);
        delay_ms(5); //去抖
        while (KeyPort != 0xfd);
        return Val;
      }
      KeyPort = 0xfb; //检测第三行
      if (KeyPort != 0xfb)
      {
        Val = KeyPort & 0xf0;
        Val += 0x0b;
        while (KeyPort != 0xfb);
        delay_ms(5); //去抖
        while (KeyPort != 0xfb);
        return Val;
      }
      KeyPort = 0xf7; //检测第四行
      if (KeyPort != 0xf7)
      {
        Val = KeyPort & 0xf0;
        Val += 0x07;
        while (KeyPort != 0xf7);
        delay_ms(5); //去抖
        while (KeyPort != 0xf7);
        return Val;
      }
    }
  }
  return 0xff;
}
char Key_getnum(void)    //解析按键的 值
{
  switch (KeyScan())
  {
    case 0x7e: return 4; break; // 按下相应的键显示相对应的码值
    case 0x7d: return 8; break;
    case 0x7b: return 12; break;
    case 0x77: return 16; break;

    case 0xbe: return 3; break;
    case 0xbd: return 7; break;
    case 0xbb: return 11; break;
    case 0xb7: return 15; break;

    case 0xde: return 2; break;
    case 0xdd: return 6; break;
    case 0xdb: return 10; break;
    case 0xd7: return 14; break;

    case 0xee: return 1; break;
    case 0xed: return 5; break;
    case 0xeb: return 9; break;
    case 0xe7: return 13; break;
    default: return -1; break;
  }
}

