#include <reg52.h>
#include <intrins.h>
#define uint unsigned int
#define uchar unsigned char
#define delay4us() {_nop_();_nop_();_nop_();_nop_();}	//延时4us

sbit RS = P2^7;	  //引脚定义，跟仿真图一致
sbit RW = P2^6;
sbit E  = P2^5;

sbit CS  = P1^3;
sbit CLK = P1^0;
sbit DI  = P1^1;
sbit DO = P1^2;

sbit relay1 =P3^2;
sbit relay2 =P3^3;
sbit relay3 =P3^4;

sbit buzzer =P2^3;

uchar Display_Buffer[] = "00.00V";		//lcd1602上显示的
uchar code Line1[] = "Current Voltage:";

void DelayMS(uint ms)	  //大概的ms延时函数
{
 	uchar i;
	while(ms--)
	{
	 	for(i=0;i<120;i++);
	}
}

bit LCD_Busy_Check()	  //lcd读忙函数
{
 	bit result;
	RS = 0;
	RW = 1;
	E  = 1;
	delay4us();
	result = (bit)(P0&0x80);
	E  = 0;
	return result;
}

void LCD_Write_Command(uchar cmd)	   //写命令函数
{
 	while(LCD_Busy_Check());
	RS = 0;
	RW = 0;
	E  = 0;
	_nop_();
	_nop_();
	P0 = cmd;
	delay4us();
	E = 1;
	delay4us();
	E = 0;
}

void Set_Disp_Pos(uchar pos)	 //设置lcd1602 光标位置
{
 	LCD_Write_Command(pos | 0x80);
}

void LCD_Write_Data(uchar dat)	 //lcd写数据函数
{
 	while(LCD_Busy_Check());
	RS = 1;
	RW = 0;
	E  = 0;
	P0 = dat;
	delay4us();
	E = 1;
	delay4us();
	E = 0;
}

void LCD_Initialise()			//lcd初始化设置
{
	LCD_Write_Command(0x38); DelayMS(1);
	LCD_Write_Command(0x0c); DelayMS(1);
	LCD_Write_Command(0x06); DelayMS(1);
	LCD_Write_Command(0x01); DelayMS(1);
}

uchar Get_AD_Result(uchar ch_num)	  //ADC0832读取电压  0-255值
{
 	uchar i,dat1=0,dat2=0;
	CS  = 0;
	CLK = 0;
	DI=DO = 1; _nop_(); _nop_();

	CLK = 1; _nop_(); _nop_();	   
	CLK = 0;DI=DO = 1; _nop_(); _nop_(); //开始
	
	CLK = 1; _nop_(); _nop_();
	CLK = 0;DI=DO = ch_num; _nop_(); _nop_();	//哪一路输入
	
	CLK = 1;DI=DO = 1; _nop_(); _nop_();
	CLK = 0;DI=DO = 1; _nop_(); _nop_();
	
	for(i=0;i<8;i++)				//获取前一半数据
	{
	 	CLK = 1; _nop_(); _nop_();
		CLK = 0; _nop_(); _nop_();
		dat1 = dat1 << 1 | (DO);	//
	}
	for(i=0;i<8;i++)
	{
	 	dat2 = dat2 << ((uchar)(DI)<<i);//
		CLK = 1; _nop_(); _nop_();
		CLK = 0; _nop_(); _nop_();
	}
	CS = 1;
	//return (dat1 == dat2) ? dat1:0;
	return dat1;
}

void main()
{
 	uchar i;
	uint d;
	LCD_Initialise();	 //初始化lcd
	DelayMS(10);

	relay1=0;		//继电器 蜂鸣器 关闭
	relay2=0;
	relay3=0;
	buzzer=0;
	while(1)
	{
		relay1=0;
		relay2=0; 
		relay3=1;  //选中  量程最大
		DelayMS(30);
	 	d = Get_AD_Result(0)*8241.0/255;  //理论值：83.3333
		if(d>=8240)  buzzer=1;	   //电压大于最大量程  鸣叫
		else 	buzzer=0;
		if(d<=500){		 //电压小于5V  
			relay1=1;
			relay2=0;
			relay3=0;
			DelayMS(30);
		    d = Get_AD_Result(0)*500.0/255;   //理论值 5.00
		}
		else if(d<=4000){	   //电压小于40V时，自动切换量程  更精确
			relay1=0;
			relay2=1;
			relay3=0;
			DelayMS(30);
		    d = Get_AD_Result(0)*4130.0/255;   //理论值  42.00
		}
		else if(d<=6000){	 //电压小于60V时，自动切换量程  更精确
			relay1=0;
			relay2=1;
			relay3=1;
			DelayMS(30);
		    d = Get_AD_Result(0)*6140.0/255;	//理论值： 61.813
		} 

		Display_Buffer[0]=d/1000+'0';	 //将要显示的数据存入数组 缓存
		Display_Buffer[1]=d/100%10+'0';

		Display_Buffer[3]=d/10%10+'0';
		Display_Buffer[4]=d%10+'0';
		Set_Disp_Pos(0x01);				//设置显示位置
		i = 0;
		while(Line1[i]!='\0')		   //显示第一行 字符串
		{
		 	LCD_Write_Data(Line1[i++]);	
		}	 
		Set_Disp_Pos(0x46);
		i = 0;
		while(Display_Buffer[i]!='\0')	  //显示出电压值
		{
		 	LCD_Write_Data(Display_Buffer[i++]);
		}
		DelayMS(200);
	}
}









