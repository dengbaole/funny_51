#include  <REG52.H>
#include  <INTRINS.H>

#define uchar unsigned char   //宏定义方便以后用
#define uint unsigned int
#define ulong unsigned long

#define DataPort P0   //数码管段选 引脚

//引脚设置    照着仿真图来
sbit seg1 = P2 ^ 0 ;  //数码管  的38译码器
sbit seg2 = P2 ^ 1 ;
sbit seg3 = P2 ^ 2 ;
sbit control =  P3 ^ 7 ;   //控制水泵
sbit motor =  P1 ^ 0 ;     //水泵

sbit  sda = P1 ^ 6; //用于24c02的引脚
sbit  scl = P1 ^ 7;

sbit key_mode = P3 ^ 2 ;  //5个按键
sbit key_2 = P3 ^ 3 ;
sbit key_3 = P3 ^ 4 ;
sbit key_jia = P3 ^ 5 ;
sbit key_jian = P3 ^ 6 ;

sbit DATA = P2 ^ 5; //连接DHT11的引脚
sbit SCK = P2 ^ 4;

typedef union      //定义共同类型
{
  unsigned int i;    //i表示测量得到的温湿度数据（int 形式保存的数据）
  float f;         //f表示测量得到的温湿度数据（float 形式保存的数据）
} value;
enum {TEMP, HUMI};
//uchar TEMP_BUF[7];          //用于记录温度
//uchar HUMI_BUF[5];          //用于记录湿度
int real_tempure = 0; //实际温度
uchar real_humi = 0;
char humi_L = 50, humi_H = 80; //设置限制范围

#define noACK 0       //继续传输数据，用于判断是否结束通讯
#define ACK   1             //结束数据传输；
//地址  命令  读/写
#define STATUS_REG_W 0x06   //000   0011    0
#define STATUS_REG_R 0x07   //000   0011    1
#define MEASURE_TEMP 0x03   //000   0001    1
#define MEASURE_HUMI 0x05   //000   0010    1
#define RESET        0x1e   //000   1111    0


uchar code dofly_table[10] = {0xc0, 0xf9, 0xa4, 0xb0, 0x99, 0x92, 0x82, 0xf8, 0x80, 0x90,}; //段选 码
uchar hour = 12, min = 12, sec = 0; //时间  时分秒

uchar seg_show[8] = {0xc0, 0xf9, 0xa4, 0xb0, 0x99, 0x92, 0x82, 0xf8};  //用于缓存即将显示的 数码管值
uchar seg_num = 0;      //用于在全局中  定时器中刷新
uchar setting_flag = 0; //非设置 暂停       设置 继续
uchar sec_temp = 0;   //用于定时器计算 时间的

void seg_switch(uchar num) {   //让138译码器选中位选
  seg3 = num / 4 % 2;
  seg2 = num / 2 % 2;
  seg1 = num % 2;
}
delay_ms(uint x)    //ms延时函数
{
  uint i, j;
  for (j = 0; j < x; j++)
    for (i = 0; i < 123; i++);
}
void Init_Timer0(void)  //定时器0  初始化   用于提供时间 10ms中断一次
{
  TMOD |= 0x01;    //使用模式1，16位定时器，使用"|"符号可以在使用多个定时器时不受影响
  TH0 = (65536 - 9216) / 256;  //给定初值，
  TL0 = (65536 - 9216) % 256;
  EA = 1;          //总中断打开
  ET0 = 1;         //定时器中断打开
  TR0 = 1;         //定时器开关打开
}


//写字节程序 DHT11
char SHTXX_write_byte(unsigned char value)
{
  unsigned char i, error = 0;
  for (i = 0x80; i > 0; i /= 2)     //shift bit for masking 高位为1，循环右移
  {
    if (i & value) DATA = 1;      //和要发送的数相与，结果为发送的位
    else DATA = 0;
    SCK = 1;
    _nop_(); _nop_(); _nop_();      //延时3us
    SCK = 0;
  }
  DATA = 1;                         //释放数据线
  SCK = 1;
  error = DATA;                     //检查应答信号，确认通讯正常
  _nop_(); _nop_(); _nop_();
  SCK = 0;
  DATA = 1;
  return error;                     //error=1 通讯错误
}

//读字节程序
char SHTXX_read_byte(unsigned char ack)
{
  unsigned char i, val = 0;
  DATA = 1;                         //释放数据线
  for (i = 0x80; i > 0; i >>= 1)    //高位为1，循环右移
  {
    SCK = 1;
    if (DATA) val = (val | i);   //读一位数据线的值
    SCK = 0;
  }
  DATA = !ack;                      //如果是校验，读取完后结束通讯；
  SCK = 1;
  _nop_(); _nop_(); _nop_();        //延时3us
  SCK = 0;
  _nop_(); _nop_(); _nop_();
  DATA = 1;                         //释放数据线
  return val;
}
//启动传输
void SHTXX_transstart(void)
{
  DATA = 1; SCK = 0;               //准备
  _nop_();
  SCK = 1;
  _nop_();
  DATA = 0;
  _nop_();
  SCK = 0;
  _nop_(); _nop_(); _nop_();
  SCK = 1;
  _nop_();
  DATA = 1;
  _nop_();
  SCK = 0;
}
//连接复位
void SHTXX_Init(void)
{
  unsigned char i;
  DATA = 1; SCK = 0;                //准备
  for (i = 0; i < 9; i++)           //DATA保持高，SCK时钟触发9次，发送启动传输，通迅即复位
  {
    SCK = 1;
    SCK = 0;
  }
  SHTXX_transstart();                   //启动传输
}

//温湿度测量   // 进行温度或者湿度转换，由参数mode决定转换内容；
char SHTXX_measure(unsigned char *p_value, unsigned char *p_checksum, unsigned char mode)
{
  //  enum {TEMP,HUMI};    //已经在头文件中定义
  unsigned error = 0;
  unsigned int i;

  SHTXX_transstart();                   //启动传输
  switch (mode)                    //选择发送命令
  {
    case TEMP : error += SHTXX_write_byte(MEASURE_TEMP); break;    //测量温度
    case HUMI : error += SHTXX_write_byte(MEASURE_HUMI); break;    //测量湿度
    default     : break;
  }
  for (i = 0; i < 65535; i++) if (DATA == 0) break; //等待测量结束
  if (DATA) error += 1;             // 如果长时间数据线没有拉低，说明测量错误
  *(p_value) = SHTXX_read_byte(ACK);   //读第一个字节，高字节 (MSB)
  *(p_value + 1) = SHTXX_read_byte(ACK); //读第二个字节，低字节 (LSB)
  *p_checksum = SHTXX_read_byte(noACK); //read CRC校验码
  return error;          // error=1 通讯错误
}

//温湿度值标度变换及温度补偿
void SHTXX_calc(float *p_humidity , float *p_temperature)
{

  float rh = *p_humidity;           // rh:      12位 湿度
  float t = *p_temperature;         // t:       14位 温度
  float rh_lin;                     // rh_lin: 湿度 linear值
  float rh_true;                    // rh_true: 湿度 ture值
  float t_C;                        // t_C   : 温度 ℃

  t_C = t * 0.01 - 40;              //补偿温度
  rh_lin = -0.0000028 * rh * rh + 0.0405 * rh - 4; //相对湿度非线性补偿
  rh_true = rh / 33;
  if (rh_true > 26)
    rh_true = rh / 33 + 1;
  if (rh_true > 33)
    rh_true = rh / 33 + 1.5;
  if (rh_true > 40)
    rh_true = rh / 33 + 3;
  if (rh_true > 50)
    rh_true = rh / 33 + 3.7;
  if (rh_true > 70)
    rh_true = rh / 33 + 3;
  if (rh_true > 90)
    rh_true = rh / 33 + 1.6;

  //rh_true=(t_C-25)*(0.01+0.00008*rh)+rh_lin;   //相对湿度对于温度依赖性补偿  ，仿真的时候去掉补偿


  if (rh_true > 100)rh_true = 100;  //湿度最大修正
  if (rh_true < 0.1)rh_true = 0.1;  //湿度最小修正

  *p_temperature = t_C;             //返回温度结果
  *p_humidity = rh_true;            //返回湿度结果
}


void Covert_HT()
{
  value humi_val, temp_val;   //定义两个共同体，一个用于湿度，一个用于温度
  uchar checksum;     //CRC
  uchar error = 0;
  error = 0;           //初始化error=0，即没有错误
  error += SHTXX_measure((unsigned char*)&temp_val.i, &checksum, TEMP); //温度测量
  error += SHTXX_measure((unsigned char*)&humi_val.i, &checksum, HUMI); //湿度测量
  if (error != 0) SHTXX_Init();              ////如果发生错误，系统复位
  else
  {
    humi_val.f = (float)humi_val.i;                 //转换为浮点数
    temp_val.f = (float)temp_val.i;                 //转换为浮点数
    SHTXX_calc(&humi_val.f, &temp_val.f);           //修正相对湿度及温度

    real_tempure = temp_val.f + 1; //修正一下偏
    real_humi    = humi_val.f - 1;
	if(real_humi>=82)	  real_humi-=1;
  }
}






void delay()        //延时应大于4.7us     24c02的
{
  ;;;
}

void start()        //开始发送数据
{ sda = 1;
  delay();         //scl在高电平期间，sda由高到低
  scl = 1;
  delay();
  sda = 0;
  delay();
}

void stop()         //停止发送数据
{ sda = 0;           //scl在高电平期间，sda由高到低
  delay();
  scl = 1;
  delay();
  sda = 1;
  delay();
}

void response()    //应答
{ uchar i;
  scl = 1;
  delay();
  if ((sda == 1) && i < 250) i++; //应答sda为0，非应答为1
  scl = 0;                        //释放总线
  delay();
}

void  noack()
{ scl = 1;
  delay();
  scl = 1;
  delay();
  scl = 0;
  delay();
  sda = 0;
  delay();
}
void init()                      //初始化
{ sda = 1;
  delay();
  scl = 1;
  delay();
}
void write_byte(uchar date)     //写一个字节
{ uchar i, temp;
  temp = date;
  for (i = 0; i < 8; i++)
  { temp = temp << 1;
    scl = 0;                  //scl上跳沿写入
    delay();
    sda = CY;                 //溢出位
    delay();
    scl = 1;
    delay();
    scl = 0;
    delay();
  }
  scl = 0;
  delay();
  sda = 1;
  delay();
}
uchar read_byte()  //  读取1字节
{ uchar i, k;
  scl = 0;
  delay();
  sda = 1;
  delay();
  for (i = 0; i < 8; i++)
  { scl = 1;
    delay();
    k = (k << 1) | sda;
    scl = 0;
    delay();
  }
  return k;
}
void delay1(uchar x)  //延时  用于24c02的
{ uchar a, b;
  for (a = x; a > 0; a--)
    for (b = 200; b > 0; b--);
}

void write_add(uchar address, uchar date)    //写数据
{ start();
  write_byte(0xa0);                    //设备地址
  response();
  write_byte(address);
  response();
  write_byte(date);
  response();
  stop();
}

uchar read_add(uchar address)	  //读数据
{ uchar date;
  start();
  write_byte(0xa0);
  response();
  write_byte(address);
  response();
  start();
  write_byte(0xa1);                //1表示接收地址
  response();
  date = read_byte();
  noack();
  stop();
  return date;
}


//主函数：
void main()
{
  uint i;
  uchar mode_num = 0;  //模式的值
  uchar wei_num = 0;   //时分秒位数的
  DataPort = 0xff;     //不显示
  control = 1;         //上拉   用于输入
  SHTXX_Init();  //初始化  DHT11
  Init_Timer0(); //定时器0  中断 用于数码管显示

  humi_L = read_add(0); //从24c02读取数据
  delay_ms(1);
  humi_H = read_add(1); //从24c02读取数据

  delay_ms(20);
  while (1)        //大循环
  {


    if (key_mode == 0) { //确定  按钮
      delay_ms(10);
      if (key_mode == 0) {
        mode_num = 0;	//正常显示模式
        setting_flag = 0; //清零
        write_add(0, humi_L); //写入24c02中
        delay_ms(1);
        write_add(1, humi_H);

        while (key_mode == 0);	//等待按键松开
        delay_ms(10);
      }
    }
    if (key_2 == 0) { //设置 按钮
      delay_ms(10);
      if (key_2 == 0) {
        setting_flag = 1; //设置的标志  为1
        mode_num = 1;
        wei_num = 0;

        while (key_2 == 0);
        delay_ms(10);
      }
    }
    if (key_3 == 0) { //时分秒  位数选择   复位计时
      delay_ms(10);
      if (key_3 == 0) {
        if (mode_num == 1) { 
          wei_num++;      //即将设置的那位数  会闪烁
          if (wei_num >= 4) wei_num = 0;
        }

        while (key_3 == 0);
        delay_ms(10);
      }
    }
    if (key_jian == 0) { //--  
      if (mode_num == 1) {
        switch (wei_num) {	  //根据位数   对应修改数值
          case 0:  humi_L -= 10;   break;
          case 1:  humi_L -= 1;  break;
          case 2:  humi_H -= 10;   break;
          case 3:  humi_H -= 1;  break;
        }
        if (humi_L < 0) humi_L = 0;	   //限制不能低于0 
        if (humi_L >= humi_H) humi_H = humi_L + 1;	   //低限制不能高于 高限制

      }
      while (key_jian == 0);
      delay_ms(10);
    }
    if (key_jia == 0) { //++
      if (mode_num == 1) {
        switch (wei_num) {
          case 0:  humi_L += 10;   break;
          case 1:  humi_L += 1;  break;
          case 2:  humi_H += 10;   break;
          case 3:  humi_H += 1;  break;
        }
        if (humi_H > 99) humi_H = 99;			 //限制最高湿度为99
        if (humi_L >= humi_H) humi_L =   humi_H - 1;   //低限制不能高于 高限制
      }
      while (key_jia == 0);
      delay_ms(10);
    }

    if (mode_num == 1) {   //   设置模式
      seg_show[2] = 0xbf; //“-”
      seg_show[5] = 0xff; //“-”
      seg_show[0] = dofly_table[humi_L / 10 % 10];
      seg_show[1] = dofly_table[humi_L  % 10];
      seg_show[3] = dofly_table[humi_H / 10 % 10];
      seg_show[4] = dofly_table[humi_H  % 10];
      seg_show[6] = dofly_table[real_humi / 10 % 10];
      seg_show[7] = dofly_table[real_humi  % 10];
      if ((setting_flag == 1) && (sec_temp < 100)) { //设置	  并且前半秒
        switch (wei_num) {						   //根据位数  来闪烁
          case 0:  seg_show[0] = 0xff;   break;
          case 1:  seg_show[1] = 0xff;   break;
          case 2:  seg_show[3] = 0xff;   break;
          case 3:  seg_show[4] = 0xff;   break;
        }
      }
    }


    if (mode_num == 0) { //正常显示	 模式
      Covert_HT();		 //获取湿度	 显示
      seg_show[0] = dofly_table[humi_L / 10 % 10];
      seg_show[1] = dofly_table[humi_L  % 10];
      seg_show[2] = 0xbf; //“-”
      seg_show[3] = dofly_table[humi_H / 10 % 10];
      seg_show[4] = dofly_table[humi_H  % 10];
      seg_show[5] = 0xff; //“ ”

      seg_show[6] = dofly_table[real_humi / 10 % 10];
      seg_show[7] = dofly_table[real_humi  % 10];
    }

    if (humi_L > real_humi)  motor = 0;	  //湿度低于最低限度   打开水泵  
    if (humi_H < real_humi)  motor = 1;	  //湿度高于 最高限度  关闭水泵
    if ((humi_L <= real_humi) && (humi_H >= real_humi))	 //在范围内  由一个开关控制
      if (control == 0) motor = 0;
      else  motor = 1;




    delay_ms(10);
  }
}

void Timer0_isr(void) interrupt 1 using 1  //定时器0中断服务函数   用于数码管的显示
{
  TH0 = (65536 - 4608) / 256;  //给定初值， 5ms
  TL0 = (65536 - 4608) % 256;
  seg_num++;      	     //数码管刷新
  if (seg_num >= 8)  seg_num = 0;
  DataPort = 0xff;       //这里用于显示时间  时分秒
  seg_switch(seg_num);   //位选
  DataPort = seg_show[seg_num];  //段选


  sec_temp++;				  //同于用于计算时间
  if (sec_temp >= 200) {     //200次得1秒
    sec_temp = 0;
    sec++;
  }
  if (sec >= 60) {
    sec = 0;
    min++;
  }
  if (min >= 60) {
    min = 0;
    hour++;
  }
  if (hour >= 24) {
    hour = 0;
  }
}
