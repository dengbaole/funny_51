/******************************************************************************

  /*******************************************************************************/
//#include <reg52.h>                            //MCU头文件
#include "STC15W.h"
#include "intrins.h"                            //包含nop指令头文件

#define nop  _nop_();
//宏定义
/********************************定义控制端口***********************************/
sbit DO = P3 ^ 7;                                //定义信号输出DO    ws2812灯带的引脚
sbit led = P1 ^ 0;                                //工作指示灯的引脚  低电平亮

sbit yellow_led = P1 ^ 1;              //黄灯  引脚   高电平亮
sbit write_led = P1 ^ 2;                //白灯  引脚   高电平亮

sbit key_u = P3 ^ 6; //对应5个按键  引脚
sbit key_d = P3 ^ 5;
sbit key_o = P3 ^ 4;
sbit key_3 = P3 ^ 3;
sbit key_2 = P3 ^ 2;
/**********************************定义变量*************************************/

unsigned char PWM = 255;                    //灰度数据
unsigned char Rda, Gda, Bda;                //R、G、B灰度数据  临时
unsigned char bdata LED_data;               //可位操作的数据发送暂存变量声明
sbit bit0 = LED_data ^ 0;                        //被发送的数据各位定义
sbit bit1 = LED_data ^ 1;
sbit bit2 = LED_data ^ 2;
sbit bit3 = LED_data ^ 3;
sbit bit4 = LED_data ^ 4;
sbit bit5 = LED_data ^ 5;
sbit bit6 = LED_data ^ 6;
sbit bit7 = LED_data ^ 7;

#define numLEDs 50  //数量   50个
unsigned char idata buf_R[numLEDs] = {0};//颜色缓存数组
unsigned char idata buf_G[numLEDs] = {0};
unsigned char idata buf_B[numLEDs] = {0};
unsigned long code colour_3[7] = {         //红绿蓝  黄青紫  白
  0xff0000, 0xff00, 0xff, 0xffff00, 0xffff, 0xff00ff, 0xffffff
};
unsigned char light = 100;  //亮度   RGB的    0-100
unsigned char color_num = 0;   //颜色  对应colour_3  数组颜色值
/**********************************延时函数*************************************/
void delay(unsigned int n)                  //n=1,延时500us   不确定
{
  unsigned int i;
  while (n--)
    for (i = 0; i < 860; i++);
}

/********************发送0码函数,高电平400ns,周期1.25us************************/
void send_data_0()
{
  DO = 1;
  nop; nop; nop; nop; nop;
  nop; nop; nop; nop; nop;
  DO = 0;
  nop; nop; nop; nop; nop; nop; nop;
  nop; nop; nop; nop; nop; nop; nop;
}

/*******************发送1码函数,高电平800ns,周期1.25us*************************/
void send_data_1()
{
  DO = 1;
  nop; nop; nop; nop; nop; nop;
  nop; nop; nop; nop; nop;
  nop; nop; nop; nop; nop; nop;
  nop; nop; nop; nop; nop;
  DO = 0;
}

/***********************发送1个字节数据,高位先发*******************************/
void send_data(unsigned char DATA)
{
  LED_data = DATA;
  if (bit7)  send_data_1();  else send_data_0();
  if (bit6)  send_data_1();  else send_data_0();
  if (bit5)  send_data_1();  else send_data_0();
  if (bit4)  send_data_1();  else send_data_0();
  if (bit3)  send_data_1();  else send_data_0();
  if (bit2)  send_data_1();  else send_data_0();
  if (bit1)  send_data_1();  else send_data_0();
  if (bit0)  send_data_1();  else send_data_0();
}
/******************************发送50  led数据*************************************/
void show_ws2812(void)    //显示
{
  unsigned int i;
  for (i = 0; i < numLEDs; i++)
  {
    send_data(buf_G[i]);             //发送G灰度数据
    send_data(buf_R[i]);             //发送R灰度数据
    send_data(buf_B[i]);             //发送B灰度数据
  }
  delay(20) ;
}
void SetPixelColor(unsigned char num, unsigned long c) //存在缓存数组里    分成三基色
{
  buf_R[num] = (unsigned char)(c >> 16) * light / 100;
  buf_G[num] = (unsigned char)(c >> 8) * light / 100;
  buf_B[num] = (unsigned char)(c) * light / 100;
}
void clear_Buff(void)   //清空显示数据缓存
{
  unsigned char i = 0;
  for ( i = 0; i < numLEDs; i++)
    SetPixelColor(i, 0);
}
void colorWipe(unsigned char num, unsigned long c)    //单种颜色填充   0-num都为c颜色值   范围1-50
{
  unsigned char i = 0;
  for ( i = 0; i < num; i++)
  {
    SetPixelColor(i, c);
  }
}
void color1led(unsigned char num, unsigned long c)    //单种颜色填充   1个   在第num个led设置颜色值
{
  SetPixelColor(num, c);
}
void Move_one_ni(void) {         //向前移动一个led  循环   逆时针
  unsigned char i;
  unsigned char R;
  unsigned char G;
  unsigned char B;

  R = buf_R[numLEDs - 1];
  G = buf_G[numLEDs - 1];
  B = buf_B[numLEDs - 1];

  for (i = 0; i < numLEDs - 1; i++)
  {
    buf_R[numLEDs - i - 1] = buf_R[numLEDs - i - 2];
    buf_G[numLEDs - i - 1] = buf_G[numLEDs - i - 2];
    buf_B[numLEDs - i - 1] = buf_B[numLEDs - i - 2];
  }
  buf_R[0] = R;
  buf_G[0] = G;
  buf_B[0] = B;
}
void Move_one_shun(void) {    //向后移动一个led  循环   顺时针
  unsigned char i;
  unsigned char R;
  unsigned char G;
  unsigned char B;

  R = buf_R[0];
  G = buf_G[0];
  B = buf_B[0];

  for (i = 0; i < numLEDs - 1; i++)
  {
    buf_R[i] = buf_R[i + 1];
    buf_G[i] = buf_G[i + 1];
    buf_B[i] = buf_B[i + 1];
  }
  buf_R[49] = R;
  buf_G[49] = G;
  buf_B[49] = B;
}


void  initT0() {                           //初始化定时器0   高速pwm驱动黄白灯
  AUXR |= 0x80;  //1T
  TMOD = 0xf0;
  TH0 = 0xff;
  TL0 = 0x00;
  TF0 = 0;    //清除标记
  EA = 1;
  //ET0=1;     //中断 允许位   T0
  TR0 = 1;    //定时器0  开始计时
}
void allchange(unsigned int i) {           //七彩全亮   渐变
  unsigned char j;
  //PWM=255*light/100;         //??
  if (i <= PWM) {
    Gda = i;
    Rda = PWM - i;
    for (j = 0; j < numLEDs; j++) {
      buf_R[j] = Rda;
      buf_G[j] = Gda;
      buf_B[j] = 0;
    }
    show_ws2812();  //??????
    //delay(50);  //????
  } else if (i <= PWM * 2) {
    i = i - PWM;
    Bda = i;
    Gda = PWM - i; //????
    for (j = 0; j < numLEDs; j++) {
      buf_B[j] = Bda;
      buf_G[j] = Gda;
      buf_R[j] = 0;
    }
    show_ws2812();  //??????
    //delay(50);  //????
  } else if (i <= PWM * 3) {
    i = i - PWM * 2;
    Rda = i;
    Bda = PWM - i; //????
    for (j = 0; j < numLEDs; j++) {
      buf_B[j] = Bda;
      buf_R[j] = Rda;
      buf_G[j] = 0;
    }
    show_ws2812();  //??????
  }

}


void shun6led() {                      //6色灯模式  顺时针
  show_ws2812();  //显示
  Move_one_shun();    //顺时针移动一个
}
void ni6led() {     //6色灯 逆时针
  show_ws2812();  //??????
  Move_one_ni();
}
void shun6Wipe(unsigned int i) {     //6色   顺  逐渐填满
  if (i <= 50) {
    colorWipe(i, colour_3[0]);
    show_ws2812();
  } else   if (i <= 50 * 2) {
    i = i - 50;
    colorWipe(i, colour_3[1]);
    show_ws2812();
  } else   if (i <= 50 * 3) {
    i = i - 50 * 2;
    colorWipe(i, colour_3[2]);
    show_ws2812();
  } else   if (i <= 50 * 4) {
    i = i - 50 * 3;
    colorWipe(i, colour_3[3]);
    show_ws2812();
  } else   if (i <= 50 * 5) {
    i = i - 50 * 4;
    colorWipe(i, colour_3[4]);
    show_ws2812();
  } else   if (i <= 50 * 6) {
    i = i - 50 * 5;
    colorWipe(i, colour_3[5]);
    show_ws2812();
  }
}
void breath(unsigned int j) {    //呼吸灯模式
  unsigned int i;
  //  unsigned char temp;

  if (j <= 100)   for (i = 0; i < numLEDs; i++)
    {
      switch (color_num) {  //color_num  控制当前颜色
        case 0:    buf_R[i] = j;  buf_G[i] = buf_B[i] = 0; break;
        case 1:    buf_G[i] = j;  buf_R[i] = buf_B[i] = 0; break;
        case 2:    buf_B[i] = j;  buf_R[i] = buf_G[i] = 0; break;

        case 3:    buf_R[i] = buf_G[i] = j;  buf_B[i] = 0; break;
        case 4:    buf_B[i] = buf_G[i] = j;  buf_R[i] = 0; break;
        case 5:    buf_R[i] = buf_B[i] = j;  buf_G[i] = 0; break;
        case 6:    buf_R[i] = buf_G[i] = buf_B[i] = j; break;
        default:    break;
      }

    }
  else  if (j <= 100 * 2) {
    j = 200 - j;
    for (i = 0; i < numLEDs; i++)
    {
      switch (color_num) {
        case 0:    buf_R[i] = j;  buf_G[i] = buf_B[i] = 0; break;
        case 1:    buf_G[i] = j;  buf_R[i] = buf_B[i] = 0; break;
        case 2:    buf_B[i] = j;  buf_R[i] = buf_G[i] = 0; break;

        case 3:    buf_R[i] = buf_G[i] = j;  buf_B[i] = 0; break;
        case 4:    buf_B[i] = buf_G[i] = j;  buf_R[i] = 0; break;
        case 5:    buf_R[i] = buf_B[i] = j;  buf_G[i] = 0; break;
        case 6:    buf_R[i] = buf_G[i] = buf_B[i] = j; break;
        default:    break;
      }

    }
  }
  show_ws2812();          //发送灰度数据
}
void Twocolor(unsigned int a) {    //全盘有2种颜色的灯   1/4         输入以一个颜色序号
  unsigned int j;
  for (j = 0; j < 12; j++) { //
    color1led(j, colour_3[a]);
    color1led(j + 25, colour_3[a]);
  }
  for (j = 0; j < 13; j++) { //4种颜色
    color1led(j + 12, colour_3[a + 1]);
    color1led(j + 37, colour_3[a + 1]);
  }
  show_ws2812();  //??????
}


unsigned int time_temp = 0;       //用于定时器0的计数值
unsigned int wy_num = 3;          //用于黄白灯的占空比的值   越大越亮

unsigned char mode_rgb = 1;    //rgb的工作模式序号
unsigned char mode_wy = 1;     //黄白灯 工作模式序号
unsigned char work_flag = 0;    //0  待机    1工作
unsigned char now_flag = 2;      //当前在哪种显示        2黄白灯         3七彩灯
unsigned long times = 0;        //大循环中的数值变化，可用于非阻塞延时


void mode_init() {          //RGB七彩灯模式下   对应模式序号的初始化   数据缓存     用于解决开关机、模式切换时的显示初始化
  unsigned char i;
  times = 0;
  //if(mode_rgb==7) times=0;
  //if(mode_rgb==8) colorWipe(50,0);      //初始化 清空
  if (mode_rgb == 9) {
    /*light=50; */         for (i = 0; i < numLEDs; i++)  color1led(i, colour_3[i % 6]);
  }
  if (mode_rgb == 10) {
    colorWipe(50, 0); //初始化 清空
  }//times=0;}
  if (mode_rgb == 11) {
    PWM = 17;
    for (i = 0; i < PWM; i++) //全亮    七彩逐渐变化      改为  七彩盘  转
    {
      buf_R[i] = 102 - i * 6;
      buf_G[i] = i * 6;
      buf_B[i] = 0;
    }
    for (i = 0; i < PWM; i++) //????,????
    {
      buf_R[i + 17] = 0;
      buf_G[i + 17] = 102 - i * 6;
      buf_B[i + 17] = i * 6;
    }
    for (i = 0; i < PWM - 1; i++) //????,????
    {
      buf_R[i + 34] = i * 6;
      buf_G[i + 34] = 0;
      buf_B[i + 34] = 102 - i * 6;
    }
  }
  if (mode_rgb == 12) { //逆时针     水滴效果
    if (light < 60)light = 60;
    clear_Buff();
    color1led(10, colour_3[color_num]);         //颜色
    for (i = 0; i < 10; i++) {
      buf_R[i] = buf_R[10] * i / 10;
      buf_G[i] = buf_G[10] * i / 10;
      buf_B[i] = buf_B[10] * i / 10;
    }
  }
  if (mode_rgb == 13) { //顺时针     水滴效果
    if (light < 60)light = 60;
    clear_Buff();
    color1led(39, colour_3[color_num]);     //颜色
    for (i = 0; i < 10; i++) {
      buf_R[49 - i] = buf_R[39] * i / 10;
      buf_G[49 - i] = buf_G[39] * i / 10;
      buf_B[49 - i] = buf_B[39] * i / 10;
    }
  }
}
/*===============================主函数=======================================*/
void main()
{
  unsigned char i;
  unsigned char j;

  delay(200);
  clear_Buff();  //初始化缓存数组
  initT0();         //初始化定时器0
  light = 10;
  delay(20);
  P1M0 = 0x07; P1M1 = 0x00;   //强推挽输出    对3个led控制引脚

  key_u = 1;     //按键全部上拉电阻
  key_d = 1;
  key_o = 1;
  key_3 = 1;
  key_2 = 1;
  led = 0;   //亮灯   待机
  yellow_led = 0;    //灭
  write_led = 0;

  while (1)
  {
    times++;
    if (times >= 6000000) times = 0; //防溢出


    if (key_o == 0) { //工作or待机  按键
      delay(20);
      if (key_o == 0) {
        times = 0;
        work_flag = 1 - work_flag;
        if (work_flag == 1) { //工作
          led = 1;
          if (now_flag == 2)  ET0 = 1;
          if (now_flag == 3) {
            mode_init();

          }



        }
        else {
          led = 0;
          ET0 = 0;
          colorWipe(50, 0);
          show_ws2812();
          //ET0=1;
          yellow_led = 0;    //灭
          write_led = 0;
        }
        while (key_o == 0);
        delay(20);
      }
    }

    if (work_flag == 1) { //当前是工作状态下
      if (key_2 == 0) { //黄白灯  按键
        delay(20);
        if (key_2 == 0) {

          if (now_flag == 2) {
            mode_wy++;
            if (mode_wy >= 4) mode_wy = 1;
          }
          else {
            now_flag = 2;
            colorWipe(50, 0);
            show_ws2812();
          }
          ET0 = 1;

          while (key_2 == 0);
          delay(20);
        }
      }
      if (key_3 == 0) { //RGB七彩模式  按键
        delay(20);
        if (key_3 == 0) {

          if (now_flag == 3) {
            mode_rgb++;
            if (mode_rgb > 17) mode_rgb = 1;  //??
         
          }
          else {
            now_flag = 3;
            yellow_led = 0;    //灭
            write_led = 0;
          }
          ET0 = 0;
          mode_init();
          while (key_3 == 0);
          delay(20);
        }
      }

      if (key_u == 0) { //+++安吉那
        delay(20);
        if (key_u == 0) {

          if (now_flag == 2) {
            wy_num++;
            if (wy_num >= 15) wy_num = 15;  //??
          }
          if (now_flag == 3) {
            color_num++;
            if (color_num > 6) color_num = 0;
            light += 7;
            if (light > 100) light = 100;

            if (mode_rgb == 12) { //顺时针     水滴效果
              if (light < 60)light = 60;
              clear_Buff();
              color1led(10, colour_3[color_num]);         //颜色
              for (i = 0; i < 10; i++) {
                buf_R[i] = buf_R[10] * i / 10;
                buf_G[i] = buf_G[10] * i / 10;
                buf_B[i] = buf_B[10] * i / 10;
              }
            }
            if (mode_rgb == 13) {
              if (light < 60)light = 60;
              clear_Buff();
              color1led(39, colour_3[color_num]);     //颜色
              for (i = 0; i < 10; i++) {
                buf_R[49 - i] = buf_R[39] * i / 10;
                buf_G[49 - i] = buf_G[39] * i / 10;
                buf_B[49 - i] = buf_B[39] * i / 10;
              }
            }
          }

          while (key_u == 0);
          delay(20);
        }
      }

      if (key_d == 0) { //---按键
        delay(20);
        if (key_d == 0) {

          if (now_flag == 2) {
            wy_num--;
            if (wy_num <= 1) wy_num = 2;  //  最暗   不关闭
          }
          if (now_flag == 3) {
            if (color_num <= 0) color_num = 6;
            else color_num--;
            light -= 7;
            if (light <= 7) light = 7;

            if (mode_rgb == 12) { //顺时针     水滴效果   初始化数据
              if (light < 60)light = 60;
              clear_Buff();
              color1led(10, colour_3[color_num]);         //颜色
              for (i = 0; i < 10; i++) {
                buf_R[i] = buf_R[10] * i / 10;
                buf_G[i] = buf_G[10] * i / 10;
                buf_B[i] = buf_B[10] * i / 10;
              }
            }
            if (mode_rgb == 13) {     //初始化数据
              if (light < 60)light = 60;
              clear_Buff();
              color1led(39, colour_3[color_num]);     //颜色
              for (i = 0; i < 10; i++) {
                buf_R[49 - i] = buf_R[39] * i / 10;
                buf_G[49 - i] = buf_G[39] * i / 10;
                buf_B[49 - i] = buf_B[39] * i / 10;
              }
            }

          }

          while (key_d == 0);
          delay(20);
        }
      }





      if (now_flag == 3) { //七彩灯  模式下       共17中动画效果
        switch (mode_rgb) {
          case 1:  colorWipe(50, colour_3[0]);  show_ws2812();         break;  //单色
          case 2:  colorWipe(50, colour_3[1]);  show_ws2812();         break;
          case 3:  colorWipe(50, colour_3[2]);  show_ws2812();         break;
          case 4:  colorWipe(50, colour_3[3]);  show_ws2812();         break;
          case 5:  colorWipe(50, colour_3[4]);  show_ws2812();         break;
          case 6:  colorWipe(50, colour_3[5]);  show_ws2812();         break;
          case 7:  colorWipe(50, colour_3[times / 100 % 6]);  show_ws2812();         break; //6种颜色   跳变          亮暗
          case 8: PWM = 250 * light / 100;  allchange((times * 2 / (100 / light)) % (PWM * 3));  break; //全一样   七彩  变换    速度差不多了   亮暗
          case 9:  if (times % (16 - light / 7) == 0)  shun6led();      break; //6色灯交替  顺时针    速度   背后控制亮度
          case 10:  shun6Wipe(times / 8 % 300 + 1);  break; //6种颜色  转满                亮暗
          case 11:  if (times % (100 / light) == 0)  shun6led();      break; //彩虹  顺时针     速度
          case 12:  if (times % 8 == 0)  ni6led();      break; //水滴  ni时针        颜色     亮暗
          case 13:  if (times % 8 == 0)  shun6led();      break; //水滴  顺时针        颜色     亮暗
          case 14:  breath(times / 2 % 200);  break; //6种颜色  呼吸灯      颜色
          case 15:  Twocolor(0); break;     //红绿        亮暗
          case 16:  Twocolor(2); break;
          case 17:  Twocolor(4); break;

          default:   break;
        }


      }
      delay(30);







    }









  }
}
void T0int() interrupt 1       //定时器0    用于黄白灯  的亮度
{
  TH0 = 0xff;
  TL0 = 0x00;
  time_temp++;
  if (time_temp > 15)  time_temp = 1; //多个档

  if (mode_wy == 1) {  //白
    if (time_temp >= wy_num)  write_led = 0;
    else write_led = 1;

    yellow_led = 0;
  }
  if (mode_wy == 2) {  //黄
    if (time_temp >= wy_num)  yellow_led = 0;
    else yellow_led = 1;

    write_led = 0;
  }
  if (mode_wy == 3) {  //白 黄
    if (time_temp >= wy_num)  write_led = 0;
    else write_led = 1;
    if (time_temp >= wy_num)  yellow_led = 0;
    else yellow_led = 1;
  }
}