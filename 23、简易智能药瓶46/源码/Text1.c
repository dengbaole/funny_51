#include <reg51.h>
#include <intrins.h>
#include "uart.h"
#include "ds1302.h"
#include "lcd1602.h"

#define uchar unsigned char
#define uint  unsigned int        
//引脚定义								    
sbit A1=P1^0;  //D3	 共用	三组用于驱动3个 步进电机   
sbit B1=P1^1;
sbit C1=P1^2;
sbit D1=P1^3;  //A2	 共用	   加了上啦电阻
sbit B2=P1^4;
sbit C2=P1^5;
sbit D2=P1^6;  //A3	 共用
sbit B3=P1^7;
sbit C3=P3^2;

sbit buzzer  =  P2^0;   //蜂鸣器  低电平响
sbit key_setup=P2^4;	//按键  设置
sbit key_u=P2^3;		//按键  +
sbit key_d=P2^2;		//-
sbit key_move=P2^1;		// 右移
sbit key_ok=P3^6;		//按键  确定

uchar sec, min, hour, day, month, year; //对应ds1302读取的时间变量
uchar RXD_num = 0;          //串口读取的字符数  用于判断长命令的
uchar uart_flag = 0;          //串口读取长数据 是否完成
uchar uart_data[20];          //串口数据存放的数组
void get_showlcd();			//获取时间并显示在lcd上
#define step_delay 1	    //步进控制速度  数值越大越慢

void step_stop(){		   //步进电机  全部停止
   P1=0x00;
   C3=0;
}
void step1_shun(uint num){	 //步进1  顺时针转多少脉冲
   uint i;
   for(i=0;i<num;i++){
   A1=1;B1=1;C1=0;D1=0;
   delay_ms(step_delay);
   A1=0;B1=1;C1=1;D1=0;
   delay_ms(step_delay);
   A1=0;B1=0;C1=1;D1=1;
   delay_ms(step_delay);
   A1=1;B1=0;C1=0;D1=1;
   delay_ms(step_delay);
   }
   step_stop();
}
void step2_shun(uint num){	  //步进2
   uint i;
   for(i=0;i<num;i++){
   D1=1;B2=1;C2=0;D2=0;
   delay_ms(step_delay);
   D1=0;B2=1;C2=1;D2=0;
   delay_ms(step_delay);
   D1=0;B2=0;C2=1;D2=1;
   delay_ms(step_delay);
   D1=1;B2=0;C2=0;D2=1;
   delay_ms(step_delay);
   }
   step_stop();
}
void step3_shun(uint num){	// 步进3
   uint i;
   for(i=0;i<num;i++){
   D2=1;B3=1;C3=0;A1=0;
   delay_ms(step_delay);
   D2=0;B3=1;C3=1;A1=0;
   delay_ms(step_delay);
   D2=0;B3=0;C3=1;A1=1;
   delay_ms(step_delay);
   D2=1;B3=0;C3=0;A1=1;
   delay_ms(step_delay);
   }
   step_stop();
}
void step123(uchar which,uint num){	   //选择哪一个步进电机  转多少圈
   if(which==1) step1_shun(num*516);
   if(which==2) step2_shun(num*610);
   if(which==3) step3_shun(num*660);
}

//有关闹钟1、2、3的时、分、对应药的数量、闹钟打开or关闭
uchar hour123[]={11,12,13};		
uchar min123[]={1,2,3};
uchar num123[]={1,2,3};
uchar work123[]={1,1,1};	  //1 是打开    0是关闭

uchar work[2][5]={" off"," on "};	//对应闹钟开关
uchar address0[]={4,5,7,8,11,12};	//用于设置里 1602的显示位置	     那个设置界面的


void main(void)
{
  uchar mode_num=0;		//模式的值
  uint i=0;
  uchar wei1602=0;	    //用于设置闹钟 位选
  uchar buzzer_flag=1;  //用于 按键停止  闹钟响	  平时为1
	delay_ms(20);

  uart_init();            //串口初始化  波特率 9600
  lcd_init();             //lcd1602初始化
  ds1302_init();          //ds1302时钟模块的初始化
	step_stop();
	buzzer=0;
	delay_ms(100);
	buzzer=1;			  //蜂鸣器  不响
	 key_setup=1;		  //5个按键为上啦输入
	 key_u=1;
	 key_d=1;
	 key_move=1;
	 key_ok=1;

	write_com(0x80);        //lcd第一行位置
  write_com(0xc0);      //lcd第二行位置
  print_string("    Welcome     ");//lcd显示内容

   get_showlcd();		 //获取并显示时间
   //依次设置闹钟1、2、3 的时间为此时的1、2、3分钟后。方便演示，可以在硬件的按键进行修改
   if(min!=59){ min123[0]=min+1;hour123[0]=hour;	 }		
   else if(hour!=23) { min123[0]=0;hour123[0]=hour+1;	 }

   if(min!=58){ min123[1]=min+2;hour123[1]=hour;	 }
   else if(hour!=23) { min123[1]=0;hour123[1]=hour+1;	 }

   if(min!=57){ min123[2]=min+3;hour123[2]=hour;	 }
   else if(hour!=23) { min123[2]=0;hour123[2]=hour+1;	 }

   delay_ms(600);
	  	  write_com(0xc0);      //lcd第二行位置
  		  print_string("working:        ");//lcd显示  当前工作的闹钟序号
		  if(work123[0]==1){  write_com(0xc0+10);  print_string("1"); }
		  if(work123[1]==1){  write_com(0xc0+12);  print_string("2"); }
		  if(work123[2]==1){  write_com(0xc0+14);  print_string("3"); }

	while(1){	   //大循环

    if (uart_flag == 1) {	 //如果数据已经接收完成      可以使用串口波特率9600来直接修改实时时钟
      if (uart_data[13] == '*') {		   //如果第14个是‘*’ 表示修改ds1302时间  数据格式举例：“#200514120000*”
        ds1302_write(write_protect, 0x00);   //禁止写保护，允许写入数据
        ds1302_write(write_second, (uart_data[11] - 48) * 16 + uart_data[12] - 48); 
        ds1302_write(write_minute, (uart_data[9] - 48) * 16 + uart_data[10] - 48); 
        ds1302_write(write_hour, (uart_data[7] - 48) * 16 + uart_data[8] - 48); 
        ds1302_write(write_day, (uart_data[5] - 48) * 16 + uart_data[6] - 48);
        ds1302_write(write_month, (uart_data[3] - 48) * 16 + uart_data[4] - 48);
        ds1302_write(write_year, (uart_data[1] - 48) * 16 + uart_data[2] - 48);
        ds1302_write(write_protect, 0x80);   //允许写保护，禁止写入数据
        uart_send_str("change time ok!");	 //设置成功会发回字符串
      }
      for (i = 0; i < 20; i++) uart_data[i] = 0;   //清空接收数组缓存
      uart_flag = 0;
    }

    if (key_setup == 0) {			//按键判断  设置按键
      delay_ms(10);
      if (key_setup == 0){
	  	mode_num++;
		if(mode_num>3) mode_num=0;	//对应4种模式  3个闹钟
		if(mode_num!=0){		//模式0是  正常显示模式   1-3是设置1-3闹钟模式
		    wei1602=0;

			write_com(0x80);
			print_string("Seting alarm...");
			write_com(0xc0);
		
		  write_data(mode_num%10+48);	   //显示闹钟序号 时分 数量 是否工作
		  print_string(".  "); 
		  write_data(hour123[mode_num-1]/10%10+48); 
		  write_data(hour123[mode_num-1]%10+48);
		  write_data(':');
		  write_data(min123[mode_num-1]/10%10+48); 
		  write_data(min123[mode_num-1]%10+48);
		  print_string("  ");
		  write_data(num123[mode_num-1]%10+48);  
		  print_string(work[work123[mode_num-1]]);
		  print_string(" ");   



		  delay_ms(1100);	  //延时一段时间
		  write_com(0xc0+4); 
		  print_string("__:__  _ __ ");		 //清空显示设置  此时可以进行修改
		}	  
		else{
			write_com(0xc0);   //清空第二行
			print_string("                ");
		}
		  while(key_setup == 0); 
		  delay_ms(20);			 
	  }      
    }

    if (key_u == 0) {			//按键判断 +
      delay_ms(10);
      if (key_u == 0){
	    if(mode_num!=0){
		  switch(wei1602){		//根据 设置的数据位置  来限制数字的大小  避免超范围
			case 0:	hour123[mode_num-1]+=10;if(hour123[mode_num-1]>=30) hour123[mode_num-1]-=30;	 break;
			case 1:	if(hour123[mode_num-1]%10!=9)  hour123[mode_num-1]+=1; if(hour123[mode_num-1]>=24) hour123[mode_num-1]=23;	 break;
			case 2:	min123[mode_num-1]+=10; if(min123[mode_num-1]>=60) min123[mode_num-1]-=60;	 break;
			case 3:	if(min123[mode_num-1]%10!=9)   min123[mode_num-1]+=1; else min123[mode_num-1]-=9;	 break;
			case 4:	if(num123[mode_num-1]!=9)   num123[mode_num-1]+=1;  else num123[mode_num-1]-=9;  break;
			case 5:	if(work123[mode_num-1]!=1)  work123[mode_num-1]+=1;	else work123[mode_num-1]=0;  break;
			default:  break;					
	  	  }
		  switch(wei1602){	 //注意   没有break;	 显示出设置的数值
			case 5:	write_com(0xc0+address0[5]); print_string(work[work123[mode_num-1]]); 
			case 4:	write_com(0xc0+address0[4]); write_data(num123[mode_num-1]%10+48);
			case 3:	write_com(0xc0+address0[3]); write_data(min123[mode_num-1]%10+48);
			case 2:	write_com(0xc0+address0[2]); write_data(min123[mode_num-1]/10%10+48);
			case 1:	write_com(0xc0+address0[1]); write_data(hour123[mode_num-1]%10+48);	
			case 0:	write_com(0xc0+address0[0]); write_data(hour123[mode_num-1]/10%10+48);	
			default:  break;
	  	  }
		}	 
	      while(key_u == 0);
		  delay_ms(20);
	  }      
    }
    if (key_d == 0) {			//按键判断  -
      delay_ms(10);
      if (key_d == 0){
	   if(mode_num!=0){
		  switch(wei1602){		//根据 设置的数据位置  来限制数字的大小  避免超范围
			case 0:	if(hour123[mode_num-1]<10) hour123[mode_num-1]+=20;	else hour123[mode_num-1]-=10; break;
			case 1:	if(hour123[mode_num-1]%10!=0)  hour123[mode_num-1]-=1; 	 break;
			case 2:	if(min123[mode_num-1]<10) min123[mode_num-1]+=50; else min123[mode_num-1]-=10;	 break;
			case 3:	if(min123[mode_num-1]%10!=0) min123[mode_num-1]-=1; else min123[mode_num-1]+=9;	 break;
			case 4:	if(num123[mode_num-1]!=1)    num123[mode_num-1]-=1; else num123[mode_num-1]=9;  break;
			case 5:	if(work123[mode_num-1]!=0)  work123[mode_num-1]-=1;	else work123[mode_num-1]=1;  break;
			default:  break;					
	  	  }
		  switch(wei1602){	 //注意   没有break;   	 显示出设置的数值
			case 5:	write_com(0xc0+address0[5]); print_string(work[work123[mode_num-1]]); 
			case 4:	write_com(0xc0+address0[4]); write_data(num123[mode_num-1]%10+48);
			case 3:	write_com(0xc0+address0[3]); write_data(min123[mode_num-1]%10+48);
			case 2:	write_com(0xc0+address0[2]); write_data(min123[mode_num-1]/10%10+48);
			case 1:	write_com(0xc0+address0[1]); write_data(hour123[mode_num-1]%10+48);	
			case 0:	write_com(0xc0+address0[0]); write_data(hour123[mode_num-1]/10%10+48);	
			default:  break;
	  	  }
		}				 
	  	while(key_d == 0);
		delay_ms(20);
	  }      
    }
    if (key_move == 0) {			//按键判断  右移
      delay_ms(10);
      if (key_move == 0){
		wei1602++;
		if(wei1602>=6){		     //当超过设置的位数时，从头开始  重新设置
		  wei1602=0;
		  write_com(0xc0+4);print_string("__:__  _ __ ");	
		}		 
	  	while(key_move == 0);
		delay_ms(20);
	  }      
    }
    if (key_ok == 0) {			//按键判断  确定按键
      delay_ms(10);
      if (key_ok == 0){
	      mode_num=0;
		  buzzer_flag=0;	
	  	  write_com(0xc0);      //lcd第二行位置
  		  print_string("working:        ");//lcd显示内容	  工作的闹钟序号
		  if(work123[0]==1){  write_com(0xc0+10);  print_string("1"); }
		  if(work123[1]==1){  write_com(0xc0+12);  print_string("2"); }
		  if(work123[2]==1){  write_com(0xc0+14);  print_string("3"); }
					 
	  }      
    }



	if(mode_num==0){		//正常工作模式
		 get_showlcd();
	 	 delay_ms(20);

	  for(i=0;i<3;i++){		 //循环检测3个闹钟
	  	 if(work123[i]==1){  //如果 闹钟为开启  状态
		 	if((hour123[i]==hour)&&(min123[i]==min)){	 //判断时间到了
				if(sec==1) {
				  buzzer_flag=1;					//响			    
				  buzzer=0;delay_ms(100);buzzer=1;delay_ms(160);
				  buzzer=0;delay_ms(100);buzzer=1;delay_ms(160);
				  buzzer=0;delay_ms(100);buzzer=1;delay_ms(160);	
				  write_com(0xc0);      //lcd第二行位置
  		  		  print_string(" .ringing! num: ");//lcd显示内容   以及显示闹钟序号、对应药品数量
				  write_com(0xc0);  write_data(i+'1');	   
				  write_com(0xc0+15);  write_data(num123[i]+'0');
				  step123(i+1,num123[i]);  //步进	转对应圈数
				}
			  if(buzzer_flag==1){			//步进转结束后  间断响
			   if(sec%4==0)   buzzer=0;
			   if(sec%4==1)   buzzer=1;			  
			  }
			  else buzzer=1;
			if(sec==59){			 //重新设置蜂鸣器允许响的标志
			   buzzer=1;
			   buzzer_flag=1;
	  	  write_com(0xc0);      //lcd第二行位置
  		  print_string("working:        ");//lcd显示内容		显示工作的闹钟序号
		  if(work123[0]==1){  write_com(0xc0+10);  print_string("1"); }
		  if(work123[1]==1){  write_com(0xc0+12);  print_string("2"); }
		  if(work123[2]==1){  write_com(0xc0+14);  print_string("3"); }
			}			  					  
		   }

		 }
	  }
	}


	delay_ms(8);
	}	
}
void RX_int(void) interrupt 4     //串口接收中断服务函数   存放接收到的数据
{
  uchar data1;
  if (RI)	   //串口中断
  {
    RI = 0;
    data1 = SBUF;
    if (data1 == '#')  RXD_num = 0;	  //数据以‘#’开头 ，以‘*’结尾，不符合的不存放在数组里
    uart_data[RXD_num] = data1;
    RXD_num++;
    if (data1 == '*') {			//判断结尾
      RXD_num = 0;
      uart_flag = 1;			//完成数据接收 标志
      uart_send_str("get:");	//发回数据，表示收到，以便验证
      uart_send_str(uart_data);
    }
  }
}
void get_showlcd(){			  //获取ds1302的时间参数 并且显示在lcd上
      sec = ds1302_read(read_second);
      min = ds1302_read(read_minute);
      hour = ds1302_read(read_hour);
      day = ds1302_read(read_day);
      month = ds1302_read(read_month);
      year = ds1302_read(read_year);

      write_com(0x80);				//显示
      write_data(month / 10 % 10 + 48);
      write_data(month % 10 + 48);
      write_data('-');
      write_data(day / 10 % 10 + 48);
      write_data(day % 10 + 48);
      write_data(' ');
      write_data(hour / 10 % 10 + 48);
      write_data(hour % 10 + 48);
      write_data(':');
      write_data(min / 10 % 10 + 48);
      write_data(min % 10 + 48);
      write_data(':');
      write_data(sec / 10 % 10 + 48);
      write_data(sec % 10 + 48);

	  write_data(' ');
	  write_data(' ');
}