#ifndef DS1302_H
#define DS1302_H
		//快速修改：
sbit CLK_IO=P3^5;
sbit DAT_IO=P3^4;
sbit CE_RST=P3^3; 
/*
void ds1302_write(uchar address,uchar data0);
void ds1302_init(void);
uchar ds1302_read(uchar address);
void ds1302_reset(void);
*/

//读写状态地址和保护状态地址
#define write_second    0x80
#define write_minute    0x82
#define write_hour      0x84
#define write_day       0x86
#define write_month     0x88
#define write_year      0x8c
#define read_second     0x81
#define read_minute     0x83
#define read_hour       0x85
#define read_day        0x87
#define read_month      0x89
#define read_year       0x8d
#define write_protect   0x8E      //0x80,开启保护，无法写入数据。

void ds1302_write(uchar address,uchar data0)
{
	uchar i,temp;
	CE_RST = 0;        //CE拉低 数据传送终止
	CLK_IO = 0;       //清零时钟总线
	CE_RST = 1;         // CE拉高，逻辑控制有效
		 
		 //发送地址
	for(i=0;i<8;i++)
	{
		CLK_IO = 0;           //拉低
		temp=address;
		DAT_IO=(temp&0x01);   //每次传输低字节
			 
		address>>=1;      // 右移一位
		CLK_IO = 1;          //上升沿，写锁存
	}
		 
		 //发送数据
	for(i=0;i<8;i++)
	{
		CLK_IO = 0;
		temp=data0;
		DAT_IO=(temp&0x01);
		data0>>=1;
		CLK_IO = 1;
	}
	CE_RST = 0;   //中止数据传输
}
void ds1302_init(void)
{
	CE_RST = 0;
	CLK_IO = 0;
	DAT_IO = 0;
	//ds1302_write(0x8e, 0x00);   //允许写操作 
   // ds1302_write(0x80, 0x00);   //时钟启动 
   // ds1302_write(0x90, 0xa6);   //一个二极管＋4K电阻充电 
	ds1302_write(write_protect,0x80);  //开启写保护，即禁止写入数据
}



uchar ds1302_read(uchar address)
{

        //注意数据口IO连接的引脚 如果不一样要改改下面的DDRn和PINn 不然读取不了
	uchar i,temp,data0=0,dat1,dat2;
	CE_RST = 0;
	CLK_IO = 0;  //时钟线初始化
	CE_RST = 1;
		  
		  //发送地址
	for(i=0;i<8;i++)
	{
	    CLK_IO = 0;
		temp=address;
		DAT_IO=(temp&0x01);   //每次传送低字节
		address>>=1;     //右移一位
		CLK_IO = 1;
	}
		  
		  // 读取数字
	for(i=0;i<8;i++)
	{

		if(DAT_IO!=0)data0|=0x80;
		else data0&=~(0x80);
		CLK_IO = 1;
		data0>>=1;
		CLK_IO = 0;
	}

	CE_RST = 0;
	
	dat2=data0/16;  //16进制转换成10进制
	dat1=data0%16;
	data0=dat1+dat2*10;
	return(data0);   
}

void ds1302_reset(void)
{
     ds1302_write(write_protect,0x00);  //禁止写保护，允许写入数据
	ds1302_write(write_second,0x00);   //秒位初始化,开始计时
	ds1302_write(write_minute,0x09);   //分钟初始化
	ds1302_write(write_hour,0x21);     //小时初始化
	ds1302_write(write_day,0x01);
	ds1302_write(write_month,0x08);
	ds1302_write(write_year,0x20);
	ds1302_write(write_protect,0x80);  //允许写保护，禁止写入数据
}
/*
char second=0,resecond=0,minute=0,hour=0,day=0,month=0,year=0;
void read_time(void)
{
    second=ds1302_read(read_second);
	minute=ds1302_read(read_minute);
	hour=ds1302_read(read_hour);
	day=ds1302_read(read_day);
	month=ds1302_read(read_month);
	year=ds1302_read(read_year); 
}
*/
/*
void ds1302_init(void);                      //引脚，写保护
void ds1302_write(uchar address,uchar data0); //  写入数据
uchar ds1302_read(uchar address);            //读取 数据
void ds1302_reset(void);                     //修改数据（时间）
  second=ds1302_read(read_second);
	minute=ds1302_read(read_minute);
	hour=ds1302_read(read_hour);
	day=ds1302_read(read_day);
	month=ds1302_read(read_month);
	year=ds1302_read(read_year); 
*/


 // CE即为RST  若CE为低时，数据传送中止，为高时，可以进行数据传送










#endif 