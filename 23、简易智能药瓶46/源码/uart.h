#ifndef  UART_H
#define  UART_H
#define uchar unsigned char
/*
void uart_init(void);
void uart_send_char(uchar i);  
void uart_send_str(char *zifu);        //发送 字符串
void uart_send(int num);			    //发送4位数字
*/


/*
晶振11.0592M,波特率9600
定时器模式：T1八位自动重装
串口工作模式：1
中断：开串口中断，关定时器1中断
 */
void uart_init(void)
{
	TMOD&=0x0f;
	TMOD|=0x20;    //设置T1为八位自动重装 SM0=1，SM1=0！，【GATE=0（不受外部中断限制）】，【C/T=0(定时器模式)】
	TH1=0xfd;	//晶振11.0592M,波特率9600    =fs/12/(256-TH1)/32
	TL1=0xfd;		
	TR1=1;	//启动定时器1
 
	SM0=0;	//设置串口工作模式为模式1
	SM1=1;	
	EA=1;	//打开总总中断
	ES=1;	//打开串口中断
	REN=1;	//允许串口接收
}
void uart_send_char(uchar i)  
{
	ES=0;  //关闭串口中断
	SBUF=i;
	while(!TI);	//等待发送完毕
	TI=0;
	ES=1;
}
void uart_send_str(char *zifu)        //发送 字符串
{
	int i=0;
	while(zifu[i]!='\0')
	{
		uart_send_char(zifu[i]);
		i++;
	}
}
void uart_send(int num)    //发送4位数字
{
	//uart_send_char(num/10000%10+48);
	uart_send_char(num/1000%10+48);
	uart_send_char(num/100%10+48);
	uart_send_char(num/10%10+48);
	uart_send_char(num%10+48);
	uart_send_char(13);
}

/*
void RX_int(void) interrupt 4     //接收单个字符        对
{
	char i;
        if(RI)
        {
                RI = 0;
                i = SBUF;
			 uart_send_char(i+1); 
			 uart_send_char(13); 
	   }
}
uchar RXD_num = 0;          //串口读取的字符数  用于判断长命令的
uchar uart_flag = 0;        //串口读取长数据 是否完成
uchar uart_data[20];        //串口数据存放的数组
void RX_int(void) interrupt 4     //串口接收中断服务函数   存放接收到的数据
{
  uchar data1;
  if (RI)    //串口中断
  {
    RI = 0;
    data1 = SBUF;
    if (data1 == '#') {
      RXD_num = 0;    //数据以‘#’开头 ，以‘*’结尾，不符合的不存放在数组里
    }
    uart_data[RXD_num] = data1;
    RXD_num++;
    if (data1 == '*') {     //判断结尾
      RXD_num = 0;
      uart_flag = 1;      //完成数据接收 标志
    }
  }
}
if (uart_flag == 1) {	 //如果数据已经接收完成      数组里："#2*"
      if (uart_data[2] == '*') {	 //如果数据第三个结束  表示控制继电器
      }
      for (i = 0; i < 20; i++) uart_data[i] = 0;   //清空接收数组缓存
      uart_flag = 0;
}
*/
#endif 