/***************   writer:shopping.w   ******************/
#include <reg52.h>
#include "lcd1602.h"
#define uint unsigned int
#define uchar unsigned char

//引脚定义
sbit OE  = P2^5;
sbit EOC = P2^4;
sbit ST  = P2^7;
sbit CLK = P2^6;
sbit PWM_pin = P1^0;

sbit human_pin = P1^1;	//热释放人体感应 引脚
sbit key1 = P2^0;		//自动手动  
sbit key2 = P2^1;		//-
sbit key3 = P2^2;		//+

//变量
uchar PWM_num=100;	//用于调整PWM占空比	  显示的
uchar PWM_num1=0;	//驱动引脚的
uchar PWM_temp=0;   //用于计算中断数
char key1_flag=0;   //0 为自动   1为手动
uchar get_value_flag=0;

void DelayMS(uint ms)
{
 	uchar i;
	while(ms--)
	{
	 	for(i=0;i<100;i++);
	}
}

void Display_Result(uchar d)
{
	write_com(0xc0);
	write_data(d/100%10+'0');
	write_data(d/10%10+'0');
	write_data(d%10+'0');
}
uchar GetValue(void){
	uchar num;
	ST = 0;
	ST = 1;
	ST = 0;
	while(EOC == 0);
	OE = 1;
	num=P0;
	//Display_Result(PWM_num);
	OE = 0;
	return num;
}

void main()
{
	int i=0;
 	TMOD = 0x02;	   //模式2 自动重载  8位
	//TH0  = 0x14;		 //0x14  255us
	TH0  = 0xee;	   //频率要高
	TL0  = 0x00;
	//IE   = 0x82;	   //0b1000 0010	  EA  ET0	总中断、定时器0中断
	EA=1;
	ET0=1;
	TR0  = 1;		   //定时器0开关打开

	OE  =  1;
	EOC  =  1;
	ST  =  1;
	CLK  =  1;

	key1=1;
	key2=1;
	key3=1;
	human_pin=0;

	lcd_init();             //lcd1602初始化
	write_com(0x80);        //lcd第一行位置
	print_string("welcome");//lcd显示内容
	//write_com(0xc0);      //lcd第二行位置
	write_com(0xc6);
	print_string("Auto");

	while(1)
	{
		if(human_pin==1){
		
			if(key1==0){
				DelayMS(1);
				if(key1==0){
					key1_flag=1-key1_flag;
					write_com(0xc6);
					if(key1_flag==0){	
						print_string("Auto");
					}
					else print_string("Hand"); 
					while(key1==0);	
				}
			}
			if(key1_flag==0){  //自动
				if(get_value_flag==1){
				    //PWM_num=0;
					//for(i=0;i<2;i++)  PWM_num+=GetValue()/2;
					PWM_num=GetValue();
					get_value_flag=0;
				} 
				//DelayMS(2);

			}
			else{
				if(key2==0){
					DelayMS(1);
					if(key2==0){	 //-
						if(PWM_num<=245) PWM_num+=10;
						else PWM_num=255; 
					}
					while(key2==0);
				}
				if(key3==0){
					DelayMS(1);
					if(key3==0){	 //+
						if(PWM_num>=10) PWM_num-=10;	
						else  PWM_num=0;
					}
					while(key3==0);
				}
			}
			Display_Result(PWM_num);

			//DelayMS(1);
		}
		else{
			PWM_num=255;
			//PWM_num1=200;
			write_com(0xc0);      //lcd第二行位置
			print_string("          ");//lcd显示内容
		}
		//if(PWM_num<200)	PWM_num1=PWM_num;   //亮度很高了，就直接不亮灯
		//if(PWM_num<10)	PWM_num1=0;   //很暗，就全亮把
		PWM_num1=PWM_num;
	}
}

void Timer0_INT() interrupt 1
{
 	CLK = !CLK;
	if(PWM_temp>=255){ PWM_temp=0; get_value_flag=1;}
	else PWM_temp++;
	if(PWM_temp<PWM_num1)   PWM_pin=0;
	else PWM_pin=1;	
}