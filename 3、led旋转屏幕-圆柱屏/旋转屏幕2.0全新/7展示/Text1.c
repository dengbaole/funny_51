#include <reg52.h>
sbit KEY=P3^6;	   			    /*定位红外接收引脚*/

sbit p10=P1^0;
sbit p11=P1^1;
sbit p12=P1^2;
sbit p13=P1^3;
sbit p14=P1^4; 
sbit p17=P1^7; 
code unsigned  int zi0[] = {     /*欢迎您，志宇*/ 
       0x00,0x00,    
/*欢*/ 0x20,0x08,0x2C,0x10,0x23,0x60,0x20,0x80,
      0x23,0x41,0x3C,0x31,0x04,0x02,0x08,0x0C,
      0xF0,0x30,0x17,0xC0,0x10,0x30,0x10,0x08,
      0x14,0x06,0x18,0x03,0x00,0x02,0x00,0x00,

/*迎*/ 0x02,0x00,0x42,0x02,0x22,0x04,0x13,0xF8,
      0x00,0x04,0x3F,0xE2,0x20,0x42,0x40,0x82,
      0x41,0x02,0x3F,0xFE,0x20,0x02,0x20,0x42,
      0x20,0x22,0x7F,0xC6,0x20,0x04,0x00,0x00,

/*您*/ 0x02,0x02,0x04,0x0C,0x08,0x00,0x3F,0xEE,
      0xC4,0x01,0x08,0x81,0x31,0x11,0xE6,0x49,
      0x20,0x2D,0x2F,0xC1,0x20,0x01,0x22,0x07,
      0x29,0x00,0x31,0x88,0x00,0x06,0x00,0x00,

/*，*/ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x62,
      0x00,0xF4,0x00,0xF8,0x00,0x60,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*志*/ 0x10,0x04,0x10,0x18,0x11,0x00,0x11,0x3C,
      0x11,0x02,0x11,0x02,0x11,0x42,0xFF,0x22,
      0x11,0x32,0x11,0x02,0x11,0x02,0x13,0x0E,
      0x11,0x00,0x30,0x10,0x10,0x0C,0x00,0x00,

/*宇*/ 0x08,0x00,0x30,0x80,0x20,0x80,0x24,0x80,
      0x24,0x80,0x24,0x82,0xA4,0x81,0x67,0xFE,
      0x24,0x80,0x24,0x80,0x2C,0x80,0x24,0x80,
      0x21,0x80,0x28,0x80,0x30,0x00,0x00,0x00,

/*空白*/0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
 };      

code unsigned  int zi1[] = {     /*我们的口号是：零八零八，意气风发，奋力拼搏，勇创最佳！*/ 
       0x00,0x00,   
/*我*/ 0x04,0x00,0x24,0x10,0x24,0x12,0x24,0x21,
      0x7F,0xFE,0xC4,0x40,0x44,0x84,0x04,0x08,
      0xFF,0x90,0x04,0x60,0x44,0x58,0x35,0x86,
      0x04,0x01,0x0C,0x07,0x04,0x00,0x00,0x00,

/*们*/ 0x02,0x00,0x04,0x00,0x1F,0xFF,0xE0,0x00,
      0x00,0x00,0x1F,0xFF,0x40,0x00,0x20,0x00,
      0x10,0x00,0x20,0x00,0x20,0x00,0x20,0x02,
      0x20,0x01,0x7F,0xFE,0x20,0x00,0x00,0x00,

/*的*/ 0x00,0x00,0x1F,0xFE,0x30,0x84,0xD0,0x84,
      0x10,0x84,0x10,0x84,0x3F,0xFE,0x14,0x00,
      0x09,0x00,0xF0,0x80,0x10,0xC2,0x10,0x01,
      0x10,0x02,0x3F,0xFC,0x10,0x00,0x00,0x00,

/*口*/ 0x00,0x00,0x00,0x00,0x3F,0xFC,0x20,0x08,
      0x20,0x08,0x20,0x08,0x20,0x08,0x20,0x08,
      0x20,0x08,0x20,0x08,0x20,0x08,0x20,0x08,
      0x7F,0xFC,0x20,0x00,0x00,0x00,0x00,0x00,

/*号*/ 0x01,0x00,0x01,0x00,0x01,0x00,0x7D,0x20,
      0x45,0x60,0x45,0xA0,0x45,0x20,0x45,0x20,
      0x45,0x22,0x45,0x21,0x45,0x22,0xFD,0x7C,
      0x41,0x20,0x03,0x00,0x01,0x00,0x00,0x00,

/*是*/ 0x00,0x80,0x00,0x81,0x00,0x82,0x00,0x84,
      0xFE,0xB8,0x92,0x84,0x92,0x82,0x92,0xFE,
      0x92,0x91,0x92,0x91,0xFE,0xB1,0x00,0x91,
      0x00,0x81,0x01,0x83,0x00,0x82,0x00,0x00,

/*：*/ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x01,0x8C,0x03,0xDE,0x03,0xDE,0x01,0x8C,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*零*/ 0x08,0x20,0x30,0x20,0x20,0x40,0xAA,0x40,
      0xAA,0x90,0xAA,0x90,0xA1,0x54,0xFE,0x32,
      0xA1,0x15,0xAA,0x98,0xAA,0x90,0xAA,0x40,
      0x20,0x40,0x28,0x20,0x30,0x20,0x00,0x00,

/*八*/ 0x00,0x00,0x00,0x02,0x00,0x04,0x00,0x18,
      0x00,0xE0,0x3F,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x7F,0x00,0x00,0xE0,0x00,0x10,
      0x00,0x0C,0x00,0x06,0x00,0x04,0x00,0x00,

/*零*/ 0x08,0x20,0x30,0x20,0x20,0x40,0xAA,0x40,
      0xAA,0x90,0xAA,0x90,0xA1,0x54,0xFE,0x32,
      0xA1,0x15,0xAA,0x98,0xAA,0x90,0xAA,0x40,
      0x20,0x40,0x28,0x20,0x30,0x20,0x00,0x00,

/*八*/ 0x00,0x00,0x00,0x02,0x00,0x04,0x00,0x18,
      0x00,0xE0,0x3F,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x7F,0x00,0x00,0xE0,0x00,0x10,
      0x00,0x0C,0x00,0x06,0x00,0x04,0x00,0x00,

/*，*/ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x62,
      0x00,0xF4,0x00,0xF8,0x00,0x60,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*意*/ 0x08,0x02,0x08,0x0C,0x48,0x00,0x4B,0xEE,
      0x6A,0xA1,0x5A,0xA1,0x4A,0xB1,0xCA,0xA9,
      0x4A,0xAD,0x5A,0xA1,0x6A,0xA1,0xCF,0xE7,
      0x4A,0x00,0x18,0x08,0x08,0x06,0x00,0x00,

/*气*/ 0x02,0x00,0x04,0x00,0x1A,0x00,0xE2,0x00,
      0x2A,0x00,0x2A,0x00,0x2A,0x00,0x2A,0x00,
      0x2A,0x00,0x2A,0x00,0x2B,0xF8,0x28,0x04,
      0x60,0x02,0x20,0x01,0x00,0x0E,0x00,0x00,

/*风*/ 0x00,0x01,0x00,0x02,0x7F,0xFC,0x40,0x04,
      0x50,0x08,0x48,0x10,0x46,0x60,0x41,0x80,
      0x46,0x60,0x58,0x18,0x40,0x00,0xFF,0xFC,
      0x40,0x02,0x00,0x01,0x00,0x07,0x00,0x00,

/*发*/ 0x00,0x02,0x08,0x04,0x18,0x09,0x68,0x11,
      0x08,0x62,0x09,0x82,0x0F,0xC4,0xF9,0x28,
      0x09,0x10,0x09,0x28,0x49,0x44,0x29,0x84,
      0x08,0x02,0x18,0x03,0x08,0x02,0x00,0x00,

/*，*/ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x62,
      0x00,0xF4,0x00,0xF8,0x00,0x60,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*奋*/ 0x10,0x80,0x10,0x80,0x11,0x00,0x11,0xFF,
      0x12,0x92,0x14,0x92,0x18,0x92,0xF0,0xFE,
      0x18,0x92,0x14,0x92,0x12,0x92,0x11,0xFF,
      0x11,0x00,0x31,0x80,0x11,0x00,0x00,0x00,

/*力*/ 0x00,0x00,0x08,0x01,0x08,0x01,0x08,0x01,
      0x08,0x06,0x08,0x18,0xFF,0xE0,0x08,0x00,
      0x08,0x04,0x08,0x02,0x08,0x01,0x08,0x02,
      0x1F,0xFC,0x08,0x00,0x00,0x00,0x00,0x00,

/*拼*/ 0x08,0x40,0x08,0x42,0x08,0x81,0xFF,0xFE,
      0x09,0x00,0x0A,0x81,0x90,0x82,0x5F,0xFC,
      0x30,0x80,0x10,0x80,0x30,0x80,0x5F,0xFF,
      0xD0,0x80,0x11,0x80,0x00,0x80,0x00,0x00,

/*搏*/ 0x08,0x40,0x08,0x42,0x08,0x81,0xFF,0xFE,
      0x09,0x10,0x20,0x10,0x2F,0xD8,0x2A,0x94,
      0x2A,0x90,0xFF,0xD2,0x2A,0x91,0xAA,0xBE,
      0x6F,0xD0,0x20,0x10,0x00,0x10,0x00,0x00,

/*，*/ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x62,
      0x00,0xF4,0x00,0xF8,0x00,0x60,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*勇*/ 0x00,0x00,0x00,0x21,0x9F,0xE1,0x95,0x22,
      0x95,0x22,0x95,0x24,0xD5,0x38,0xBF,0xE0,
      0xB5,0x20,0xD5,0x22,0x95,0x21,0x95,0x62,
      0x3F,0xBC,0x10,0x00,0x00,0x00,0x00,0x00,

/*创*/ 0x01,0x00,0x02,0x00,0x0F,0xFC,0x32,0x02,
      0xC2,0x42,0x22,0x22,0x13,0xC2,0x08,0x0E,
      0x0C,0x00,0x00,0x00,0x1F,0xE0,0x00,0x02,
      0x00,0x01,0xFF,0xFE,0x00,0x00,0x00,0x00,

/*最*/ 0x02,0x04,0x02,0x04,0x03,0xFC,0xFA,0xA8,
      0xAA,0xA8,0xAA,0xA8,0xAB,0xFF,0xAA,0x81,
      0xAA,0xE2,0xAA,0x94,0xAA,0x88,0xFA,0x94,
      0x02,0xA6,0x06,0xC3,0x02,0x02,0x00,0x00,

/*佳*/ 0x01,0x00,0x02,0x00,0x04,0x00,0x1F,0xFF,
      0xE2,0x02,0x12,0x22,0x12,0x22,0x12,0x22,
      0x12,0x22,0xFF,0xFE,0x12,0x22,0x12,0x22,
      0x32,0x62,0x16,0x26,0x02,0x02,0x00,0x00,

/*！*/ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x3E,0x08,0x7F,0xDC,
      0x7F,0xDC,0x3E,0x08,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*空白*/0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

code unsigned  int zi2[] = {    /***********只需更改数组和z的值*****/
       0x00,0x00,		  /*从前有座山，山里有座庙，庙里有个老和尚和小和尚，老和尚对小和尚说：*/
/*从*/ 0x00,0x01,0x00,0x02,0x00,0x0C,0x00,0x30,
      0x7F,0xC0,0x00,0x21,0x00,0x12,0x00,0x0C,
      0x00,0x18,0x01,0xE0,0x7E,0x00,0x01,0xE0,
      0x00,0x1C,0x00,0x07,0x00,0x02,0x00,0x00,

/*前*/ 0x10,0x00,0x10,0x00,0x17,0xFF,0x94,0x90,
      0x54,0x92,0x74,0x91,0x17,0xFE,0x10,0x00,
      0x10,0x00,0x33,0xF0,0x50,0x02,0xD0,0x01,
      0x17,0xFE,0x30,0x00,0x10,0x00,0x00,0x00,

/*有*/ 0x20,0x20,0x20,0x40,0x20,0x80,0x21,0x00,
      0x27,0xFF,0x3C,0x90,0xE4,0x90,0x24,0x90,
      0x24,0x90,0x24,0x92,0x24,0x91,0x2F,0xFE,
      0x24,0x00,0x60,0x00,0x20,0x00,0x00,0x00,

/*座*/ 0x00,0x02,0x00,0x0C,0x3F,0xF2,0x20,0x42,
      0x21,0x92,0x2E,0x12,0x21,0x12,0xA0,0xD2,
      0x7F,0xFE,0x20,0x52,0x21,0x92,0x2E,0x32,
      0x21,0x12,0x60,0xC6,0x20,0x02,0x00,0x00,

/*山*/ 0x00,0x00,0x0F,0xFE,0x00,0x04,0x00,0x04,
      0x00,0x04,0x00,0x04,0x00,0x04,0xFF,0xFC,
      0x00,0x04,0x00,0x04,0x00,0x04,0x00,0x04,
      0x00,0x04,0x0F,0xFE,0x00,0x00,0x00,0x00,

/*，*/ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x62,
      0x00,0xF4,0x00,0xF8,0x00,0x60,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*山*/ 0x00,0x00,0x0F,0xFE,0x00,0x04,0x00,0x04,
      0x00,0x04,0x00,0x04,0x00,0x04,0xFF,0xFC,
      0x00,0x04,0x00,0x04,0x00,0x04,0x00,0x04,
      0x00,0x04,0x0F,0xFE,0x00,0x00,0x00,0x00,

/*里*/ 0x00,0x02,0x00,0x12,0x7F,0x92,0x48,0x92,
      0x48,0x92,0x48,0x92,0x48,0x92,0x7F,0xFE,
      0x48,0x92,0x48,0x92,0x48,0x92,0x48,0x92,
      0xFF,0xB2,0x40,0x16,0x00,0x02,0x00,0x00,

/*有*/ 0x20,0x20,0x20,0x40,0x20,0x80,0x21,0x00,
      0x27,0xFF,0x3C,0x90,0xE4,0x90,0x24,0x90,
      0x24,0x90,0x24,0x92,0x24,0x91,0x2F,0xFE,
      0x24,0x00,0x60,0x00,0x20,0x00,0x00,0x00,

/*座*/ 0x00,0x02,0x00,0x0C,0x3F,0xF2,0x20,0x42,
      0x21,0x92,0x2E,0x12,0x21,0x12,0xA0,0xD2,
      0x7F,0xFE,0x20,0x52,0x21,0x92,0x2E,0x32,
      0x21,0x12,0x60,0xC6,0x20,0x02,0x00,0x00,

/*庙*/ 0x00,0x02,0x00,0x0C,0x3F,0xF0,0x20,0x00,
      0x23,0xFF,0x22,0x22,0x22,0x22,0xA2,0x22,
      0x6F,0xFE,0x22,0x22,0x22,0x22,0x22,0x22,
      0x27,0xFF,0x62,0x00,0x20,0x00,0x00,0x00,

/*，*/ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x62,
      0x00,0xF4,0x00,0xF8,0x00,0x60,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*庙*/ 0x00,0x02,0x00,0x0C,0x3F,0xF0,0x20,0x00,
      0x23,0xFF,0x22,0x22,0x22,0x22,0xA2,0x22,
      0x6F,0xFE,0x22,0x22,0x22,0x22,0x22,0x22,
      0x27,0xFF,0x62,0x00,0x20,0x00,0x00,0x00,

/*里*/ 0x00,0x02,0x00,0x12,0x7F,0x92,0x48,0x92,
      0x48,0x92,0x48,0x92,0x48,0x92,0x7F,0xFE,
      0x48,0x92,0x48,0x92,0x48,0x92,0x48,0x92,
      0xFF,0xB2,0x40,0x16,0x00,0x02,0x00,0x00,

/*有*/ 0x20,0x20,0x20,0x40,0x20,0x80,0x21,0x00,
      0x27,0xFF,0x3C,0x90,0xE4,0x90,0x24,0x90,
      0x24,0x90,0x24,0x92,0x24,0x91,0x2F,0xFE,
      0x24,0x00,0x60,0x00,0x20,0x00,0x00,0x00,

/*个*/ 0x01,0x00,0x01,0x00,0x02,0x00,0x04,0x00,
      0x08,0x00,0x10,0x00,0x20,0x00,0xC7,0xFF,
      0x20,0x00,0x10,0x00,0x08,0x00,0x04,0x00,
      0x02,0x00,0x03,0x00,0x02,0x00,0x00,0x00,

/*老*/ 0x02,0x02,0x02,0x04,0x12,0x08,0x12,0x10,
      0x12,0x20,0x12,0x7E,0xFE,0x89,0x13,0x09,
      0x32,0x11,0x16,0x11,0x0A,0x21,0x12,0x61,
      0x32,0x01,0x06,0x07,0x02,0x00,0x00,0x00,

/*和*/ 0x04,0x08,0x24,0x10,0x24,0x60,0x25,0x80,
      0x3F,0xFF,0x44,0x80,0xCC,0x60,0x44,0x00,
      0x0F,0xFC,0x08,0x08,0x08,0x08,0x08,0x08,
      0x08,0x08,0x1F,0xFC,0x08,0x00,0x00,0x00,

/*尚*/ 0x00,0x00,0x07,0xFF,0x44,0x00,0x24,0x00,
      0x34,0xFC,0x04,0x88,0x04,0x88,0xFC,0x88,
      0x04,0x88,0x04,0x88,0x15,0xFC,0x24,0x82,
      0x64,0x01,0x0F,0xFE,0x04,0x00,0x00,0x00,

/*和*/ 0x04,0x08,0x24,0x10,0x24,0x60,0x25,0x80,
      0x3F,0xFF,0x44,0x80,0xCC,0x60,0x44,0x00,
      0x0F,0xFC,0x08,0x08,0x08,0x08,0x08,0x08,
      0x08,0x08,0x1F,0xFC,0x08,0x00,0x00,0x00,

/*小*/ 0x00,0x00,0x00,0x10,0x00,0x20,0x00,0x40,
      0x01,0x80,0x06,0x02,0x00,0x01,0xFF,0xFE,
      0x00,0x00,0x04,0x00,0x02,0x00,0x01,0x00,
      0x00,0x80,0x00,0x60,0x00,0x00,0x00,0x00,

/*和*/ 0x04,0x08,0x24,0x10,0x24,0x60,0x25,0x80,
      0x3F,0xFF,0x44,0x80,0xCC,0x60,0x44,0x00,
      0x0F,0xFC,0x08,0x08,0x08,0x08,0x08,0x08,
      0x08,0x08,0x1F,0xFC,0x08,0x00,0x00,0x00,

/*尚*/ 0x00,0x00,0x07,0xFF,0x44,0x00,0x24,0x00,
      0x34,0xFC,0x04,0x88,0x04,0x88,0xFC,0x88,
      0x04,0x88,0x04,0x88,0x15,0xFC,0x24,0x82,
      0x64,0x01,0x0F,0xFE,0x04,0x00,0x00,0x00,

/*，*/ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x62,
      0x00,0xF4,0x00,0xF8,0x00,0x60,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,


/*老*/ 0x02,0x02,0x02,0x04,0x12,0x08,0x12,0x10,
      0x12,0x20,0x12,0x7E,0xFE,0x89,0x13,0x09,
      0x32,0x11,0x16,0x11,0x0A,0x21,0x12,0x61,
      0x32,0x01,0x06,0x07,0x02,0x00,0x00,0x00,

/*和*/ 0x04,0x08,0x24,0x10,0x24,0x60,0x25,0x80,
      0x3F,0xFF,0x44,0x80,0xCC,0x60,0x44,0x00,
      0x0F,0xFC,0x08,0x08,0x08,0x08,0x08,0x08,
      0x08,0x08,0x1F,0xFC,0x08,0x00,0x00,0x00,

/*尚*/ 0x00,0x00,0x07,0xFF,0x44,0x00,0x24,0x00,
      0x34,0xFC,0x04,0x88,0x04,0x88,0xFC,0x88,
      0x04,0x88,0x04,0x88,0x15,0xFC,0x24,0x82,
      0x64,0x01,0x0F,0xFE,0x04,0x00,0x00,0x00,

/*对*/ 0x10,0x04,0x12,0x08,0x11,0x30,0x10,0xC0,
      0x13,0x20,0x1C,0x18,0x08,0x00,0x09,0x00,
      0x08,0xC0,0x08,0x02,0x08,0x01,0xFF,0xFE,
      0x08,0x00,0x18,0x00,0x08,0x00,0x00,0x00,

/*小*/ 0x00,0x00,0x00,0x10,0x00,0x20,0x00,0x40,
      0x01,0x80,0x06,0x02,0x00,0x01,0xFF,0xFE,
      0x00,0x00,0x04,0x00,0x02,0x00,0x01,0x00,
      0x00,0x80,0x00,0x60,0x00,0x00,0x00,0x00,

/*和*/ 0x04,0x08,0x24,0x10,0x24,0x60,0x25,0x80,
      0x3F,0xFF,0x44,0x80,0xCC,0x60,0x44,0x00,
      0x0F,0xFC,0x08,0x08,0x08,0x08,0x08,0x08,
      0x08,0x08,0x1F,0xFC,0x08,0x00,0x00,0x00,

/*尚*/ 0x00,0x00,0x07,0xFF,0x44,0x00,0x24,0x00,
      0x34,0xFC,0x04,0x88,0x04,0x88,0xFC,0x88,
      0x04,0x88,0x04,0x88,0x15,0xFC,0x24,0x82,
      0x64,0x01,0x0F,0xFE,0x04,0x00,0x00,0x00,

/*说*/ 0x02,0x00,0x02,0x00,0x42,0x00,0x33,0xFE,
      0x00,0x04,0x00,0x09,0x4F,0xC2,0x28,0x8C,
      0x18,0xF0,0x08,0x80,0x18,0xFC,0x28,0x82,
      0x4F,0xC2,0x00,0x02,0x00,0x0E,0x00,0x00,

/*：*/ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x01,0x8C,0x03,0xDE,0x03,0xDE,0x01,0x8C,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*空白*/0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};	                    


code unsigned  int zi3[] = {    /***********只需更改数组和z的值*****/
       0x00,0x00,	
/*家*/ 0x08,0x00,0x30,0x92,0x24,0x92,0x25,0x24,
      0x25,0x24,0x26,0x4A,0xA5,0x91,0x64,0xFE,
      0x24,0x20,0x24,0x50,0x24,0x88,0x25,0x88,
      0x24,0x04,0x28,0x06,0x30,0x04,0x00,0x00,

/*宇*/ 0x08,0x00,0x30,0x80,0x20,0x80,0x24,0x80,
      0x24,0x80,0x24,0x82,0xA4,0x81,0x67,0xFE,
      0x24,0x80,0x24,0x80,0x2C,0x80,0x24,0x80,
      0x21,0x80,0x28,0x80,0x30,0x00,0x00,0x00,

/*，*/ 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x62,
      0x00,0xF4,0x00,0xF8,0x00,0x60,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*手*/ 0x00,0x40,0x20,0x40,0x24,0x40,0x24,0x40,
      0x24,0x40,0x24,0x42,0x24,0x41,0x3F,0xFE,
      0x44,0x40,0x44,0x40,0x44,0x40,0xCC,0x40,
      0x44,0x40,0x00,0xC0,0x00,0x40,0x00,0x00,

/*机*/ 0x08,0x20,0x08,0xC0,0x0B,0x00,0xFF,0xFF,
      0x09,0x01,0x08,0x82,0x00,0x04,0x3F,0xF8,
      0x20,0x00,0x20,0x00,0x20,0x00,0x7F,0xFC,
      0x20,0x02,0x00,0x02,0x00,0x0E,0x00,0x00,

/*收*/ 0x00,0x00,0x1F,0xF0,0x00,0x20,0x00,0x40,
      0xFF,0xFF,0x00,0x01,0x01,0x01,0x06,0x02,
      0xFB,0x84,0x08,0x68,0x08,0x10,0x08,0x68,
      0x0F,0x86,0x18,0x03,0x08,0x02,0x00,0x00,

/*起*/ 0x02,0x02,0x12,0x04,0x12,0xF8,0x12,0x04,
      0xFF,0xFE,0x12,0x42,0x12,0x42,0x00,0x02,
      0x23,0xE2,0x22,0x12,0x22,0x12,0x22,0x12,
      0x7F,0x12,0x20,0x76,0x00,0x04,0x00,0x00,

/*好*/ 0x08,0x02,0x08,0x44,0x0F,0xA8,0xF8,0x10,
      0x08,0x28,0x0F,0xC6,0x01,0x00,0x41,0x00,
      0x41,0x02,0x41,0x01,0x47,0xFE,0x49,0x00,
      0x51,0x00,0x63,0x00,0x01,0x00,0x00,0x00,

/*好*/ 0x08,0x02,0x08,0x44,0x0F,0xA8,0xF8,0x10,
      0x08,0x28,0x0F,0xC6,0x01,0x00,0x41,0x00,
      0x41,0x02,0x41,0x01,0x47,0xFE,0x49,0x00,
      0x51,0x00,0x63,0x00,0x01,0x00,0x00,0x00,

/*读*/ 0x02,0x00,0x02,0x00,0x42,0x00,0x33,0xFE,
      0x00,0x04,0x04,0x28,0x24,0x21,0x24,0xA1,
      0x26,0x62,0x25,0x24,0xFC,0x38,0x27,0xE4,
      0x64,0x22,0x25,0x63,0x06,0x20,0x00,0x00,

/*书*/ 0x00,0x80,0x10,0x80,0x10,0x80,0x10,0x80,
      0x10,0x80,0x10,0x80,0xFF,0xFF,0x10,0x80,
      0x10,0x80,0x10,0x80,0x1F,0x84,0x40,0x82,
      0x20,0x84,0x30,0xF8,0x00,0x00,0x00,0x00,

/*啦*/ 0x00,0x00,0x3F,0xF8,0x20,0x10,0x3F,0xF8,
      0x10,0x42,0x10,0x41,0xFF,0xFE,0x11,0x02,
      0x12,0x02,0x17,0xC2,0x90,0x3A,0x70,0x02,
      0x10,0x7A,0x37,0x82,0x10,0x02,0x00,0x00,

/*。*/ 0x00,0x00,0x00,0x1C,0x00,0x3E,0x00,0x22,
      0x00,0x22,0x00,0x3E,0x00,0x1C,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*。*/ 0x00,0x00,0x00,0x1C,0x00,0x3E,0x00,0x22,
      0x00,0x22,0x00,0x3E,0x00,0x1C,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*。*/ 0x00,0x00,0x00,0x1C,0x00,0x3E,0x00,0x22,
      0x00,0x22,0x00,0x3E,0x00,0x1C,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*空白*/0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00


};


code unsigned  int zi4[] = {    /***********只需更改数组和z的值*****/
       0x00,0x00,	
/*今*/ 0x01,0x00,0x01,0x00,0x02,0x00,0x04,0x40,
      0x08,0x40,0x10,0x40,0x24,0x40,0xC2,0x41,
      0x23,0x42,0x10,0x44,0x08,0x58,0x04,0x60,
      0x02,0x00,0x03,0x00,0x02,0x00,0x00,0x00,

/*天*/ 0x02,0x01,0x42,0x01,0x42,0x02,0x42,0x04,
      0x42,0x08,0x42,0x30,0x42,0xC0,0x7F,0x00,
      0x42,0xC0,0x42,0x30,0x42,0x08,0x42,0x04,
      0xC2,0x02,0x46,0x03,0x02,0x02,0x00,0x00,

/*是*/ 0x00,0x80,0x00,0x81,0x00,0x82,0x00,0x84,
      0xFE,0xB8,0x92,0x84,0x92,0x82,0x92,0xFE,
      0x92,0x91,0x92,0x91,0xFE,0xB1,0x00,0x91,
      0x00,0x81,0x01,0x83,0x00,0x82,0x00,0x00,

/*二*/ 0x00,0x08,0x00,0x08,0x10,0x08,0x10,0x08,
      0x10,0x08,0x10,0x08,0x10,0x08,0x10,0x08,
      0x10,0x08,0x10,0x08,0x10,0x08,0x30,0x08,
      0x10,0x08,0x00,0x18,0x00,0x08,0x00,0x00,

/*零*/ 0x08,0x20,0x30,0x20,0x20,0x40,0xAA,0x40,
      0xAA,0x90,0xAA,0x90,0xA1,0x54,0xFE,0x32,
      0xA1,0x15,0xAA,0x98,0xAA,0x90,0xAA,0x40,
      0x20,0x40,0x28,0x20,0x30,0x20,0x00,0x00,

/*一*/ 0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,
      0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,
      0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,
      0x01,0x00,0x03,0x00,0x01,0x00,0x00,0x00,

/*六*/0x04,0x00,0x04,0x02,0x04,0x04,0x04,0x18,
      0x04,0x60,0x44,0xC0,0x34,0x00,0x1C,0x00,
      0x04,0x00,0x04,0x80,0x04,0x40,0x04,0x30,
      0x04,0x1C,0x0C,0x0E,0x04,0x00,0x00,0x00,

/*年*/ 0x00,0x20,0x04,0x20,0x08,0x20,0x33,0xE0,
      0xE2,0x20,0x22,0x20,0x22,0x20,0x3F,0xFF,
      0x22,0x20,0x22,0x20,0x22,0x20,0x26,0x20,
      0x62,0x20,0x20,0x60,0x00,0x20,0x00,0x00,

/*六*/ 0x04,0x00,0x04,0x02,0x04,0x04,0x04,0x18,
      0x04,0x60,0x44,0xC0,0x34,0x00,0x1C,0x00,
      0x04,0x00,0x04,0x80,0x04,0x40,0x04,0x30,
      0x04,0x1C,0x0C,0x0E,0x04,0x00,0x00,0x00,

/*月*/ 0x00,0x00,0x00,0x01,0x00,0x02,0x00,0x0C,
      0x7F,0xF0,0x44,0x40,0x44,0x40,0x44,0x40,
      0x44,0x40,0x44,0x42,0x44,0x41,0xFF,0xFE,
      0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*二*/ 0x00,0x08,0x00,0x08,0x10,0x08,0x10,0x08,
      0x10,0x08,0x10,0x08,0x10,0x08,0x10,0x08,
      0x10,0x08,0x10,0x08,0x10,0x08,0x30,0x08,
      0x10,0x08,0x00,0x18,0x00,0x08,0x00,0x00,

/*十*/ 0x02,0x00,0x02,0x00,0x02,0x00,0x02,0x00,
      0x02,0x00,0x02,0x00,0x02,0x00,0xFF,0xFF,
      0x02,0x00,0x02,0x00,0x02,0x00,0x02,0x00,
      0x02,0x00,0x06,0x00,0x02,0x00,0x00,0x00,

/*五*/ 0x00,0x02,0x20,0x02,0x21,0x02,0x21,0x02,
      0x21,0x1E,0x21,0xE2,0x3F,0x02,0x21,0x02,
      0x21,0x02,0x21,0x02,0x21,0x02,0x23,0xFE,
      0x61,0x02,0x21,0x06,0x00,0x02,0x00,0x00,

/*日*/ 0x00,0x00,0x00,0x00,0x00,0x00,0x7F,0xFE,
      0x41,0x04,0x41,0x04,0x41,0x04,0x41,0x04,
      0x41,0x04,0x41,0x04,0x41,0x04,0xFF,0xFE,
      0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*。*/ 0x00,0x00,0x00,0x1C,0x00,0x3E,0x00,0x22,
      0x00,0x22,0x00,0x3E,0x00,0x1C,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,

/*空白*/0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00




 };      
code unsigned  int zi7[] = {    /***********只需更改数组和z的值*****/
       0x00,0x00,								  /*上到下，左到右，上高*/


0x00,0x42,0x00,0x8C,0xFF,0x20,0xAA,0xCE,
      0xAA,0x21,0xAB,0xE1,0xAA,0x91,0xEA,0x49,
      0x10,0x0D,0x14,0x01,0x13,0x41,0x10,0x27,
      0xFF,0xC0,0x10,0x08,0x10,0x06,0x00,0x00,

0x22,0x10,0x22,0x18,0x3F,0xF2,0x22,0x24,
      0x10,0x18,0x9F,0xE4,0x72,0x02,0x13,0xFD,
      0x18,0x06,0xF2,0xF8,0x12,0x04,0x13,0xFE,
      0x12,0x21,0x32,0xA1,0x13,0x21,0x00,0x00,

/*班*/0x21,0x04,0x21,0x06,0x3F,0xFC,0x21,0x08,
      0x60,0x49,0x2F,0x82,0x00,0x1C,0xFF,0xE0,
      0x00,0x02,0x21,0x02,0x21,0x02,0x3F,0xFE,
      0x21,0x02,0x61,0x06,0x20,0x02,0x00,0x00,

/*长*/0x01,0x00,0x01,0x00,0x01,0x00,0x01,0x00,
      0xFF,0xFF,0x01,0x02,0x01,0x02,0x05,0xC4,
      0x09,0x20,0x11,0x10,0x21,0x08,0x61,0x04,
      0x01,0x06,0x03,0x04,0x01,0x00,0x00,0x00,

/*是*/0x00,0x80,0x00,0x81,0x00,0x82,0x00,0x84,
      0xFE,0xB8,0x92,0x84,0x92,0x82,0x92,0xFE,
      0x92,0x91,0x92,0x91,0xFE,0xB1,0x00,0x91,
      0x00,0x81,0x01,0x83,0x00,0x82,0x00,0x00,

/*个*/0x01,0x00,0x01,0x00,0x02,0x00,0x04,0x00,
      0x08,0x00,0x10,0x00,0x20,0x00,0xC7,0xFF,
      0x20,0x00,0x10,0x00,0x08,0x00,0x04,0x00,
      0x02,0x00,0x03,0x00,0x02,0x00,0x00,0x00,

/*逗*/0x02,0x00,0x42,0x02,0x32,0x04,0x03,0xF8,
      0x00,0x04,0x40,0x12,0x4F,0x12,0x49,0x52,
      0x49,0x32,0x49,0x12,0x49,0x32,0x49,0x52,
      0xDF,0x12,0x48,0x16,0x00,0x04,0x00,0x00,

/*比*/0x00,0x00,0x00,0x02,0x7F,0xFE,0x02,0x04,
      0x02,0x04,0x06,0x08,0x02,0x08,0x00,0x00,
      0xFF,0xFC,0x01,0x02,0x02,0x02,0x04,0x02,
      0x0C,0x02,0x00,0x02,0x00,0x1E,0x00,0x00,

/*笑脸*/0x07,0xC0,0x18,0x30,0x20,0x08,0x40,0x44,
      0x4C,0x24,0x8C,0x12,0x80,0x12,0x80,0x12,
      0x80,0x12,0x8C,0x12,0x4C,0x24,0x40,0x44,
      0x20,0x08,0x18,0x30,0x07,0xC0,0x00,0x00,

/*空白*/0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00         	   /*保留最后一两个空格对付占用*/
};
/*****n（us）延时子程序*****/
DelayUs(unsigned int N)
    {
        unsigned int x ;
        for(x=0;x<=N;x++);
    }
/*主程序*/
main()
    {
         unsigned int i=0,j=0,z=0,k=2,l=0,m=0,n=0,p=8;    		   /*共有z个元素*/
        while(1)
        {         
/*********************************************/
    if(p10==0)
    {
	   p=0;
    }
    if(p11==0)
    {
	   p=1;
    }
    if(p12==0)
    {
	   p=2;
    }
    if(p13==0)
    {
	   p=3;
    }
    if(p14==0)
    {
	   p=4;
    }
    if(p17==0)
    {
	   p=7;
    }
    switch (p)
    {
	   case 0: z=226; P0=~zi0[k-2-i*2];P2=~zi0[k-1-i*2]; break;  
        case 1: z=898; P0=~zi1[k-2-i*2];P2=~zi1[k-1-i*2]; break; 
 	   case 2: z=1090; P0=~zi2[k-2-i*2];P2=~zi2[k-1-i*2]; break; 
	   case 3: z=514; P0=~zi3[k-2-i*2];P2=~zi3[k-1-i*2]; break; 
	   case 4: z=514; P0=~zi4[k-2-i*2];P2=~zi4[k-1-i*2]; break; 
	   case 7: z=322; P0=~zi7[k-2-i*2];P2=~zi7[k-1-i*2]; break; 
	   default :break;

    }





/**********************************************/                
                  DelayUs(73);
                  P0=0XFF;
                  P2=0XFF;
			   DelayUs(10);
			   if(k-2-i*2>0)
			   {
				   i++;
                  }
		   if(KEY==1)
                {	  
			     n++;					//加n上限
				if(n>20000)
				{
				    n=200;
                    }
			     if(n>=100)
				{				  
				  if(k-2-i*2>=0)
				   {	 
					 m++;
					 if(m==1)				  /*m==2减速*/	
					 {
                          k=l;						
					 k=k+2;
					 l=k;
					 m=0;
					 }
				  while(KEY) ;
	    	     	  }
				i=0;
				}
                }
		   if(l>z+124)
		   {
			    l=2;
			    k=2;
             }
		   if(l>z)
		   {
			     DelayUs(100*k-100*z) ;				 
				k=z;
             }
        }
    }
