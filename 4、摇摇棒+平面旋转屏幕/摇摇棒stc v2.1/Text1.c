#include<reg51.h>
sbit KEY=P3^2 ;  
sbit huan=P3^7;         //定义画面切换按键
unsigned int a,b,c,d,z1,z2,z3,z4,z,pic=0,num=0;     //pic为按键次数；num为中断次数

code unsigned  int zi1[] = {
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFE,0x3F,0x00,0x20,0x00,0x20,0x00,0x20,
0x00,0x20,0x00,0x20,0x00,0x20,0x00,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0xF8,0x0F,
0x04,0x10,0x02,0x20,0x02,0x20,0x02,0x20,0x02,0x20,0x04,0x10,0xF8,0x0F,0x00,0x00,
0x00,0x00,0x00,0x00,0xFE,0x07,0x00,0x08,0x00,0x10,0x00,0x20,0x00,0x20,0x00,0x10,
0x00,0x08,0xFE,0x07,0x00,0x00,0x00,0x00,0x00,0x00,0xFE,0x3F,0x82,0x20,0x82,0x20,
0x82,0x20,0x82,0x20,0x82,0x20,0x82,0x20,0x82,0x20,0x00,0x00,0x00,0x00,0x00,0x00/*LOVE*/
};
code unsigned  int zi2[] = {
0x78,0x00,0xFC,0x00,0xFE,0x01,0xFE,0x03,0xFE,0x07,0xFE,0x0F,0xFE,0x1F,0xFC,0x3F,
0xF8,0x7F,0xFC,0x3F,0xFE,0x1F,0xFE,0x0F,0xFE,0x07,0xFE,0x03,0xFE,0x01,0xFC,0x00,
0x78,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00/*心形图案*/
};
code unsigned  int zi3[] = {
0x00,0x00,0x00,0x00,0x00,0x00,0xC0,0x01,0x40,0x01,0xC0,0x01,0x00,0x00,0x00,0x00,
0xF0,0x0F,0x08,0x10,0x04,0x20,0x00,0x00,0x00,0x00,0xF0,0x3F,0x08,0x00,0x04,0x00,
0x04,0x00,0x04,0x00,0x08,0x00,0xF0,0x3F,0x00,0x00,0x00,0x00,0x00,0x20,0x00,0x20,
0x00,0x20,0x00,0x20,0x00,0x20,0x00,0x00,0x00,0x00,0xF0,0x3F,0x08,0x00,0x04,0x00,
0x04,0x00,0x04,0x00,0x08,0x00,0xF0,0x3F,0x00,0x00,0x00,0x00,0x04,0x20,0x08,0x10,
0xF0,0x0F,0x00,0x00,0x00,0x00,0xC0,0x01,0x40,0x01,0xC0,0x01,0x00,0x00,0x00,0x00/*呵呵o(∩_∩)o图案*/
};
code unsigned  int zi4[] = {
//-- 欢 --
      0x04,0x10,0x34,0x08,0xC4,0x06,0x04,0x01,
      0xC4,0x82,0x3C,0x8C,0x20,0x40,0x10,0x30,
      0x0F,0x0C,0xE8,0x03,0x08,0x0C,0x08,0x10,
      0x28,0x60,0x18,0xC0,0x00,0x40,0x00,0x00,
//-- 迎 --
      0x40,0x00,0x42,0x40,0x44,0x20,0xC8,0x1F,
      0x00,0x20,0xFC,0x47,0x04,0x42,0x02,0x41,
      0x82,0x40,0xFC,0x7F,0x04,0x40,0x04,0x42,
      0x04,0x44,0xFE,0x63,0x04,0x20,0x00,0x00,
//-- 使 --
      0x40,0x00,0x20,0x00,0xF8,0xFF,0x07,0x00,
      0x04,0x80,0xF4,0x43,0x14,0x45,0x14,0x29,
      0x14,0x19,0xFF,0x17,0x14,0x21,0x14,0x21,
      0x14,0x41,0xF6,0xC3,0x04,0x40,0x00,0x00,
//-- 用 --
      0x00,0x80,0x00,0x60,0xFE,0x1F,0x22,0x02,
      0x22,0x02,0x22,0x02,0x22,0x02,0xFE,0x7F,
      0x22,0x02,0x22,0x02,0x22,0x42,0x22,0x82,
      0xFF,0x7F,0x02,0x00,0x00,0x00,0x00,0x00,

//-- 神 --
      0x08,0x01,0x88,0x00,0x49,0x00,0xEE,0xFF,
      0x58,0x00,0x88,0x00,0x00,0x00,0xF8,0x1F,
      0x88,0x08,0x88,0x08,0xFF,0xFF,0x88,0x08,
      0x88,0x08,0xFC,0x1F,0x08,0x00,0x00,0x00,
//-- 奇 --
      0x40,0x00,0x40,0x00,0x44,0x00,0x44,0x3E,
      0x64,0x12,0x54,0x12,0x4C,0x12,0x47,0x12,
      0x4C,0x3F,0x54,0x42,0x74,0x80,0xC6,0x7F,
      0x44,0x00,0x60,0x00,0x40,0x00,0x00,0x00,
//-- 魔 --
      0x00,0x40,0x00,0x30,0xFE,0x8F,0x4A,0x80,
      0xAA,0x5F,0x9A,0x4A,0xFE,0x2A,0xAA,0x1A,
      0xCB,0x0F,0xAA,0x7A,0xFE,0x8A,0x9A,0xAA,
      0xAA,0x8F,0x6B,0x80,0x22,0xE0,0x00,0x00,
//-- 幻 --
      0x80,0x20,0xC0,0x30,0xA0,0x28,0x98,0x24,
      0x87,0x22,0x80,0x21,0xC4,0x30,0x04,0x60,
      0x04,0x00,0x04,0x20,0x04,0x40,0x04,0x80,
      0x04,0x40,0xFE,0x3F,0x04,0x00,0x00,0x00,
//-- 摇 --
      0x10,0x02,0x10,0x42,0x10,0x81,0xFF,0x7F,
      0x90,0x04,0x54,0x05,0xCC,0xF4,0xB4,0x44,
      0x84,0x44,0xBC,0x7F,0x82,0x44,0xA2,0x44,
      0x9B,0xF4,0x82,0x06,0x00,0x04,0x00,0x00,
//-- 摇 --
      0x10,0x02,0x10,0x42,0x10,0x81,0xFF,0x7F,
      0x90,0x04,0x54,0x05,0xCC,0xF4,0xB4,0x44,
      0x84,0x44,0xBC,0x7F,0x82,0x44,0xA2,0x44,
      0x9B,0xF4,0x82,0x06,0x00,0x04,0x00,0x00,
//-- 棒 --
      0x10,0x04,0x10,0x03,0xD0,0x00,0xFF,0xFF,
      0x90,0x00,0x54,0x05,0x44,0x12,0xD4,0x15,
      0x74,0x14,0x5F,0xFF,0xD4,0x14,0x54,0x15,
      0x56,0x12,0x44,0x06,0x40,0x02,0x00,0x00,
//-- ！ --
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x7C,0x10,0xFE,0x3B,
      0xFE,0x3B,0x7C,0x10,0x00,0x00,0x00,0x00,
      0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

/*****n（us）延时子程序*****/
void DelayUs(unsigned int N)
{
 unsigned int x;
 for(x=0; x<=N;x++);
}

/*****主函数*****/
main()
{
    a=4;				//a每次显示字数    b切换第几屏     c显示同屏几次
    c=8;		    //d共几个画面    z1z2z3z4各自话中几个字（32b）
    d=4;
    z1=3;z2=2;z3=3;z4=12;
    huan=KEY=1;
    while(1)     //主程序中只检测按键
   {
	   unsigned int i;
	   if(huan==0)
	   {
		   DelayUs(1000);
		   if(huan==0)
		   {
			  pic++;b=num=0;
             }
		   while(huan==0);
	   }
	   if(pic>d-1)
	   {
		   pic=0;
        }
	   if(KEY==0)
	   {
		   
		   num++;
		   DelayUs(5000);
		   for(i=0;i<16*a;i++)
		   {
			   switch(pic)
			   {
				   case 0:P0=~zi1[32*a*b+i*2];P2=~zi1[32*a*b+i*2+1];z=z1;break;
				   case 1:P0=~zi2[32*a*b+i*2];P2=~zi2[32*a*b+i*2+1];z=z2;break;
				   case 2:P0=~zi3[32*a*b+i*2];P2=~zi3[32*a*b+i*2+1];z=z3;break;
				   case 3:P0=~zi4[32*a*b+i*2];P2=~zi4[32*a*b+i*2+1];z=z4;break;
                  }
			   DelayUs(100);
			   if(32*a*b+i*2+2==32*z)
			   {
				   i=16*a-1;
				   if(num>c-1)
				   {b=num=0;}
                  }
             }
        }
	   if(32*a*b+i*2+2<32*z)
	   {
		   if(num>c-1)
		   {num=0;b++;}
        }
   }
}






