#ifndef eeprom_isp_H
#define eeprom_isp_H
				//5k    0x2000~0x33ff


//#define start_addr 0x2000	   //STC89C51,52,54
#define start_addr 0x8000	   //STC89C58,516



/*
void clean512(uint addr);  		 清空一扇区，地址0~512*10  已经里边加上0x000
void eeprom_write_char(uint addr,uchar dat);			地址0~512*10
uchar eeprom_read_char(uint addr);					地址0~512*10								  
void eeprom_write_str(uint i,uchar *dat);			地址0~512*10
*/
void close() 
{ 
	EA = 1;///////////////////
	ISP_CONTR = 0;            // 关闭IAP功能 
	ISP_CMD   = 0;            // 待机模式，无ISP操作 
	ISP_TRIG  = 0;            // 关闭IAP功能, 清与ISP有关的特殊功能寄存器  
}
uchar eeprom_read_char(uint addr) 
{    
    uchar dat; 
	addr+=start_addr;	 
	ISP_CONTR = 0x81;		   
    ISP_CMD   = 0x01;         // 用户可以对"Data Flash/EEPROM区"进行字节读 
    ISP_ADDRL = addr;          
    ISP_ADDRH = addr>>8;       
    EA = 0; 
    ISP_TRIG = 0x46;           
    ISP_TRIG = 0xB9;  
	//delay_us(1);          

    dat = ISP_DATA;			  // 取出数据 
	close();					  // 关闭ISP/IAP     可以不加！！	           
	return dat; 
} 





#endif 