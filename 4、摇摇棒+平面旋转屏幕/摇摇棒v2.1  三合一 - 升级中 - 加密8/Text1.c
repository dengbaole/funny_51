#include "STC89C5xRC_RD+.h"
#include "data1.h"
#include "ds1302.h"
#include "eeprom_isp.h"

#define ulong unsigned long
#define uint unsigned int
#define uchar unsigned char

sbit zhendong_key=P3^2 ;   	 //震动开关
sbit button=P3^7;             //按键
sbit huoer_key=P1^0;	     //	霍尔传感器
sbit yao_zhuan=P1^5;	   	// 开关  摇摇棒
sbit bike=P3^1; 		    //开关  选择车轮

uchar sec_temp=0;
uchar sec=0,min=27,hour=6,day=32,month=6,year=18;

// !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~
code uchar ASCII[]={    //阳码  逐列  逆向！
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,/*" ",0*/
0xFF,0xFF,0xFF,0x20,0x20,0xFF,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xF8,0xF8,0xFF,0xF8,0xF8,0xFF,0xFF,/*"未命名文件",0*/
0xEB,0xFB,0xA0,0xEB,0xA0,0xEB,0xEB,0xFF,/*"未命名文件",0*/
0xD1,0xD5,0x80,0xD5,0x80,0xD5,0xC5,0xFF,/*"未命名文件",0*/
0x38,0x9A,0xC8,0xE7,0x13,0x59,0x1C,0xFF,/*"未命名文件",0*/
0x99,0x66,0x76,0x66,0x96,0x99,0x6F,0xFF,/*"未命名文件",0*/
0xFF,0xFF,0xF8,0xF8,0xFF,0xFF,0xFF,0xFF,/*" ‘  ",0*/
0xFF,0xFF,0xC3,0xBD,0x7E,0xFF,0xFF,0xFF,/*"（",0*/
0xFF,0xFF,0xFF,0x7E,0xBD,0xC3,0xFF,0xFF,/*"）",0*/
0xFF,0xD5,0xF7,0xC1,0xF7,0xD5,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xF7,0xF7,0xC1,0xF7,0xF7,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xFF,0x7F,0x8F,0xCF,0xFF,0xFF,0xFF,/*"，",0*/
0xFF,0xF7,0xF7,0xF7,0xF7,0xF7,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xFF,0xFF,0xFF,0xFF,0x9F,0x9F,0xFF,/*"未命名文件",0*/
0xFF,0x3F,0x9F,0xCF,0xE7,0xF3,0xF9,0xFF,/*"未命名文件",0*/
0xFF,0xC3,0xBD,0x7E,0x7E,0xBD,0xC3,0xFF,/*"0",0*/
0xFF,0xFF,0x7D,0x7D,0x00,0x7F,0x7F,0xFF,/*"1",0*/
0xFF,0x7D,0x3E,0x5E,0x6E,0x76,0x79,0xFF,/*"2",0*/
0xFF,0x6D,0x6D,0x6D,0x6D,0x6D,0x01,0xFF,/*"3",0*/
0xEF,0xE7,0xEB,0xED,0x00,0xEF,0xEF,0xFF,/*"4",0*/
0xFF,0x61,0x6D,0x6D,0x6D,0x6D,0x0D,0xFF,/*"5",0*/
0x83,0x6D,0x76,0x76,0x76,0x76,0x8F,0xFF,/*"6",0*/
0xFF,0xFE,0x1E,0xEE,0xF6,0xFA,0xFC,0xFF,/*"7",0*/
0xFF,0x81,0x76,0x76,0x76,0x76,0x81,0xFF,/*"8",0*/
0xFF,0x61,0x6E,0x6E,0x6E,0x6E,0x81,0xFF,/*"9",0*/
0xFF,0xFF,0x27,0x27,0xFF,0xFF,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0x7F,0x13,0x93,0xFF,0xFF,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xFF,0xE7,0xDB,0xBD,0x7E,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xDB,0xDB,0xDB,0xDB,0xDB,0xDB,0xFF,/*"未命名文件",0*/
0xFF,0xFF,0x7E,0xBD,0xDB,0xE7,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xFD,0xFE,0x4E,0xF6,0xF9,0xFF,0xFF,/*"？",0*/
0xC3,0xBD,0x66,0x5A,0x5A,0x46,0x5D,0xC3,/*"@",0*/
0x3F,0xCF,0xD3,0xDC,0xD3,0xCF,0x3F,0xFF,/*"A",0*/
0xFF,0x80,0xB6,0xB6,0xB6,0xB6,0xC9,0xFF,/*"未命名文件",0*/
0xE3,0xDD,0xBE,0xBE,0xBE,0xBE,0xDD,0xFF,/*"未命名文件",0*/
0xFF,0x80,0xBE,0xBE,0xBE,0xDD,0xE3,0xFF,/*"未命名文件",0*/
0xFF,0x80,0xB6,0xB6,0xB6,0xB6,0xBE,0xFF,/*"未命名文件",0*/
0xFF,0x80,0xF6,0xF6,0xF6,0xF6,0xFE,0xFF,/*"未命名文件",0*/
0xE3,0xDD,0xBE,0xBE,0xBE,0xBE,0x8D,0xEF,/*"未命名文件",0*/
0xFF,0x80,0xF7,0xF7,0xF7,0xF7,0x80,0xFF,/*"未命名文件",0*/
0xFF,0xFF,0xBE,0x80,0xBE,0xFF,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xDF,0xBF,0xBE,0x80,0xFE,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0x80,0xF7,0xEB,0xDD,0xBE,0xBE,0xFF,/*"未命名文件",0*/
0xFF,0x80,0xBF,0xBF,0xBF,0xBF,0xFF,0xFF,/*"未命名文件",0*/
0x80,0xFD,0xE3,0x9F,0xE3,0xFD,0x80,0xFF,/*"未命名文件",0*/
0x80,0xFD,0xFB,0xF7,0xEF,0xDF,0x80,0xFF,/*"未命名文件",0*/
0xE3,0xDD,0xBE,0xBE,0xBE,0xDD,0xE3,0xFF,/*"O",0*/
0xFF,0x00,0xEE,0xEE,0xEE,0xEE,0xF1,0xFF,/*"未命名文件",0*/
0xE3,0xDD,0xBE,0xBE,0xAE,0xDD,0xA3,0xBF,/*"未命名文件",0*/
0xFF,0x80,0xF6,0xF6,0xE6,0xD6,0xB9,0xFF,/*"未命名文件",0*/
0xFF,0xB9,0xB6,0xB6,0xB6,0xB6,0xCE,0xFF,/*"未命名文件",0*/
0xFE,0xFE,0xFE,0x80,0xFE,0xFE,0xFE,0xFF,/*"T",0*/
0xFF,0xC0,0xBF,0xBF,0xBF,0xBF,0xC0,0xFF,/*"未命名文件",0*/
0xFF,0xF0,0xCF,0xBF,0xBF,0xCF,0xF0,0xFF,/*"未命名文件",0*/
0xE0,0x9F,0xE7,0xF9,0xE7,0x9F,0xE0,0xFF,/*"未命名文件",0*/
0xBE,0xDD,0xEB,0xF7,0xEB,0xDD,0xBE,0xFF,/*"未命名文件",0*/
0xF8,0xF7,0xEF,0x1F,0xEF,0xF7,0xF8,0xFF,/*"未命名文件",0*/
0xFF,0xBE,0x9E,0xAE,0xB6,0xBA,0xBC,0xFF,/*"Z",0*/
0xFF,0xFF,0x00,0x7E,0x7E,0x7E,0xFF,0xFF,/*"未命名文件",0*/
0xFE,0xFD,0xFB,0xF7,0xEF,0xDF,0xBF,0x7F,/*"未命名文件",0*/
0xFF,0xFF,0x7E,0x7E,0x7E,0x00,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xFB,0xFD,0xFE,0xFD,0xFB,0xFF,0xFF,/*"未命名文件",0*/
0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,0x7F,/*"未命名文件",0*/
0xFF,0xFF,0xFC,0xFC,0xFF,0xFF,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xC7,0xB6,0xB6,0xB6,0xB6,0x81,0xBF,/*"a",0*/
0xFF,0x80,0xB7,0xB7,0xB7,0xB7,0xCF,0xFF,/*"未命名文件",0*/
0xE7,0xDB,0xBD,0xBD,0xBD,0xFF,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xCF,0xB7,0xB7,0xB7,0x80,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xC3,0xB5,0xB5,0xB5,0xB5,0xF3,0xFF,/*"e",0*/
0xFF,0xF7,0x77,0x01,0xF6,0xF5,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xB1,0x76,0x76,0x76,0x81,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0x00,0xEF,0xF7,0xF7,0x0F,0xFF,0xFF,/*"h",0*/
0xFF,0xF7,0xFB,0x02,0x7F,0xBF,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0x3F,0x7F,0x7F,0x02,0xFF,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0x01,0xDF,0xAF,0x77,0xFF,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xFF,0xFF,0x00,0x7F,0xFF,0xFF,0xFF,/*"未命名文件",0*/
0x87,0xFB,0xFB,0x87,0xFB,0xFB,0x87,0xFF,/*"未命名文件",0*/
0xFF,0x87,0xFB,0xFB,0xFB,0x87,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xC7,0xBB,0xBB,0xBB,0xC7,0xFF,0xFF,/*"o",0*/
0xFF,0x01,0xED,0xED,0xF3,0xFF,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xF3,0xED,0xED,0x01,0x7F,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xFD,0x01,0xF7,0xFB,0xFD,0xFD,0xFF,/*"未命名文件",0*/
0xFF,0x73,0x6D,0x6D,0x6D,0x9D,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xF7,0xF7,0x01,0x77,0x77,0xB7,0xFF,/*"未命名文件",0*/
0xFF,0xC1,0xBF,0xBF,0xBF,0x81,0xBF,0xFF,/*"未命名文件",0*/
0xF7,0xE7,0xDF,0x3F,0xDF,0xE7,0xF7,0xFF,/*"未命名文件",0*/
0xC7,0xBF,0xDF,0xE7,0xDF,0xBF,0xC7,0xFF,/*"未命名文件",0*/
0xFF,0xBB,0xD7,0xEF,0xD7,0xBB,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xBE,0xBD,0xDB,0xE7,0xFB,0xFD,0xFE,/*"未命名文件",0*/
0xFF,0xBB,0x9B,0xAB,0xB3,0xBB,0xFF,0xFF,/*"z",0*/
0xFF,0xEF,0x11,0x7D,0x7D,0xFF,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xFF,0xFF,0x00,0xFF,0xFF,0xFF,0xFF,/*"未命名文件",0*/
0xFF,0xFF,0xFF,0x7D,0x7D,0x11,0xEF,0xFF,/*"未命名文件",0*/
0xF7,0xFB,0xFB,0xF7,0xEF,0xEF,0xF7,0xFF,/*"未命名文件",0*/
};

delay_us(uint x)
{
	while(x--);
}
delay_ms(uint x)
{
	uint i,j; 
	for(j=0;j<x;j++)
		for(i=0;i<122;i++);	
}
uchar check()
{
	uchar zi[]="zhiyu_linshengyi,qq:731801789,qq_qun:540418122.",i=0,num=0;
	for(i=0;i<46;i++)
	{
		if(eeprom_read_char(i)!=zi[i])	 num++;
	}
	return num;
} 			
void switch_key_show(uchar number0,uchar all_num)
{
	uchar i=0,j=0;
		if(check()==0)
				for(i=0;i<66;i++)
				{
					if(number0<8)	P0=~(1<<number0);	  //不同picture的切换提示
					else {P2=~(1<<(number0-8));}
					delay_ms(3);		
					if(all_num<=8)
					{
						for(j=0;j<all_num;j++)	P0 &=~(1<<j);	
					}
					else
					{
						P0=0x00;
						for(j=0;j<all_num-8;j++)	  P2 &=~(1<<j);
					}
					delay_us(10);
					P0=P2=0xff;
					delay_ms(6);
				}
		else
		{ 
			huoer_key=0;zhendong_key=0;
			delay_ms(30000);
		}
				while(button==0);
				delay_ms(10);
}



//1是显示数字时钟
//uchar yaoyaozi2_8_num[7]={4,4,4,16,4,3,4};
//uchar yaoyaozi9_16_num[8]={0,0,0,0,0,0,0,0};
//uchar yaoyao_setup[3]={8,8,4};	 	//共几个画面   小页面显示几次	  每小页面显示字数

#define yaoyaozi2_num  4
#define yaoyaozi3_num  4
#define yaoyaozi4_num  4
#define yaoyaozi5_num  16
#define yaoyaozi6_num  4
#define yaoyaozi7_num  3
#define yaoyaozi8_num  4

#define yaoyaozi9_num   0
#define yaoyaozi10_num  0
#define yaoyaozi11_num  0
#define yaoyaozi12_num  0
#define yaoyaozi13_num  0
#define yaoyaozi14_num  0
#define yaoyaozi15_num  0
#define yaoyaozi16_num  0

#define yaoyao_picture_num   8		//yaoyao_picture_num共几个画面 
#define yaoyao_show_times    8	   //show_times小页面显示几次
#define show_once_num        4	   	//show_once_num每小页面显示字数


void yaoyaobang1(void)	             //摇摇棒
{
		 				   //picture_temp为图片次数temp；show_temp为同一小页面显示 次数temp
    uint i,show_shuzu_temp=0; 	 //show_page切换第几个小页面  show_shuzu_temp为显示数组序号的temp
	uchar zi_num,show_page=0,picture_temp=0,show_temp=0;
	uchar show11[]="12:34:56";
	uchar *show111=show11;	 							  
	uchar show22[]=" 16-26  ";
	uchar *show222=show22;

    button=zhendong_key=1;

	delay_ms(500);		 ////////////待改
	P0=0xfe;
	delay_ms(200);
	P0=0xff;
	delay_ms(200);
	P0=0xfe;
	delay_ms(200);
	P0=0xff;
	delay_ms(500);

	while(1)    
	{
		show11[0]=48+hour/10;
		show11[1]=48+hour%10;
		show11[3]=48+min/10;
		show11[4]=48+min%10;
		show11[6]=48+sec/10;
		show11[7]=48+sec%10;

		show22[1]=48+month/10;
		show22[2]=48+month%10;
		show22[4]=48+day/10;
		show22[5]=48+day%10;
				
/**/	if(button==0)	  	 //程序中只检测按键	   可以不消抖动
		{
			delay_ms(10);
			if(button==0)		 //切换picture
			{
				picture_temp++;show_page=show_temp=0;
				if(picture_temp>=yaoyao_picture_num)  	picture_temp=0;
				switch_key_show(picture_temp,yaoyao_picture_num);
			}
/**/	}
		
		if(zhendong_key==0)		// 摇动时
		{
			delay_ms(10);
			if(zhendong_key==0)
			{
				show_temp++;
				delay_ms(53);   
				for(i=0;i<16*show_once_num;i++)	   //1 page
				{
					show_shuzu_temp=32*show_once_num*show_page+i*2;
					switch(picture_temp+1)
					{
						case 2:P0=yaoyaozi2[show_shuzu_temp];P2=yaoyaozi2[show_shuzu_temp+1];zi_num=yaoyaozi2_num;break;
						case 3:P0=yaoyaozi3[show_shuzu_temp];P2=yaoyaozi3[show_shuzu_temp+1];zi_num=yaoyaozi3_num;break;
						case 4:P0=yaoyaozi4[show_shuzu_temp];P2=yaoyaozi4[show_shuzu_temp+1];zi_num=yaoyaozi4_num;break;
						case 5:P0=yaoyaozi5[show_shuzu_temp];P2=yaoyaozi5[show_shuzu_temp+1];zi_num=yaoyaozi5_num;break;
						case 6:P0=yaoyaozi6[show_shuzu_temp];P2=yaoyaozi6[show_shuzu_temp+1];zi_num=yaoyaozi6_num;break;
						case 7:P0=yaoyaozi7[show_shuzu_temp];P2=yaoyaozi7[show_shuzu_temp+1];zi_num=yaoyaozi7_num;break;
						case 8:P0=yaoyaozi8[show_shuzu_temp];P2=yaoyaozi8[show_shuzu_temp+1];zi_num=yaoyaozi8_num;break;
					
						case 9:P0=yaoyaozi9[show_shuzu_temp];P2=yaoyaozi9[show_shuzu_temp+1];zi_num=yaoyaozi9_num;break;
						case 10:P0=yaoyaozi10[show_shuzu_temp];P2=yaoyaozi10[show_shuzu_temp+1];zi_num=yaoyaozi10_num;break;
						case 11:P0=yaoyaozi11[show_shuzu_temp];P2=yaoyaozi11[show_shuzu_temp+1];zi_num=yaoyaozi11_num;break;
						case 12:P0=yaoyaozi12[show_shuzu_temp];P2=yaoyaozi12[show_shuzu_temp+1];zi_num=yaoyaozi12_num;break;
						case 13:P0=yaoyaozi13[show_shuzu_temp];P2=yaoyaozi13[show_shuzu_temp+1];zi_num=yaoyaozi13_num;break;
						case 14:P0=yaoyaozi14[show_shuzu_temp];P2=yaoyaozi14[show_shuzu_temp+1];zi_num=yaoyaozi14_num;break;
						case 15:P0=yaoyaozi15[show_shuzu_temp];P2=yaoyaozi15[show_shuzu_temp+1];zi_num=yaoyaozi15_num;break;
						case 16:P0=yaoyaozi16[show_shuzu_temp];P2=yaoyaozi16[show_shuzu_temp+1];zi_num=yaoyaozi16_num;break;

						case 1:	  	zi_num=4;
						P0=ASCII[(*show111-32)*8+i%8];P2=ASCII[(*show222-32)*8+i%8];     
						if(i%8==7)
						{	show111 ++;	show222 ++;	}				break;

					}
					delay_us(100);		   //亮瞬间的时间
					if(32*show_once_num*show_page+i*2+2>=32*zi_num) //是否到最后，包括未满预定字数   
					{
						if(show_temp>=yaoyao_show_times)		//从头开始显示
						{show_page=show_temp=0;}
						i=16*show_once_num-1;

						show111=show11;
						show222=show22;

						break;
					}
				}
				if(32*show_once_num*show_page+i*2+2<32*zi_num) 					 
				{
					if(show_temp>=yaoyao_show_times)	 //未到最后，换显示下一小页面
					{show_temp=0;show_page++;}
				} 
			}
        }
	}
}



#define flash1_num 		1	   			//第5个
#define	flash1_speed 1
#define flash2_num 		4				//第6个
#define	flash2_speed 3
#define flash3_num 		0
#define	flash3_speed 0
#define flash4_num 		0
#define	flash4_speed 0
#define flash5_num 		0
#define	flash5_speed 0
#define flash6_num 		0
#define	flash6_speed 0
#define flash7_num 		0
#define	flash7_speed 0
#define flash8_num 		0
#define	flash8_speed 0
#define flash9_num 		0
#define	flash9_speed 0
#define flash10_num		0
#define	flash10_speed 0
#define flash11_num 	0
#define	flash11_speed 0
#define flash12_num 	0				  //第16个
#define	flash12_speed 0
#define pov_key_switch4			case 4:moshi=3;zi_num=12*32; P0=yaoyaozi5[lie+lie_temp];P2=yaoyaozi5[lie+1+lie_temp];	   break; 
#define pov_key_switch5			case 5: moshi=1;zi_num=2*112;P2=picture1[show_pic_numtemp][lie+lie_temp];P0=picture1[show_pic_numtemp][lie+1+lie_temp];	 flashall_num=flash1_num;flash_speed=flash1_speed;  break; 
#define pov_key_switch6			case 6: moshi=1;zi_num=2*112;P2=picture2[show_pic_numtemp][lie+lie_temp];P0=picture2[show_pic_numtemp][lie+1+lie_temp];  flashall_num=flash2_num;flash_speed=flash2_speed;  break; 
#define pov_key_switch7		//	case 7: moshi=1;zi_num=2*112;P2=picture3[show_pic_numtemp][lie+lie_temp];P0=picture3[show_pic_numtemp][lie+1+lie_temp];  flashall_num=flash3_num;flash_speed=flash3_speed;  break; 
#define pov_key_switch8		//	case 8: moshi=1;zi_num=2*112;P2=picture4[show_pic_numtemp][lie+lie_temp];P0=picture4[show_pic_numtemp][lie+1+lie_temp];  flashall_num=flash4_num;flash_speed=flash4_speed;  break; 
#define pov_key_switch9		//	case 9: moshi=1;zi_num=2*112;P2=picture5[show_pic_numtemp][lie+lie_temp];P0=picture5[show_pic_numtemp][lie+1+lie_temp];  flashall_num=flash5_num;flash_speed=flash5_speed;  break; 
#define pov_key_switch10	//	case 10: moshi=1;zi_num=2*112;P2=picture6[show_pic_numtemp][lie+lie_temp];P0=picture6[show_pic_numtemp][lie+1+lie_temp];  flashall_num=flash6_num;flash_speed=flash6_speed;  break; 
#define pov_key_switch11	//	case 11: moshi=1;zi_num=2*112;P2=picture7[show_pic_numtemp][lie+lie_temp];P0=picture7[show_pic_numtemp][lie+1+lie_temp];  flashall_num=flash7_num;flash_speed=flash7_speed;  break; 
#define pov_key_switch12	//	case 12: moshi=1;zi_num=2*112;P2=picture8[show_pic_numtemp][lie+lie_temp];P0=picture8[show_pic_numtemp][lie+1+lie_temp];  flashall_num=flash8_num;flash_speed=flash8_speed;  break; 
#define pov_key_switch13	//	case 13: moshi=1;zi_num=2*112;P2=picture9[show_pic_numtemp][lie+lie_temp];P0=picture9[show_pic_numtemp][lie+1+lie_temp];  flashall_num=flash9_num;flash_speed=flash9_speed;  break; 
#define pov_key_switch14	//	case 14: moshi=1;zi_num=2*112;P2=picture10[show_pic_numtemp][lie+lie_temp];P0=picture10[show_pic_numtemp][lie+1+lie_temp];  flashall_num=flash10_num;flash_speed=flash10_speed;  break; 
#define pov_key_switch15	//	case 15: moshi=1;zi_num=2*112;P2=picture11[show_pic_numtemp][lie+lie_temp];P0=picture11[show_pic_numtemp][lie+1+lie_temp];  flashall_num=flash11_num;flash_speed=flash11_speed;  break; 
#define pov_key_switch16	//	case 16: moshi=1;zi_num=2*112;P2=picture12[show_pic_numtemp][lie+lie_temp];P0=picture12[show_pic_numtemp][lie+1+lie_temp];  flashall_num=flash12_num;flash_speed=flash12_speed;  break; 
#define pov_picture_num 		6	  //总共几个  ，对应按键
				  
zhuan_pov5()   //显示图案	带自调整  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++==========
{
	uint time_max=1000,temp1=0,temp2=0;	  //指针和圆盘时钟用
	uint lie_temp=0,lie=0,i=0,j=0,beyond_delay=0,zi_num;      //beyond_delay文字延时数，j为转一圈显示的点数
	uchar  picture_temp=0,flashall_num=0;	     //picture_temp 对应按键  flashall_num帧数
	uchar show_pic_numtemp=0;       //显示动画的第几帧
	uchar show11[]="12:34:56";//... 30";
	uchar moshi=0;         //模式，  1、显示图案		2、显示数字时钟    3、显示文字			
	uchar delay_1=70,char_num=14;     //暂时不能改14，图像模式影响 	
	uchar *s=show11;
	uchar flash_speed=0;
	delay_ms(200);		
	P0=0xef;
	delay_ms(200);
	P0=0xff;
	delay_ms(200);		
	P0=0xef;
	delay_ms(200);
	P0=0xff;
	delay_ms(500);
	while(1)
	{    
		j++; 
/*1*/	if(button==0)		  //按键
		{	
			picture_temp++;		
			if(picture_temp>=pov_picture_num)    picture_temp=0;
			switch_key_show(picture_temp,pov_picture_num);


			lie_temp=0;lie=0;beyond_delay=0; //清零
/*1*/	}
		show11[0]=48+hour/10;
		show11[1]=48+hour%10;
		show11[3]=48+min/10;
		show11[4]=48+min%10;
		show11[6]=48+sec/10;
		show11[7]=48+sec%10;
///////////////////////////////////////////////////////////
while(picture_temp<2)		 //指针和圆盘时钟
{
	if(huoer_key==0) 			//低    到	
	{
/*		huoer_flag++;
	}
	if(huoer_flag>=1)
	{
*/		time_max=j;
		j=0;
//		huoer_flag=0;
		//P0=0xff;
		while(huoer_key==0); 
	}
		j++;
		delay_us(60);		 //亮
		//delay_us(80);		 //亮
		if(j>65500)  j=60000;

		temp1=((j*59)%(15* time_max));
		temp2=(j*59)%(5* time_max);
		if(temp2<=60)   {P0=0xfe;P2=0xff;}	   //12个点显示不全时，改右边值
		else   {P0=P2=0xff;}	 
		if(temp1<=60)   {P0&=0xf8;}			  //&很重要，考虑好

		temp1=j*59;
		temp2=time_max*sec;
/*1*/			if(button==0)		 //按键
				{	
					picture_temp++;	
					if(picture_temp>=pov_picture_num)	picture_temp=0;
					switch_key_show(picture_temp,pov_picture_num);


					lie_temp=0;lie=0;beyond_delay=0;i=0;  	//清零	?
/*1*/			}	

	if(picture_temp==1)
	{	//if(j*59/time_max==sec) { P2&=0x00;P0&=0x03;}
		if((temp1>temp2-30)&&(temp1<temp2+30)) { P2&=0x00;P0&=0x03;}   //指针
		temp2=j*59/time_max;
		if(temp2==min)   { P2&=0x00;P0&=0x1f;}
		temp1=hour%12*5+min/12;
		if(temp2==temp1) { P2&=0x00;P0&=0xff;}
	}
	else
	{	if(j*59/time_max<=sec) { P0&=0xEf;}		                      //圆盘
		temp2=j*59/time_max;
		if(temp2<=min) { P0&=0x7f;}
		temp1=hour%12*5+min/12;
		if(temp2<=temp1) { P2&=0xfB;}
	}
}/////////////////////////////////////////////////////////////////////
		switch(picture_temp+1)
		{					
			case 3:moshi=2;P0=ASCII[(*s-32)*8+i];i++;if(i>=8){ i=0;s++; }  break; 	//显示数字时钟		
			pov_key_switch4;
			pov_key_switch5;
			pov_key_switch6;
			pov_key_switch7;
			pov_key_switch8;
			pov_key_switch9;
			pov_key_switch10;
			pov_key_switch11;
			pov_key_switch12;
			pov_key_switch13;
			pov_key_switch14;
			pov_key_switch15;
			pov_key_switch16;
		}          

		if(moshi==1)	if(j*flash_speed<(beyond_delay+1)*char_num*8) P0 &= 0xfe;
	
		delay_us(delay_1);
		P0=0XFF;
		P2=0XFF;

		delay_us(delay_1/7);	 			
		if((lie+lie_temp>=zi_num-2)||(*s==0))		 //当前列  显示完,   即当前 瞬间（帧） 显示完毕或数字时钟显示完毕
		{		
			s=show11;	//数字时钟显示复位
			P0=P2=0xff;
			while(huoer_key==1)    //高		无	  ，全灭，卡到霍尔元件时
			{	
/*1*/			if(button==0)		 //按键
				{	
					picture_temp++;	
					if(picture_temp>=pov_picture_num)	picture_temp=0;
					switch_key_show(picture_temp,pov_picture_num);


					lie_temp=0;lie=0;beyond_delay=0;i=0;  	//清零
/*1*/			}
					delay_us(10);  i++;
			}					
			j=j+i*70/8/delay_1;	  //补足  相当时间的j的增量
			i=0;
		}
		if(huoer_key==1)	  	  //高          无    显示过程中（未显示完）  或 到霍尔元件之前  还没显示完）
		{
			if(lie+lie_temp<zi_num-2)
			{	lie_temp=lie_temp+2;}
		}
		if(huoer_key==0)	   	    //低		到		（到霍尔元件之前  显示完）	
		{	  	
			 if(j>char_num*8+1)	delay_1=delay_1+1;	  //自调整，根据电机速度调整画面
			 if(j<char_num*8-1)	delay_1=delay_1-1;
			 j=0;
			 
			 s=show11;i=0;P0=0xff;	


			if(lie>=zi_num-2)				//显示到最后
			{	lie=0; beyond_delay=0;	}	
			else
			{
				lie=lie+2;
			}
			if((moshi==1)|(moshi==2))lie=0;     //图案模式

			lie_temp=0;
			while(huoer_key==0);   	//低    到	 ，卡到离开霍尔元件
			if(moshi==3)		    //文字模式
			{
			 	if(beyond_delay<120)  {beyond_delay+=2;lie=0;}	//文字显示前的延时，
				else  beyond_delay=120;
				delay_us(delay_1*(120-beyond_delay));	   //待改，与j值联系  ！！
			}
			else	   //利用变量，		图画模式
			{


			 	beyond_delay++;
				if(beyond_delay>=flash_speed)	 //转动*圈后
				{
					beyond_delay=0;
					show_pic_numtemp++;		//动画的帧数
					if(show_pic_numtemp>=flashall_num)    show_pic_numtemp=0;
				}
			}
		}
	}
}

#define che_picture_num 5
#define che_show_times 300

bike1()
{
	uint i=0,show_temp=0;
	char picture_temp=0,auto_flag=1;
	delay_ms(200);
	P2=0xfe;
	delay_ms(200);
	P2=0xff;
	delay_ms(200);
	P2=0xfe;
	delay_ms(200);
	P2=0xff;
	delay_ms(500);

	while(1)
	{
		P2=che[2*i+32*picture_temp];
		P0=che[1+2*i+32*picture_temp];

		delay_us(90);//P0=P2=0xff;  
		delay_us(200);
		i++;
		if(2+2*i>=32)	  //picture  32字节显示完    重新显示
		{	i=0;show_temp++;	}
/**/	if(button==0)
		{	
			//auto_flag=0;
			picture_temp++;	
			if(auto_flag==1){auto_flag=0;picture_temp=0;}	
			if(picture_temp>=che_picture_num)   { picture_temp=-1; auto_flag=1;show_temp=0;}
			//if(picture_temp>=picture_num) {picture_temp=-1; show_temp=0; }
			switch_key_show(picture_temp+1,che_picture_num+1);

/**/	}
		if(auto_flag==1)
		{	
			if(picture_temp<=0)	  picture_temp=0;
			if(show_temp>=che_show_times)
			{
				show_temp=0;picture_temp++;		
				if(picture_temp>=che_picture_num)	picture_temp=0;	  //几段话				
			}	   //显示超过次数，下一picture
		}
	}
}
/***********************************主函数********************************/
main()
{
//	uint i,j;

    TMOD=0x01;
    TH0=0xEE;
    TL0=0x00;
    EA=1;
    ET0=1;
    TR0=1;


	huoer_key=1;

	ds1302_init();


	sec=ds1302_read(read_second);
	min=ds1302_read(read_minute);
	hour=ds1302_read(read_hour);
	day=ds1302_read(read_day);
	month=ds1302_read(read_month);
	year=ds1302_read(read_year); 

	if(day<32&&year==ds1302_read(read_year)&&month==ds1302_read(read_month)&&day==ds1302_read(read_day))
	{
		P2=0x00;
		delay_ms(500);
		P2=0xff;
	}
	if(yao_zhuan==0)
	{
		yaoyaobang1();
	}
	if(yao_zhuan==1&bike==1)
	{
		zhuan_pov5();	
	}
	if(bike==0&yao_zhuan==1)
	{
		bike1();
	}
}






timer0() interrupt 1
{
    TH0=0xEE; 
    TL0=0x00;
    sec_temp++;
    if(sec_temp==200)
	{	
		sec++;
		sec_temp=0;
	}
	if(sec==60)
	{
		min++;
		sec=0;
	}
	if(min==60)
	{
		hour++;
		min=0;
	}
}
