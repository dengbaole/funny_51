#ifndef eeprom_isp_H
#define eeprom_isp_H
				//5k    0x2000~0x33ff
//#define start_addr 0x2000	   //STC89C51,52,54
#define start_addr 0x8000	   //STC89C58,516

/*
void clean512(uint addr);  		 清空一扇区，地址0~512*10  已经里边加上0x000
void eeprom_write_char(uint addr,uchar dat);			地址0~512*10
uchar eeprom_read_char(uint addr);					地址0~512*10								  
void eeprom_write_str(uint i,uchar *dat);			地址0~512*10
*/
/*┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈ 
函数：关闭ISP/IAP操作 
┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈*/ 
void close() 
{ 
	EA = 1;///////////////////
	ISP_CONTR = 0;            // 关闭IAP功能 
	ISP_CMD   = 0;            // 待机模式，无ISP操作 
	ISP_TRIG  = 0;            // 关闭IAP功能, 清与ISP有关的特殊功能寄存器  
}
/*┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈ 
函数：擦除某一扇区（每个扇区512字节） 
入口：addr = 某一扇区首地址			   
┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈*/ 
void clean512(uint addr) 
{	 
	addr+=start_addr;					          // 0x83(晶振<5M)   0x82(晶振<10M)   0x81(晶振<20M)   0x80(晶振<40M) 
    ISP_CONTR = 0x81;	      // 打开 IAP 功能(ISPEN(ISP_CONTR.7)=1:允许编程改变Flash, 设置 Flash 操作等待时间。 
    ISP_CMD   = 0x03;		  // 用户可以对"Data Flash/EEPROM区"进行扇区擦除 
    ISP_ADDRL = addr;         // ISP/IAP操作时的地址寄存器低八位， 
    ISP_ADDRH = addr>>8;      // ISP/IAP操作时的地址寄存器高八位。  
	EA =0;    
    ISP_TRIG = 0x46;          // 在ISPEN(ISP_CONTR.7)=1时,对ISP_TRIG先写入46h， 
    ISP_TRIG = 0xB9;          // 再写入B9h,ISP/IAP命令才会生效。 
	//delay_us(1);    

 
    close();					  // 关闭ISP/IAP 
} 
/*┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈ 
函数：写一字节 
入口：addr = 扇区单元地址 , dat = 待写入数据 
┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈*/ 
void eeprom_write_char(uint addr,uchar dat) 
{ 
/*	
STC89C51RC，STC89LE51RC     0x2000 共八个扇区
STC89C52RC，STC89LE52RC     0x2000 共八个扇区
STC89C54RD+，STC89LE54RD+   0x8000 共五十八个扇区
STC89C55RD+，STC89LE55RD+   0x8000 共五十八个扇区
STC89C58RD+，STC89LE58RD+   0x8000 共五十八个扇区
*/	
    addr+=start_addr;
	ISP_CONTR = 0x81;		   
    ISP_CMD   = 0x02;	      // 用户可以对"Data Flash/EEPROM区"进行字节编程 
    ISP_ADDRL = addr;         
    ISP_ADDRH = addr>>8;       
    ISP_DATA  = dat;          // 数据进ISP_DATA 
    EA = 0; 
    ISP_TRIG = 0x46;           
    ISP_TRIG = 0xB9; 

	delay_us(1);           

    close();					  // 关闭ISP/IAP 
} 
/*┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈ 
函数：读一字节 
入口：addr = 扇区单元地址 
出口：dat  = 读出的数据 
┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈*/ 
uchar eeprom_read_char(uint addr) 
{    
    uchar dat; 
	addr+=start_addr;	 
	ISP_CONTR = 0x81;		   
    ISP_CMD   = 0x01;         // 用户可以对"Data Flash/EEPROM区"进行字节读 
    ISP_ADDRL = addr;          
    ISP_ADDRH = addr>>8;       
    EA = 0; 
    ISP_TRIG = 0x46;           
    ISP_TRIG = 0xB9;  
	//delay_us(1);          

    dat = ISP_DATA;			  // 取出数据 
	close();					  // 关闭ISP/IAP     可以不加！！	           
	return dat; 
} 
void eeprom_write_str(uint i,uchar *dat)
{
	while (*dat) 
	{
    	eeprom_write_char(i,*dat);
    	dat++;
		i++;
	}	
}





#endif 