#include<reg51.h>
#include "ceshi.c"
#include "ASCII8.8.h"
#include "delay.h"
#include "ds1302.h"

#define ulong unsigned long
#define uint unsigned int
#define uchar unsigned char
sbit zhendong_key=P3^2 ;   	 //震动开关
sbit button=P3^7;             //按键
sbit huoer_key=P1^0;	     //	霍尔传感器
sbit yao_zhuan=P1^5;	   	// 开关  摇摇棒
sbit bike=P3^1; 		    //开关  选择车轮

uint sec_temp=0;
uchar sec=0,min=27,hour=6,day=32,month=6,year=18;
/*****延时子程序*****/
/*void delay_us(uint N)
{
	uint x;
	for(x=0; x<=N;x++);
}
delay_ms(uint s)
{
	uint i,j;
	for(j=s;j>0;j--)
	for(i=114;i>0;i--);
}
*/
delay_us(uint s)
{
   	//uint i;
	//for(i=s;i>0;i--);
	while(s--);
}

/*void yaoyaobang1(void)	             //摇摇棒
{
    uint a,b,c,picture_num,z1,z2,z3,z4,zi_num,picture_temp=0,show_temp=0,i;     //picture_temp为按键次数；show_temp为同屏temp
    a=4;				//a每次显示字数    b切换第几屏     c显示同屏几次
    c=8;		    //picture_num共几个画面    z1z2z3z4各自话中几个字（32b）
    picture_num=4;
    z1=3;z2=2;z3=3;z4=12;
    button=zhendong_key=1;
	while(1)     //主程序中只检测按键
	{
		if(button==0)
		{
			delay_us(1000);
			if(button==0)
			{
				picture_temp++;b=show_temp=0;
			}
			while(button==0);
		}
		if(picture_temp>picture_num-1)
		{
			picture_temp=0;
		}
		if(zhendong_key==0)
		{
			show_temp++;
			delay_us(5000);
			for(i=0;i<16*a;i++)
			{
				switch(picture_temp)
				{
					case 0:P0=~zi1[32*a*b+i*2];P2=~zi1[32*a*b+i*2+1];zi_num=z1;break;
					case 1:P0=~zi2[32*a*b+i*2];P2=~zi2[32*a*b+i*2+1];zi_num=z2;break;
					case 2:P0=~zi3[32*a*b+i*2];P2=~zi3[32*a*b+i*2+1];zi_num=z3;break;
					case 3:P0=~zi4[32*a*b+i*2];P2=~zi4[32*a*b+i*2+1];zi_num=z4;break;
				}
				delay_us(100);
				if(32*a*b+i*2+2==32*zi_num)
				{
					i=16*a-1;
					if(show_temp>c-1)
					{b=show_temp=0;}
				}
			}
        }
		if(32*a*b+i*2+2<32*zi_num)
		{
			if(show_temp>c-1)
			{show_temp=0;b++;}
        }
	}
}  */
void yaoyaobang1(void)	             //摇摇棒
{
		 				   //picture_temp为图片次数temp；show_temp为同一小页面显示 次数temp
    uint show_once_num,show_page=0,show_times,picture_num,z1,z2,z3,z4,zi_num,picture_temp=0,show_temp=0,i; 
	  
    show_once_num=4;	//show_once_num每小页面显示字数 show_page切换第几个小页面  show_times小页面显示几次
    show_times=8;		           
    picture_num=4;		//picture_num共几个画面    z1z2z3z4各自话中几个字（32Byte）
    z1=3;z2=2;z3=3;z4=12;
    //z1=4;z2=4;z3=3;z4=12;
    button=zhendong_key=1;

	delay_ms(500);		 ////////////待改
	P0=0xfe;
	delay_ms(200);
	P0=0xff;
	delay_ms(200);
	P0=0xfe;
	delay_ms(200);
	P0=0xff;
	delay_ms(500);

	while(1)     //主程序中只检测按键
	{
		if(button==0)	  
		{
			delay_ms(10);
			if(button==0)		 //切换picture
			{
				picture_temp++;show_page=show_temp=0;
				if(picture_temp>=picture_num)
				{
					picture_temp=0;
				}	
			}
			while(button==0);
			delay_ms(50);

			P0=~(1<<picture_temp);	  //不同picture的切换提示
			delay_ms(500);
			P0=0xff;
		}
		
		if(zhendong_key==0)		// 摇动时
		{
			delay_ms(10);
			if(zhendong_key==0)
			{
				show_temp++;
				delay_ms(55);   
				for(i=0;i<16*show_once_num;i++)	   //1 page
				{
					switch(picture_temp)
					{
						case 0:P0=~zi1[32*show_once_num*show_page+i*2];P2=~zi1[32*show_once_num*show_page+i*2+1];zi_num=z1;break;
						case 1:P0=~zi2[32*show_once_num*show_page+i*2];P2=~zi2[32*show_once_num*show_page+i*2+1];zi_num=z2;break;
						case 2:P0=~zi3[32*show_once_num*show_page+i*2];P2=~zi3[32*show_once_num*show_page+i*2+1];zi_num=z3;break;
						case 3:P0=~zi4[32*show_once_num*show_page+i*2];P2=~zi4[32*show_once_num*show_page+i*2+1];zi_num=z4;break;
					}
					delay_us(100);		   //亮瞬间的时间
					if(32*show_once_num*show_page+i*2+2>=32*zi_num) //是否到最后，包括未满预定字数   
					{
						if(show_temp>=show_times)		//从头开始显示
						{show_page=show_temp=0;}
						i=16*show_once_num-1;
						break;
					}
				}
				if(32*show_once_num*show_page+i*2+2<32*zi_num) 					 
				{
					if(show_temp>=show_times)	 //未到最后，换显示下一小页面
					{show_temp=0;show_page++;}
				} 
			}
        }
	}
}					  
zhuan_pov()
{
	uint lie_temp=0,beyond_delay=0,zi_num,lie=0,picture_temp=0,picture_num=2;//3;		   /*共有z个元素*/
	
	delay_ms(200);		//模式2    闪2次
	P0=0xfd;
	delay_ms(200);
	P0=0xff;
	delay_ms(200);
	P0=0xfd;
	delay_ms(200);
	P0=0xff;
	delay_ms(500);

	while(1)
	{         
/*1*/	if(button==0)		  //按键
		{	
			picture_temp++;
			while(~button);		
			if(picture_temp>=picture_num)	picture_temp=0;

			P0=~(1<<picture_temp);	  //不同picture的切换提示
			delay_ms(500);
			P0=0xff;

			lie_temp=0;lie=0;beyond_delay=0;  	//清零
/*1*/	}

		switch(picture_temp)
		{
			case 0:zi_num=6*32; P2=love[lie-lie_temp];P0=love[lie+1-lie_temp];	   break; 
			case 1:zi_num=6*32; P2=love1[lie-lie_temp];P0=love1[lie+1-lie_temp];	break; 
		//	case 2:zi_num=8*94; P0=ASCII[lie-lie_temp/2];	break; 
		}          
		delay_us(70);		 //亮
		P0=0XFF;
		P2=0XFF;
		delay_us(10);	   //灭
		if(lie-lie_temp==0)					 //当前列  显示完,   即当前 瞬间 显示完毕
		{		
			P0=P2=0xff;
			while(huoer_key==1)	 	//高		无	  ，全灭，卡到霍尔元件时
			{	
/*1*/			if(button==0)		 //按键
				{	
					picture_temp++;
					while(~button);		
					if(picture_temp>=picture_num)	picture_temp=0;
		
					P0=~(1<<picture_temp);	  //不同picture的切换提示
					delay_ms(500);
					P0=0xff;

					lie_temp=0;lie=0;beyond_delay=0;  	//清零
/*1*/			}
			}
		}
		if(huoer_key==1)	  	  //高		无    显示过程中（未显示完）  或 到霍尔元件之前  还没显示完）
		{
			if(lie-lie_temp>0)
			{	lie_temp=lie_temp+2;	}
		}
		if(huoer_key==0)	   	    //低		到		（到霍尔元件之前  显示完）	
		{	  	
			if(lie_temp==0)			 //大作用 ，巧合？？移动到最后重新开始
			{	lie=0; beyond_delay=0;  		}	
			
			 
			if(lie>=zi_num-2)		  //显示 文字 最后一列时
			{
				//lie=zi_num-4;
				lie=zi_num-2;

				beyond_delay++;	     //  延时增加
			}
			else
			{	lie=lie+2;	}
			lie_temp=0;
			while(huoer_key==0); 			//低    到	 ，卡到离开霍尔元件
			delay_us(beyond_delay*100);				//全显示出来，过  霍尔元件
		}
	}
}



zhuan_pov34()   //数字时钟，自调整，固定画面++++++++++++++++++++++++++++++++++++++++++++++++++++++==========
{
	uint lie_temp=0,i=0,j=0,beyond_delay=0,zi_num,lie=0;		   /*共有z个元素*/
	uchar  picture_temp=0,picture_num=2;//3;
	uchar show11[]="12:34:56::1230";
						
	uchar char_num=14,delay_1=70; 	

	uchar *s=show11;
	delay_ms(200);		//模式2    闪2次
	P0=0xfd;
	delay_ms(200);
	P0=0xff;
	delay_ms(500);
	 
	while(1)
	{    
		j++;
	     
/*1*/	if(button==0)		  //按键
		{	
			picture_temp++;
			while(~button);		
			if(picture_temp>=picture_num) picture_temp=0;

			P0=~(1<<picture_temp);	  //不同picture的切换提示
			delay_ms(500);
			P0=0xff;

			lie_temp=0;lie=0;beyond_delay=0; //清零
/*1*/	}


		show11[0]=48+hour/10;
		show11[1]=48+hour%10;
		show11[3]=48+min/10;
		show11[4]=48+min%10;
		show11[6]=48+sec/10;
		show11[7]=48+sec%10;



		switch(picture_temp)
		{					

		
			case 0:zi_num=12*32; P0=~zi4[lie+lie_temp];P2=~zi4[lie+1+lie_temp];	   break; 
			//case 1:zi_num=6*32; P2=love1[lie+lie_temp];P0=love1[lie+1+lie_temp];	break; 
			//case 0:zi_num=8*94*2; P0=ASCII[(lie+lie_temp)/2];	break; 
			case 1:  /*  while (*s) 
				{
					zi_num=8*lie_temp*2;
   				 	lie_temp++;
    				s ++;
			  }	
			  	  lie_temp=0;  */


						P0=ASCII[(*s-33)*8+i];     
						i++;
						if(i>=8)
						{
							i=0;
							s ++;
							if(*s==0)    
							 { 
							 	/*P0=0xff;*/  s=show11;
								while(huoer_key==1);
								
								delay_1=delay_1+1;
								 j=char_num*8;		
							}
				  		}
					break; 		


		}          
		//delay_us(70);		 //亮
		delay_us(delay_1);
		P0=0XFF;
		P2=0XFF;
		//delay_us(10);	   //灭
		delay_us(delay_1/7);	 
		//if(lie-lie_temp==0)			

		 //当前列  显示完,   即当前 瞬间 显示完毕
		if(lie+lie_temp>=zi_num-2)		
		{		
			P0=P2=0xff;
			while(huoer_key==1)    //高		无	  ，全灭，卡到霍尔元件时
			{	
/*1*/			if(button==0)		 //按键
				{	
					picture_temp++;
					while(~button);	

	
					if(picture_temp>=picture_num)	picture_temp=0;
		
					P0=~(1<<picture_temp);	  //不同picture的切换提示
					delay_ms(500);
					P0=0xff;
	

					lie_temp=0;lie=0;beyond_delay=0;  	//清零
/*1*/			}
			}
		}
		if(huoer_key==1)	  	  //高          无    显示过程中（未显示完）  或 到霍尔元件之前  还没显示完）
		{
			//if(lie-lie_temp>0)
			if(lie+lie_temp<zi_num-2)
			{	lie_temp=lie_temp+2;}
		}
		if(huoer_key==0)	   	    //低		到		（到霍尔元件之前  显示完）	
		{	  	
			//if(lie_temp==0)      //大作用 ，巧合？？移动到最后重新开始
			


			 delay_1=delay_1-(char_num*8+1-j)/3;
			 j=0;
			 
			 
			 s=show11;i=0;P0=0xff;	





			//	lie=0;


			if(lie>=zi_num-2)
			{	lie=0; beyond_delay=0;	}	
			else
			{
				lie=lie+2;
			}
			
			 
			//if(lie>=zi_num-2)		  //显示 文字 最后一列时
		/*	if(lie+lie_temp>=zi_num-2)
			{
		
			//	lie=zi_num-2;

				//beyond_delay++;	     //延时增加
			}
			else
			{	lie=lie+2;	} */

			lie_temp=0;
			while(huoer_key==0);   	//低    到	 ，卡到离开霍尔元件
		//	delay_us(beyond_delay*100);		

		//全显示出来，过  霍尔元件
		}
	}
}

zhuan_pov5()   //显示图案	带自调整  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++==========
{
	uint lie_temp=0,i=0,j=0,beyond_delay=0,zi_num,lie=0;		   /*共有z个元素*/
	uchar  picture_temp=0,picture_num=3;
	uchar show11[]="12:34:56::1230";
	uchar moshi=0;//模式，  1、显示图案		2、显示数字时钟  3、显示文字			
	uchar char_num=14,delay_1=70; 	

	uchar *s=show11;
	delay_ms(200);		//模式2    闪2次
	P0=0xfd;
	delay_ms(200);
	P0=0xff;
	delay_ms(500);
	 
	while(1)
	{    
		j++;
	     
/*1*/	if(button==0)		  //按键
		{	
			picture_temp++;
			while(~button);		
			if(picture_temp>=picture_num) picture_temp=0;

			P0=~(1<<picture_temp);	  //不同picture的切换提示
			delay_ms(500);
			P0=0xff;

			lie_temp=0;lie=0;beyond_delay=0; //清零
/*1*/	}


		show11[0]=48+hour/10;
		show11[1]=48+hour%10;
		show11[3]=48+min/10;
		show11[4]=48+min%10;
		show11[6]=48+sec/10;
		show11[7]=48+sec%10;



		switch(picture_temp)
		{					

		
			case 2:moshi=3; zi_num=12*32; P0=~zi4[lie+lie_temp];P2=~zi4[lie+1+lie_temp];	   break; 
			//case 1:moshi=3;zi_num=6*32; P2=love1[lie+lie_temp];P0=love1[lie+1+lie_temp];	break; 
			//case 1:zi_num=8*94*2; P0=ASCII[(lie+lie_temp)/2];	break; 
			case 0:  /*  while (*s) 
				{
					zi_num=8*lie_temp*2;
   				 	lie_temp++;
    				s ++;
			  }	
			  	  lie_temp=0;  */

				  moshi=2;
						P0=ASCII[(*s-33)*8+i];     
						i++;
						if(i>=8)
						{
							i=0;
							s ++;
							if(*s==0)    
							 { 
							 	/*P0=0xff;*/  s=show11;
								while(huoer_key==1)
								{

/*1*/	if(button==0)		  //按键
		{	
			picture_temp++;
			while(~button);		
			if(picture_temp>=picture_num) picture_temp=0;

			P0=~(1<<picture_temp);	  //不同picture的切换提示
			delay_ms(500);
			P0=0xff;

			lie_temp=0;lie=0;beyond_delay=0; //清零
/*1*/	}


								//delay_us(10);  i++;
								}
								//i=0;
								delay_1=delay_1+1;
								 j=char_num*8;		
						    	}
				  		}  	
						break; 	

	
			 case 1: moshi=1;zi_num=2*112;P2=picture1[lie+lie_temp];P0=picture1[lie+1+lie_temp];	   break; 




		}          
		//delay_us(70);		 //亮
		delay_us(delay_1);
		P0=0XFF;
		P2=0XFF;
		//delay_us(10);	   //灭
		delay_us(delay_1/7);	 
		//if(lie-lie_temp==0)			

		 //当前列  显示完,   即当前 瞬间 显示完毕
		if(lie+lie_temp>=zi_num-2)		
		{		
			P0=P2=0xff;
			while(huoer_key==1)    //高		无	  ，全灭，卡到霍尔元件时
			{	
/*1*/			if(button==0)		 //按键
				{	
					picture_temp++;
					while(~button);	

	
					if(picture_temp>=picture_num)	picture_temp=0;
		
					P0=~(1<<picture_temp);	  //不同picture的切换提示
					delay_ms(500);
					P0=0xff;
	

					lie_temp=0;lie=0;beyond_delay=0;  	//清零
/*1*/			}
			}


			if(moshi==1)
			{
				delay_1=delay_1+1;	 //显示图案用的
				j=char_num*8;
			}

		}
		if(huoer_key==1)	  	  //高          无    显示过程中（未显示完）  或 到霍尔元件之前  还没显示完）
		{
			//if(lie-lie_temp>0)
			if(lie+lie_temp<zi_num-2)
			{	lie_temp=lie_temp+2;}
		}
		if(huoer_key==0)	   	    //低		到		（到霍尔元件之前  显示完）	
		{	  	
			//if(lie_temp==0)      //大作用 ，巧合？？移动到最后重新开始
			


			 delay_1=delay_1-(char_num*8+1-j)/3;
			 j=0;
			 
			 
			 s=show11;i=0;P0=0xff;	





			if((moshi==1)|(moshi==2))lie=0;     //图案模式

			if(lie>=zi_num-2)
			{	lie=0; beyond_delay=0;	}	
			else
			{
				lie=lie+2;
			}
			
			 
			//if(lie>=zi_num-2)		  //显示 文字 最后一列时
		/*	if(lie+lie_temp>=zi_num-2)
			{
		
			//	lie=zi_num-2;

				//beyond_delay++;	     //延时增加
			}
			else
			{	lie=lie+2;	} */

			lie_temp=0;
			while(huoer_key==0);   	//低    到	 ，卡到离开霍尔元件
		//	delay_us(beyond_delay*100);		

		//全显示出来，过  霍尔元件
		}
	}
}








show_time()
{
	uint j=0,delay0=50,temp=0;
	uchar time_max=600;
	P0=0xff;
while(1)
{
	if(huoer_key==0) 			//低    到	
	{
		//time_max=j;
		//j=0;
		if(j>time_max+1) delay0+=1;
		if(j<time_max-1) delay0-=1;

	//	if(j==time_max) P2=0x00;

		j=0;
		while(huoer_key==0); 
	}
	if(huoer_key==1)		 	//高    未到
	{
		j++;
		//delay_us(47);		 //亮
		delay_us(delay0);		 //亮
		if(j>65500)  j=60000;

		temp=((j*59)%(15* time_max));
		if(((j*59)%(5* time_max))<=60)   {P0=0xfe;P2=0xff;}	   //12个点显示不全时，改右边值
	
		if(temp<=60)   {P0&=0xf8;}	

		if(((j*59)%(5* time_max))>60)   {P0=P2=0xff;}	 


		//if(j*58/time_max==sec) { P2&=0;P0&=0x03;}
		if((j*59/time_max==sec)&&(j*59/sec%time_max<=26)) { P2&=0;P0&=0x03;}
		//if(j*58/time_max==min) { P2&=0;P0&=0x1f;}
		//if(j*58/time_max==hour*5+min/12) { P2&=0x00;P0&=0xff;}
	}
}
}




show_time11()
{
	uint j=0,time_max=1000,temp=0;
	P0=0xff;
while(1)
{
	if(huoer_key==0) 			//低    到	
	{
		time_max=j;
		j=0;
		//P0=0xff;
		while(huoer_key==0); 
	}
	if(huoer_key==1)		 	//高    未到
	{
		j++;
		 /*
		switch(j*60/time_max)
		{
			case 5:		P0=0xfc;break;
			case 10:	P0=0xfc;break;
			case 15:	P0=0xfc;break;
			case 20:	P0=0xfc;break;
			case 25:	P0=0xfc;break;
			case 30:	P0=0xfc;break;
			case 35:	P0=0xfc;break;
			case 40:	P0=0xfc;break;
			case 45:	P0=0xfc;break;
			case 50:	P0=0xfc;break;
			case 55:	P0=0xfc;break;
			case 60:	P0=0xfc;break;
			default:	P0=P2=0xff;break;
		} */ 
		//delay_us(47);		 //亮
		delay_us(80);		 //亮
		if(j>65500)  j=20000;

		temp=((j*59)%(15* time_max));
		if(((j*59)%(5* time_max))<=60)   {P0=0xfe;P2=0xff;}	   //12个点显示不全时，改右边值
		//else   P0=P2=0xff;
		if(temp<=60)   {P0&=0xf8;}//P2=0xff;}
			
		if(((j*59)%(5* time_max))>60)   {P0=P2=0xff;}	 

		 


		if(j*59/time_max==sec) { P2&=0;P0&=0x03;}
		//else  { P2|=0xff;P0|=~0x03;    }
		if(j*59/time_max==min) { P2&=0;P0&=0x1f;}
	
		if(j*59/time_max==hour*5+min/12) { P2&=0x00;P0&=0xff;}
	}
}
}

bike1()
{
	uint i=0,picture_temp,show_temp=0,auto_flag=1,picture_num=5,show_times=300;

	delay_ms(200);
	P0=0xfb;
	delay_ms(200);
	P0=0xff;
	delay_ms(200);
	P0=0xfb;
	delay_ms(200);
	P0=0xff;
	delay_ms(500);

	while(1)
	{
		P2=che[2*i+32*picture_temp];
		P0=che[1+2*i+32*picture_temp];

		delay_us(90);//P0=P2=0xff;  
		delay_us(200);
		i++;
		if(2+2*i>=32)	  //picture  32字节显示完    重新显示
		{	i=0;show_temp++;	}
		if(button==0)
		{	
			auto_flag=0;
			while(~button);
			picture_temp++;
		
			
			P0=~(1<<picture_temp);	  //不同picture的切换提示
			if(picture_temp>=8)   P2=~(1<<(picture_temp-8));
			else     P2=0xff;
			delay_ms(1000);
			P0=0xff;
		}


		if(auto_flag==1)
		{	
			if(show_temp>=show_times){		show_temp=0;picture_temp++;	}	   //显示超过次数，下一picture
		}
		if(picture_temp>=picture_num)	picture_temp=0;	  //几段话
	}
}
   


/***********************************主函数********************************/
main()
{
//	uint i,j;

    TMOD=0x01;
    TH0=0xEE;
    TL0=0x00;
    EA=1;
    ET0=1;
    TR0=1;


	huoer_key=1;

	ds1302_init();


	sec=ds1302_read(read_second);
	min=ds1302_read(read_minute);
	hour=ds1302_read(read_hour);
	day=ds1302_read(read_day);
	month=ds1302_read(read_month);
	year=ds1302_read(read_year); 

	if(day<32&&year==ds1302_read(read_year)&&month==ds1302_read(read_month)&&day==ds1302_read(read_day))
	{
		P2=0x00;
		delay_ms(500);
		P2=0xff;
	}


	if(yao_zhuan==0)
	{
		yaoyaobang1();
	}
	if(yao_zhuan==1&bike==1)
	{
		//zhuan_pov3();
		//zhuan_pov34();

		//zhuan_pov5();
		//show_time11();
		show_time();
	}
	if(bike==0&yao_zhuan==1)
	{
		bike1();
	}
}






timer0() interrupt 1
{
    TH0=0xEE; 
    TL0=0x00;
    sec_temp++;
    if(sec_temp==200)
	{	
		sec++;
		sec_temp=0;
	}
	if(sec==60)
	{
		min++;
		sec=0;
	}
	if(min==60)
	{
		hour++;
		min=0;
	}
}
