#include  <REG51.H>
#include  <math.h>    //Keil library  
#include  <stdio.h>   //Keil library  
#include  <INTRINS.H>
#include "uart.h"
#include "eeprom_isp.h"

#define   uchar unsigned char
#define   uint unsigned int
#define   DataPort P1      //LCD1602数据端口
sbit      SCL = P2 ^ 4;    //IIC时钟引脚定义
sbit      SDA = P2 ^ 3;    //IIC数据引脚定义
sbit      LCM_RS = P2 ^ 0; //LCD1602命令端口
sbit      LCM_RW = P2 ^ 1; //LCD1602命令端口
sbit      LCM_EN = P2 ^ 2; //LCD1602命令端口
sbit      clear_all = P3 ^ 7;   //清除总步数按键
sbit      clear_now = P3 ^ 6;	//清除当次步数按键
sbit      Show_other = P3 ^ 5;	//切换显示按键
#define SlaveAddress   0xA6     //定义器件在IIC总线中的从地址,根据ALT  ADDRESS地址引脚不同修改
//ALT  ADDRESS引脚接地时地址为0xA6，接电源时地址为0x3A
typedef unsigned char  BYTE;
typedef unsigned short WORD;

BYTE BUF[8];                         //接收数据缓存区
uchar ge, shi, bai, qian, wan;       //显示变量
int  dis_data;                       //变量

uint average_all = 0;         //3个方向xyz  的  average_all_times次的平均值
uint average_all_temp = 0;    //单次的数据值
uint average_all_num = 0;     //第几个
#define average_all_times 3   //当获得3次数据  后取平均为average_all
uint step = 0;                //当次步数
uint step_all = 0;            //总步数
char Show_other_flag = 0;	  //对应Show_other按键的   模式值

void delay(unsigned int k);
void InitLcd();                      //初始化lcd1602
void Init_ADXL345(void);             //初始化ADXL345

void WriteDataLCM(uchar dataW);
void WriteCommandLCM(uchar CMD, uchar Attribc);
void DisplayOneChar(uchar X, uchar Y, uchar DData);
void conversion(uint temp_data);

void  Single_Write_ADXL345(uchar REG_Address, uchar REG_data);  //单个写入数据
uchar Single_Read_ADXL345(uchar REG_Address);                   //单个读取内部寄存器数据
void  Multiple_Read_ADXL345();                                  //连续的读取内部寄存器数据
//------------------------------------
void Delay5us();
void Delay5ms();
void ADXL345_Start();
void ADXL345_Stop();
void ADXL345_SendACK(bit ack);
bit  ADXL345_RecvACK();
void ADXL345_SendByte(BYTE dat);
BYTE ADXL345_RecvByte();
void ADXL345_ReadPage();
void ADXL345_WritePage();
//-----------------------------------

//*********************************************************
void conversion(uint temp_data)	   //将整数转成一个个字符
{
  wan = temp_data / 10000 + 0x30 ;
  temp_data = temp_data % 10000; //取余运算
  qian = temp_data / 1000 + 0x30 ;
  temp_data = temp_data % 1000; //取余运算
  bai = temp_data / 100 + 0x30   ;
  temp_data = temp_data % 100; //取余运算
  shi = temp_data / 10 + 0x30    ;
  temp_data = temp_data % 10;  //取余运算
  ge = temp_data + 0x30;
}

/*******************************/
void delay(unsigned int k)	    //延时函数ms
{
  unsigned int i, j;
  for (i = 0; i < k; i++)
  {
    for (j = 0; j < 121; j++)
    {
      ;
    }
  }
}
/*******************************/
void WaitForEnable(void)
{
  DataPort = 0xff;
  LCM_RS = 0; LCM_RW = 1; _nop_();
  LCM_EN = 1; _nop_(); _nop_();
  while (DataPort & 0x80);
  LCM_EN = 0;
}
/*******************************/
void WriteCommandLCM(uchar CMD, uchar Attribc)
{
  if (Attribc)WaitForEnable();
  LCM_RS = 0; LCM_RW = 0; _nop_();
  DataPort = CMD; _nop_();
  LCM_EN = 1; _nop_(); _nop_(); LCM_EN = 0;
}
/*******************************/
void WriteDataLCM(uchar dataW)
{
  WaitForEnable();
  LCM_RS = 1; LCM_RW = 0; _nop_();
  DataPort = dataW; _nop_();
  LCM_EN = 1; _nop_(); _nop_(); LCM_EN = 0;
}
/***********************************/
void InitLcd()				 //初始化LCD
{
  WriteCommandLCM(0x38, 1);
  WriteCommandLCM(0x08, 1);
  WriteCommandLCM(0x01, 1);
  WriteCommandLCM(0x06, 1);
  WriteCommandLCM(0x0c, 1);
}
/***********************************/
void DisplayOneChar(uchar X, uchar Y, uchar DData)	  //在lcd特定位置显示字符
{
  Y &= 1;
  X &= 15;
  if (Y)X |= 0x40;
  X |= 0x80;
  WriteCommandLCM(X, 0);
  WriteDataLCM(DData);
}

/**************************************
  延时5微秒(STC90C52RC@12M)
  不同的工作环境,需要调整此函数，注意时钟过快时需要修改
  当改用1T的MCU时,请调整此延时函数
**************************************/
void Delay5us()
{
  _nop_(); _nop_(); _nop_(); _nop_();
  _nop_(); _nop_(); _nop_(); _nop_();
  _nop_(); _nop_(); _nop_(); _nop_();
}

/**************************************
  延时5毫秒(STC90C52RC@12M)
  不同的工作环境,需要调整此函数
  当改用1T的MCU时,请调整此延时函数
**************************************/
void Delay5ms()
{
  WORD n = 560;

  while (n--);
}

/**************************************
  起始信号
**************************************/
void ADXL345_Start()
{
  SDA = 1;                    //拉高数据线
  SCL = 1;                    //拉高时钟线
  Delay5us();                 //延时
  SDA = 0;                    //产生下降沿
  Delay5us();                 //延时
  SCL = 0;                    //拉低时钟线
}

/**************************************
  停止信号
**************************************/
void ADXL345_Stop()
{
  SDA = 0;                    //拉低数据线
  SCL = 1;                    //拉高时钟线
  Delay5us();                 //延时
  SDA = 1;                    //产生上升沿
  Delay5us();                 //延时
}

/**************************************
  发送应答信号
  入口参数:ack (0:ACK 1:NAK)
**************************************/
void ADXL345_SendACK(bit ack)
{
  SDA = ack;                  //写应答信号
  SCL = 1;                    //拉高时钟线
  Delay5us();                 //延时
  SCL = 0;                    //拉低时钟线
  Delay5us();                 //延时
}

/**************************************
  接收应答信号
**************************************/
bit ADXL345_RecvACK()
{
  SCL = 1;                    //拉高时钟线
  Delay5us();                 //延时
  CY = SDA;                   //读应答信号
  SCL = 0;                    //拉低时钟线
  Delay5us();                 //延时

  return CY;
}

/**************************************
  向IIC总线发送一个字节数据
**************************************/
void ADXL345_SendByte(BYTE dat)
{
  BYTE i;

  for (i = 0; i < 8; i++)     //8位计数器
  {
    dat <<= 1;              //移出数据的最高位
    SDA = CY;               //送数据口
    SCL = 1;                //拉高时钟线
    Delay5us();             //延时
    SCL = 0;                //拉低时钟线
    Delay5us();             //延时
  }
  ADXL345_RecvACK();
}

/**************************************
  从IIC总线接收一个字节数据
**************************************/
BYTE ADXL345_RecvByte()
{
  BYTE i;
  BYTE dat = 0;

  SDA = 1;                    //使能内部上拉,准备读取数据,
  for (i = 0; i < 8; i++)     //8位计数器
  {
    dat <<= 1;
    SCL = 1;                //拉高时钟线
    Delay5us();             //延时
    dat |= SDA;             //读数据
    SCL = 0;                //拉低时钟线
    Delay5us();             //延时
  }
  return dat;
}

//******单字节写入*******************************************

void Single_Write_ADXL345(uchar REG_Address, uchar REG_data)
{
  ADXL345_Start();                  //起始信号
  ADXL345_SendByte(SlaveAddress);   //发送设备地址+写信号
  ADXL345_SendByte(REG_Address);    //内部寄存器地址，请参考中文pdf22页
  ADXL345_SendByte(REG_data);       //内部寄存器数据，请参考中文pdf22页
  ADXL345_Stop();                   //发送停止信号
}

//********单字节读取*****************************************
uchar Single_Read_ADXL345(uchar REG_Address)
{ uchar REG_data;
  ADXL345_Start();                          //起始信号
  ADXL345_SendByte(SlaveAddress);           //发送设备地址+写信号
  ADXL345_SendByte(REG_Address);            //发送存储单元地址，从0开始
  ADXL345_Start();                          //起始信号
  ADXL345_SendByte(SlaveAddress + 1);       //发送设备地址+读信号
  REG_data = ADXL345_RecvByte();            //读出寄存器数据
  ADXL345_SendACK(1);
  ADXL345_Stop();                           //停止信号
  return REG_data;
}
//*********************************************************
//
//连续读出ADXL345内部加速度数据，地址范围0x32~0x37
//
//*********************************************************
void Multiple_read_ADXL345(void)
{ uchar i;
  ADXL345_Start();                          //起始信号
  ADXL345_SendByte(SlaveAddress);           //发送设备地址+写信号
  ADXL345_SendByte(0x32);                   //发送存储单元地址，从0x32开始
  ADXL345_Start();                          //起始信号
  ADXL345_SendByte(SlaveAddress + 1);       //发送设备地址+读信号
  for (i = 0; i < 6; i++)                  //连续读取6个地址数据，存储中BUF
  {
    BUF[i] = ADXL345_RecvByte();          //BUF[0]存储0x32地址中的数据
    if (i == 5)
    {
      ADXL345_SendACK(1);                //最后一个数据需要回NOACK
    }
    else
    {
      ADXL345_SendACK(0);                //回应ACK
    }
  }
  ADXL345_Stop();                          //停止信号
  Delay5ms();
}


//*****************************************************************

//初始化ADXL345，根据需要请参考pdf进行修改************************
void Init_ADXL345()
{
  Single_Write_ADXL345(0x31, 0x0B);  //测量范围,正负16g，13位模式
  Single_Write_ADXL345(0x2C, 0x08);  //速率设定为12.5 参考pdf13页
  Single_Write_ADXL345(0x2D, 0x08);  //选择电源模式   参考pdf24页
  Single_Write_ADXL345(0x2E, 0x80);  //使能 DATA_READY 中断
  Single_Write_ADXL345(0x1E, 0x00);  //X 偏移量 根据测试传感器的状态写入pdf29页
  Single_Write_ADXL345(0x1F, 0x00);  //Y 偏移量 根据测试传感器的状态写入pdf29页
  Single_Write_ADXL345(0x20, 0x05);  //Z 偏移量 根据测试传感器的状态写入pdf29页
}
//***********************************************************************
//显示x轴
void display_x()
{ float temp;
  dis_data = (BUF[1] << 8) + BUF[0]; //合成数据
  if (dis_data < 0) {
    dis_data = -dis_data;
    DisplayOneChar(10, 0, '-');      //显示正负符号位
  }
  else DisplayOneChar(10, 0, ' ');   //显示空格

  temp = (float)dis_data * 3.9;      //计算数据和显示,查考ADXL345快速入门第4页
  conversion(temp);                  //转换出显示需要的数据
  DisplayOneChar(8, 0, 'X');
  DisplayOneChar(9, 0, ':');
  DisplayOneChar(11, 0, qian);
  DisplayOneChar(12, 0, '.');
  DisplayOneChar(13, 0, bai);
  DisplayOneChar(14, 0, shi);
  DisplayOneChar(15, 0, ' ');
}

//***********************************************************************
//显示y轴
void display_y()
{ float temp;
  dis_data = (BUF[3] << 8) + BUF[2]; //合成数据
  if (dis_data < 0) {
    dis_data = -dis_data;
    DisplayOneChar(2, 1, '-');       //显示正负符号位
  }
  else DisplayOneChar(2, 1, ' ');    //显示空格

  temp = (float)dis_data * 3.9;      //计算数据和显示,查考ADXL345快速入门第4页
  conversion(temp);                  //转换出显示需要的数据
  DisplayOneChar(0, 1, 'Y');         //第1行，第0列 显示y
  DisplayOneChar(1, 1, ':');
  DisplayOneChar(3, 1, qian);
  DisplayOneChar(4, 1, '.');
  DisplayOneChar(5, 1, bai);
  DisplayOneChar(6, 1, shi);
  DisplayOneChar(7, 1, ' ');
}

//***********************************************************************
//显示z轴
void display_z()
{
  float temp;
  dis_data = (BUF[5] << 8) + BUF[4]; //合成数据
  if (dis_data < 0) {
    dis_data = -dis_data;
    DisplayOneChar(10, 1, '-');      //显示负符号位
  }
  else DisplayOneChar(10, 1, ' ');   //显示空格

  temp = (float)dis_data * 3.9;      //计算数据和显示,查考ADXL345快速入门第4页
  conversion(temp);                  //转换出显示需要的数据
  DisplayOneChar(8, 1, 'Z');         //第0行，第10列 显示Z
  DisplayOneChar(9, 1, ':');
  DisplayOneChar(11, 1, qian);
  DisplayOneChar(12, 1, '.');
  DisplayOneChar(13, 1, bai);
  DisplayOneChar(14, 1, shi);
  DisplayOneChar(15, 1, ' ');
}
//整合xyz轴的数据,正整数，返回 x*x+y*y+z*z	的计算值
float ADXL345_xyz()
{
  float temp;
  float all = 0.0;
  dis_data = (BUF[1] << 8) + BUF[0]; //合成数据
  if (dis_data < 0)  dis_data = -dis_data;
  temp = (float)dis_data / 10;
  all += temp * temp;

  dis_data = (BUF[3] << 8) + BUF[2]; //合成数据
  if (dis_data < 0)  dis_data = -dis_data;
  temp = (float)dis_data / 10;
  all += temp * temp;

  dis_data = (BUF[5] << 8) + BUF[4]; //合成数据
  if (dis_data < 0)  dis_data = -dis_data;
  temp = (float)dis_data / 10;
  all += temp * temp;
  return all;
}
//获取2次数据作为当前的获得数据，多次数据取平均值作为判断步数的阈值，与当前获得数据比较 ，保存总步数到eeprom
void calculate_xyz()
{
  float temp;
  temp = ADXL345_xyz();
  delay(20);
  Multiple_Read_ADXL345();
  temp = (temp + ADXL345_xyz()) / 2; //两次值取平均

  uart_send((uint)temp);        //串口发送获得的数据
  uart_send_char('\n');

  average_all_temp += (uint)(temp / average_all_times);
  average_all_num++;
  if (average_all_num >= average_all_times) {        //达到average_all_times次数  数据的平局作为阈值判断
    average_all_num = 0;
    average_all = average_all_temp;
    average_all_temp = 0;
  }

  if (temp > average_all + 55) {			  //当测量值 比阈值+55大，说明走了一步
    step++;
    step_all++;
    delay(150);								 //延时，避免一步当做多步
  }

  if (step_all % 10 == 0) {					 //当总步数是10的倍数时，把数据保存在eeprom中，
    Sector_erase(0x2000);
    eeprom_write(0x2000, step_all / 10000 % 10);
    eeprom_write(0x2001, step_all / 1000 % 10);
    eeprom_write(0x2002, step_all / 100 % 10);
    eeprom_write(0x2003, step_all / 10 % 10);
    eeprom_write(0x2004, step_all % 10);
  }
}
void clear_lcd()         //清空lcd显示内容
{
  int i;
  for (i = 0; i < 15; i++)  DisplayOneChar(i, 0, ' ');
  for (i = 0; i < 15; i++)  DisplayOneChar(i, 1, ' ');
}
//*********************************************************
//******主程序********
//*********************************************************
void main()
{
  uchar devid;
  uint i;
  uint cal_temp = 0;
  delay(100);                     //上电延时
  clear_all = 1;
  clear_now = 1;
  Show_other = 1;
  InitLcd();                      //液晶初始化ADXL345
  uart_init();					  //串口初始化  9600
  //从eeprom中获取之前保存的总步数
  step_all = eeprom_read(0x2000) * 10000 + eeprom_read(0x2001) * 1000 + eeprom_read(0x2002) * 100 + eeprom_read(0x2003) * 10 + eeprom_read(0x2004);

  Init_ADXL345();                         //初始化ADXL345
  devid = Single_Read_ADXL345(0X00);      //读出的数据为0XE5,表示正确
  delay(100);
  for (i = 0; i < average_all_times; i++) //初始化  计算合加速度数据的阈值
  {
    Multiple_Read_ADXL345();
    average_all_temp += (uint)(ADXL345_xyz() / average_all_times);
    average_all_num++;
    if (average_all_num >= average_all_times) {  //达到次数  所测的数据平均值作为阈值
      average_all_num = 0;
      average_all = average_all_temp;
      average_all_temp = 0;
    }
    delay(80);
  }
  while (1)                         //循环
  {
    Multiple_Read_ADXL345();        //连续读出数据，存储在BUF中


    calculate_xyz();    //计算阈值、步数、存储
    if (Show_other == 0) {			//切换显示共4种
      delay(15);
      if (Show_other == 0) {
        clear_lcd();
        Show_other_flag++;
        if (Show_other_flag >= 4)   Show_other_flag = 0;
		while(Show_other==0);
      }
    }
    if (clear_all == 0) {			//清除总步数
      delay(15);
      if (clear_all == 0) {
        step_all = 0;
		Sector_erase(0x2000);		//数据清0  在eeprom中
        eeprom_write(0x2000, 0);	
        eeprom_write(0x2001, 0);
        eeprom_write(0x2002, 0);
        eeprom_write(0x2003, 0);
        eeprom_write(0x2004, 0);
		while(clear_all==0);
      }
    }
    if (clear_now == 0) {			//清除当前步数值
      delay(15);
      if (clear_now == 0){ step = 0;while(clear_now==0);	 }
    }



    if (Show_other_flag == 0) {		 //显示模式0   显示当前步数和总步数
      DisplayOneChar(0, 0, 'S');
      DisplayOneChar(1, 0, 't');
      DisplayOneChar(2, 0, 'e');
      DisplayOneChar(3, 0, 'p');
      DisplayOneChar(4, 0, ':');
      conversion(step);
      DisplayOneChar(5, 0, wan);
      DisplayOneChar(6, 0, qian);
      DisplayOneChar(7, 0, bai);
      DisplayOneChar(8, 0, shi);
      DisplayOneChar(9, 0, ge);

      DisplayOneChar(0, 1, 'A');
      DisplayOneChar(1, 1, 'l');
      DisplayOneChar(2, 1, 'l');
      DisplayOneChar(3, 1, ' ');
      DisplayOneChar(4, 1, 'S');
      DisplayOneChar(5, 1, 't');
      DisplayOneChar(6, 1, 'e');
      DisplayOneChar(7, 1, 'p');
      DisplayOneChar(8, 1, ':');
      conversion(step_all);
      DisplayOneChar(9, 1, wan);
      DisplayOneChar(10, 1, qian);
      DisplayOneChar(11, 1, bai);
      DisplayOneChar(12, 1, shi);
      DisplayOneChar(13, 1, ge);
    }

    if (Show_other_flag == 1) {			 //显示模式1   显示当前路程和总路程
      cal_temp = step * 0.6; //路程
      conversion(cal_temp);
      DisplayOneChar(0, 0, 'N');
      DisplayOneChar(1, 0, '0');
      DisplayOneChar(2, 0, 'w');
      DisplayOneChar(3, 0, ':');
      DisplayOneChar(4, 0, wan);
      DisplayOneChar(5, 0, qian);
      DisplayOneChar(6, 0, bai);
      DisplayOneChar(7, 0, shi);
      DisplayOneChar(8, 0, ge);
      DisplayOneChar(9, 0, 'm');

      cal_temp = step_all * 0.6; //路程
      conversion(cal_temp);
      DisplayOneChar(0, 1, 'A');
      DisplayOneChar(1, 1, 'l');
      DisplayOneChar(2, 1, 'l');
      DisplayOneChar(3, 1, ':');
      DisplayOneChar(4, 1, wan);
      DisplayOneChar(5, 1, qian);
      DisplayOneChar(6, 1, bai);
      DisplayOneChar(7, 1, shi);
      DisplayOneChar(8, 1, ge);
      DisplayOneChar(9, 1, 'm');
    }

    if (Show_other_flag == 2) {			   //显示模式2   显示当前消耗能量和总消耗能量
      cal_temp = step / 20; //消耗能量
      conversion(cal_temp);
      DisplayOneChar(0, 0, 'N');
      DisplayOneChar(1, 0, '0');
      DisplayOneChar(2, 0, 'w');
      DisplayOneChar(3, 0, ':');
      DisplayOneChar(4, 0, wan);
      DisplayOneChar(5, 0, qian);
      DisplayOneChar(6, 0, bai);
      DisplayOneChar(7, 0, shi);
      DisplayOneChar(8, 0, ge);
      DisplayOneChar(9, 0, 'c');
      DisplayOneChar(10, 0, 'a');
      DisplayOneChar(11, 0, 'l');

      cal_temp = step_all / 20; //消耗能量
      conversion(cal_temp);
      DisplayOneChar(0, 1, 'A');
      DisplayOneChar(1, 1, 'l');
      DisplayOneChar(2, 1, 'l');
      DisplayOneChar(3, 1, ':');
      DisplayOneChar(4, 1, wan);
      DisplayOneChar(5, 1, qian);
      DisplayOneChar(6, 1, bai);
      DisplayOneChar(7, 1, shi);
      DisplayOneChar(8, 1, ge);
      DisplayOneChar(9, 1, 'c');
      DisplayOneChar(10, 1, 'a');
      DisplayOneChar(11, 1, 'l');
    }

    if (Show_other_flag == 3) {		   //显示模式3   显示当xyz测量值  以及x、y、z单个轴的加速度值
      conversion(ADXL345_xyz());
      DisplayOneChar(0, 0, 'x');
      DisplayOneChar(1, 0, 'y');
      DisplayOneChar(2, 0, 'z');
      DisplayOneChar(3, 0, ':');
      DisplayOneChar(4, 0, qian);
      DisplayOneChar(5, 0, bai);
      DisplayOneChar(6, 0, shi);
      DisplayOneChar(7, 0, ge);
	  display_x();                    //---------显示X轴
      display_y();                    //---------显示Y轴
      display_z();                    //---------显示Z轴
	}
    delay(100);                     //延时
  }
}
