#include  <REG51.H>
#include  <INTRINS.H>
#include  <stdio.h>
#include "uart.h"
#include "eeprom_isp.h"
#include "lcd1602.h"
#include "ds1302.h"

#define   uchar unsigned char
#define   uint unsigned int
//引脚设置
sbit key1_on =  P3 ^ 4 ;
sbit key1_off = P3 ^ 5 ;
sbit key2_on =  P3 ^ 6 ;
sbit key2_off = P3 ^ 7 ;
sbit relay1 =   P2 ^ 7 ;
sbit relay2 =   P2 ^ 6 ;

uchar sec, min, hour, day, month, year; //对应ds1302读取的时间变量
uchar RXD_num = 0;          //串口读取的字符数  用于判断长命令的
uchar uart_flag = 0;          //串口读取长数据 是否完成
uchar uart_data[20];          //串口数据存放的数组

void get_showlcd();	  //获取ds1302的时间参数 并且显示在lcd上

//主函数：
void main()
{
  uint i;
  uint tuntimes = 0;

  key1_on = 1;
  key1_off = 1;
  key2_on = 1;
  key2_off = 1;
  relay1 = 1;
  relay2 = 1;

  uart_init();            //串口初始化  波特率 9600
  lcd_init();             //lcd1602初始化
  ds1302_init();          //ds1302时钟模块的初始化

  delay_ms(100);
  write_com(0x80);        //lcd第一行位置
  //ds1302_reset();       //ds1302时间重设
  //write_com(0xc0);      //lcd第二行位置
  //print_string("Hello");//lcd显示内容

  while (1)        //大循环
  {
    tuntimes++;
    if (tuntimes >= 40) {  //整体运行40次  之后再执行里边的内容  避免刷新频率过高
      tuntimes = 0;
      get_showlcd();   //获取ds1302的时间参数 并且显示在lcd上
    }
    if (uart_flag == 1) {	 //如果数据已经接收完成
      if (uart_data[13] == '*') {		   //如果第14个是‘*’ 表示修改ds1302时间  数据格式举例：“#200514120000”
        ds1302_write(write_protect, 0x00);   //禁止写保护，允许写入数据
        ds1302_write(write_second, (uart_data[11] - 48) * 16 + uart_data[12] - 48); 
        ds1302_write(write_minute, (uart_data[9] - 48) * 16 + uart_data[10] - 48); 
        ds1302_write(write_hour, (uart_data[7] - 48) * 16 + uart_data[8] - 48); 
        ds1302_write(write_day, (uart_data[5] - 48) * 16 + uart_data[6] - 48);
        ds1302_write(write_month, (uart_data[3] - 48) * 16 + uart_data[4] - 48);
        ds1302_write(write_year, (uart_data[1] - 48) * 16 + uart_data[2] - 48);
        ds1302_write(write_protect, 0x80);   //允许写保护，禁止写入数据
        uart_send_str("change time ok!");
      }
      if (uart_data[2] == '*') {	 //如果数据第三个结束  表示控制继电器
        if (uart_data[1] == '1')    relay1 = 0;	   //“#1*” 打开继电器1
        if (uart_data[1] == '2')    {			   //“#2*” 都打开
          relay1 = 0;
          relay2 = 0;
        }
        if (uart_data[1] == '3')     relay2 = 0;   //“#3*” 打开继电器2
        if (uart_data[1] == '4')   {			   //“#4*” 都关闭
          relay1 = 1;
          relay2 = 1;
        }
      }
      if (uart_data[1] == '@') {				  //类似“#@hello*”表示在lcd第二行显示hello
        write_com(0xc0);
        for (i = 0; i < 15; i++)  write_data(' ');
        write_com(0xc0);
        for (i = 2; i < 20; i++) {
          if (uart_data[i] != '*') write_data(uart_data[i]);
          else break;
        }
      }
      for (i = 0; i < 20; i++) uart_data[i] = 0;   //清空接收数组缓存
      uart_flag = 0;
    }


    if (key1_on == 0) {			//按键判断  跟串口的一致
      delay_ms(20);
      if (key1_on == 0)      relay1 = 0;
    }
    if (key1_off == 0) {
      delay_ms(20);
      if (key1_off == 0)   {
        relay1 = 0;
        relay2 = 0;
      }
    }
    if (key2_on == 0) {
      delay_ms(20);
      if (key2_on == 0)    relay2 = 0;
    }
    if (key2_off == 0) {
      delay_ms(20);
      if (key2_off == 0)    {
        relay1 = 1;
        relay2 = 1;
      }
    }


    delay_ms(10);
  }
}


void RX_int(void) interrupt 4     //串口接收中断服务函数   存放接收到的数据
{
  uchar data1;
  if (RI)	   //串口中断
  {
    RI = 0;
    data1 = SBUF;
    if (data1 == '#')  RXD_num = 0;	  //数据以‘#’开头 ，以‘*’结尾，不符合的不存放在数组里
    uart_data[RXD_num] = data1;
    RXD_num++;
    if (data1 == '*') {			//判断结尾
      RXD_num = 0;
      uart_flag = 1;			//完成数据接收 标志
      uart_send_str("get:");	//发回数据，表示收到，以便验证
      uart_send_str(uart_data);
    }
  }
}
void get_showlcd(){			  //获取ds1302的时间参数 并且显示在lcd上
      sec = ds1302_read(read_second);
      min = ds1302_read(read_minute);
      hour = ds1302_read(read_hour);
      day = ds1302_read(read_day);
      month = ds1302_read(read_month);
      year = ds1302_read(read_year);

      write_com(0x80);
      write_data(month / 10 % 10 + 48);
      write_data(month % 10 + 48);
      write_data('-');
      write_data(day / 10 % 10 + 48);
      write_data(day % 10 + 48);
      write_data(' ');
      write_data(hour / 10 % 10 + 48);
      write_data(hour % 10 + 48);
      write_data(':');
      write_data(min / 10 % 10 + 48);
      write_data(min % 10 + 48);
      write_data(':');
      write_data(sec / 10 % 10 + 48);
      write_data(sec % 10 + 48);
}