板子自带4个按键，可以分别控制开一个、开另一个、两个全开、两个全关。lcd1602上显示当前实时时间

！！板子上到的蓝牙需要正确接上，不可反接！！
手机上需要下载 蓝牙调试助手  “SPP”
打开手机蓝牙，配对蓝牙“happy1”  密码1234
打开SPP软件，点击连接，选happ1

手机设置按钮可以实现控制：

发送#1*    打开左边继电器
#2*     打开全部
#3*     打开右边
#4*     关闭全部

发送#@nihao*     将会在lcd上的第二行显示nihao    可以显示任意英文字母

发送#200515123022*
实现对显示的时间修改为   2020年5月15日12点30分22秒
并且储存在ds1302里，下次接电，lcd显示的时间就是修改后的实时时间