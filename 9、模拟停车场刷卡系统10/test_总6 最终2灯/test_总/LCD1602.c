/*------------------------------------------------------------------*-

   LCD1232.c (v1.00)

  ------------------------------------------------------------------
   
   'C Source code' for project LCD12232.

   COPYRIGHT
   ---------

   This code is copyright (c) 2004 by lansy.
 
   Send email to lansy1@163.com for copyright details and other information.
   All rights reserved.

-*------------------------------------------------------------------*/

#include "LCD1602.h"

static bit   LCDisBusy();
static void  SendToLCD(const uchar, bit flag);
/*------------------------------------------------------------------*-

  FUNCTION: DispStr()

  Display string on LCM screen (via pointer)
  - edit as required to meet the needs of your application.

  HARDWARE: Please refer to Head file "port.h"

  GLOBALS:  -

  PARAMS:   *str,x,y 

  RETURNS:  void

-*------------------------------------------------------------------*/
#if ( DISPSTRAT_ENB )
// if enable dispstr,IDE will compile the function below
void DispStrAt(uchar *pStr, uchar data x, uchar data y)
{   
    SetCursorPos(x, y);                        
    DispStr(pStr);
}
#endif

#if (DISPSTRAT_ENB||DISPSTR_ENB)
void DispStr(uchar *pStr)
{
	while(*pStr != '\0')
    {   
        //DAT imply what we send is data
        SendToLCD(*pStr++, DAT);        
    }
}
#endif
/*
---------------------------------------------------------------------------

  FUNCTION:  DispNum()

  Display number, e.g 345,this function will divide it into 3,4,5.and then 
  construct them as a string to send to display  
---------------------------------------------------------------------------
*/
#if ( DISPDECSPACE_ENB|| DISPDECSPACEAT_ENB )  
// if enable dispnum,IDE will compile the function below
void DispDecSpace(ulong Num,uchar Len)
{
	uchar i = 9;
	uchar vec[10] = {' ', ' ', ' ', ' ', ' ', ' ', ' ',' ', ' ', ' '};
	
	vec[9] = '\0';
 
	do
	{
		vec[--i] = Num%10 + '0';
		Num /= 10;
	}
	while(Num);
	Len = 9-Len;
	DispStr(vec+Len);
}
#endif

#if (DISPDECSPACE_ENB||DISPDECSPACEAT_ENB)
void DispDecSpaceAt(ulong Num, uchar x, uchar y,uchar Len)
{   
	SetCursorPos(x, y);
	DispDecSpace(Num, Len);
}
#endif

#if (DISPDECSHIFTAT_ENB)
void DispDecShiftAt(ulong Num, uchar x, uchar y,uchar DotPos)
{
	SetCursorPos(x, y);
	DispDecShift(Num, DotPos);
}
#endif

#if (DISPDECSHIFT_ENB||DISPDECSHIFTAT_ENB)
void DispDecShift(ulong Num, uchar DotPos)
{
	uchar i = 9;
	uchar vec[10];

	vec[9] = '\0';

	do
	{
	    if ( i == 9 - DotPos && DotPos)
		{
           vec[--i] = '.';
		}
		vec[--i] = Num%10 + '0';
		Num /= 10;
	}
	while(Num);

	while(DotPos >= 9-i)
	{
		if (DotPos == 9 - i)
		{
			vec[--i]   = '.';
		}
		vec[--i] = '0';
	}

	DispStr(vec+i);
}

#endif

/*----------display single charactor------------------------*/
#if ( DISPCHAR_ENB )                           // if enable dispchar,IDE will.. 
                                               //... compile the function below
void DispChar(uchar data ch)
{  
   SendToLCD(ch, DAT);
}
#endif

#if ( DISPCHARAT_ENB )
void DispCharAt(uchar ch,uchar x, uchar y)
{
	SetCursorPos(x, y);
	SendToLCD(ch, DAT);
}
#endif

/*-----------Set cursor's position-------------------*/
void SetCursorPos(const uchar data x, const uchar y)
{   
	uchar addr;

    addr = 0x80 + x + y*64;
    SendToLCD(addr, CMD);  
}

#if (SET_CURSOR_ON_ENB)
void SetCursorOn(bit On)
{
    if (On)
	{
		SendToLCD(0x0E,CMD);
	}
	else
	{
		SendToLCD(0x0C,CMD);
	}
}

#endif

/*-----------Intilize LCM-----------------------------*/ 
void InitLCD()                                      
{   
    #ifdef  FUNCTION 
    	    SendToLCD(FUNCTION, CMD);             // Function setting,
	#endif 

	#ifdef  DISPLAY_ON_OFF
            SendToLCD(DISPLAY_ON_OFF, CMD);
    #endif
     
	#ifdef  ENTRY_MODE
            SendToLCD(ENTRY_MODE, CMD);
    #endif 

	#ifdef DISPLAY_CURSOR_SHIFT
           SendToLCD(DISPLAY_CURSOR_SHIFT, CMD);
    #endif

	#ifdef CGRAM_ADDR
	       SendToLCD(CGRAM_ADDR, CMD)
	#endif    

    EraseLCD();    
}

/*-------- Clear screen------------------------------*/
void EraseLCD()
{ 
	SendToLCD(0x01,CMD); 
}   

/*---------Send data or command to LCD---------------*/
void SendToLCD(const uchar data val, bit flag) 
{   
    bit EAFlag;
	uchar timeout = 0xef;

    EAFlag = EA;
	EA = 0; 
                            
    while(LCDisBusy() && --timeout);               
    RS_LCD = flag;                 
    RW_LCD = 0;                  
    DB_LCD = val;                 
    E_LCD  = 1;                   
    E_LCD  = 0;

	EA = EAFlag;
}

/*---------Get LCM Info--------------------------------*/
uchar LCDInfo()                              
{  
    uchar info = 0;

    DB_LCD = 0xff;
    RS_LCD = 0;
    RW_LCD = 1;
    E_LCD  = 1;
    info   = DB_LCD;                         
    E_LCD  = 0;

    return info;
}

/*------------return LCM info------------------------------*/
bit LCDisBusy()
{ 
	return LCDInfo()&0x80 ;
}    // 返回忙标志

/*------------return cursor's current position--------------*/ 
#if ( LCDADDR_ENB )
uchar LCDaddr()
{ 
	return LCDInfo()&0xef ;
}    // 返回光标地址
#endif

/*------------------------------------------------------------------*-
  ---- END OF FILE -------------------------------------------------
-*------------------------------------------------------------------*/

