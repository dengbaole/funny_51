
#ifndef __MFRC500_H__
#define __MFRC500_H__

extern uchar idata	UID[5];                 /* ���к� */
void Init_FM1715(void);
unsigned char MIF_Halt(void);
unsigned char Request(unsigned char mode);
unsigned char AntiColl(void);
unsigned char Select_Card(void);
unsigned char Load_keyE2_CPY(unsigned char *uncoded_keys);
uchar Authentication(uchar idata *UID, uchar SecNR, uchar mode);
uchar MIF_READ(uchar idata *buff, uchar Block_Adr);
uchar MIF_Write(uchar idata *buff, uchar Block_Adr);

#endif