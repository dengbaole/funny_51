
#include"keyScan.h"	


/*
*********************************************************************************************************
*	函 数 名: DelayMs
*	功能说明: 时钟延时
*	形    参: ms 以毫秒为单位.
*	返 回 值: 无
*********************************************************************************************************
*/
void Delay_Ms( unsigned int ms)
{
	unsigned int i,j;
	for (i = 0; i < ms; i++)
		for(j=0;j<124;j++);
}

	   
//4X4键盘扫描程序
unsigned char keyScan(void) {
 
  unsigned char key=16;
  unsigned char temp,temp1,temp2;
  
  KEY_PORT=0XF0;
  
  if( (KEY_PORT& 0XF0) !=0XF0) {  
    Delay_Ms(50);
     if( (KEY_PORT & 0XF0) !=0XF0) {
       temp1= KEY_PORT & 0XF0;
          KEY_PORT=0X0f;
          Delay_Ms(50);
      temp2= KEY_PORT & 0X0f; 
        
      while((KEY_PORT & 0X0f) !=0X0f);//等待按键释放
      
      
      temp=temp1|temp2;
      
      switch(temp) {
         case 0xe7:key=1;break;
         case 0xd7:key=2;break;
         case 0xb7:key=3;break;
         case 0x77:key=4;break;
         
         case 0xeb:key=5;break;
         case 0xdb:key=6;break;
         case 0xbb:key=7;break;
         case 0x7b:key=8;break;  
                
        case 0xed:key=9;break;
         case 0xdd:key=0;break;
         case 0xbd:key=10;break;
         case 0x7d:key=11;break;        

        case 0xee:key=12;break;
         case 0xde:key=13;break;
         case 0xbe:key=14;break;
         case 0x7e:key=15;break; 
         
        default:break;
                
      }    
     } 
  }
  return key; 
  
}