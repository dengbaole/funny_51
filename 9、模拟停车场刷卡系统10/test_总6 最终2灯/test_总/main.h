
/*------------------------------------------------------------------*-

   Main.H (v1.00)

  ------------------------------------------------------------------
   
   'Project Header' 

   COPYRIGHT
   ---------

   This code is associated with the Alarm Clock

   This code is copyright by Lansy.

-*------------------------------------------------------------------*/
#ifndef _MAIN_H
#define _MAIN_H

//------------------------------------------------------------------
// WILL NEED TO EDIT THIS SECTION FOR EVERY PROJECT!!!!!!!!
//------------------------------------------------------------------

// Must include the appropriate microcontroller header file here

#include "reg52.h"
#include <intrins.h>
//#include <absacc.h>

// Oscillator / resonator frequency (in Hz) e.g. (11059200UL)
#define OSC_FREQ1 (12000000UL)


// Number of oscillations per instruction (12, etc)
#define OSC_PER_INST (12)

//------------------------------------------------------------------
// SHOULD NOT NEED TO EDIT THE SECTIONS BELOW
//------------------------------------------------------------------




// Type define   
typedef unsigned char uchar;
typedef unsigned int  uint;
typedef unsigned long ulong;

typedef unsigned int  tWord;
typedef unsigned char tByte;
typedef unsigned long tLong;
// Interrupts
  
#define INT_External0   0
#define INT_T0_Overflow 1 
#define INT_External1   2
#define INT_T1_Overflow 3
//#define INT_S
#define INT_T2_Overflow 5

#define DELAY_2us()  _nop_(); _nop_();
#define DELAY_4us()  DELAY_2us();DELAY_2us()
#define DELAY_10us() DELAY_4us();DELAY_4us();DELAY_2us()
#define DELAY_8us()  DELAY_4us();DELAY_4us()
#endif

/*------------------------------------------------------------------*-
  ---- END OF FILE -------------------------------------------------
-*------------------------------------------------------------------*/