//#include <string.h>
//#include <stdio.h>
//#include <absacc.h>
//#include <intrins.h>

#include "FM1702.h"
#include "reg52.h"
#include "main.h"
#include "spi.h"

void delay1(uint dlength)
{ 
    uint i;
    for (i=0;i<dlength;i++)
    {
       _nop_();
    }
}

unsigned char rev(uchar tem)
{
    uchar idata i;
	unsigned char idata var=0,var_snt=0;
	
	var=0;
	for (i=0;i<8;i++)          // 8位字节带进位左移
	{ 
		var_snt=tem&0x80;            // 选择高位
		if (var_snt==0x80) si=1;     // 输出高位
		else si=0;	
		tem<<=1;	
		var<<=1;
		CY=so;
	    if (CY==1) 
			var+=1;         // 带进位左移
		sck=1;                 // 产生SCK脉冲	
		delay1(1);		
		sck=0;	 
    }	
	return(var);          // 返回
} 

void Send(unsigned char var) 

{ 
	unsigned char data i,tem;
	 
	for (i=0;i<8;i++)          		// 8位字节输出
	{
		delay1(1);	                  // 使SCK为低电平
		tem=var&0x80;            // 选择高位
		if (tem==0x80) si=1;     // 输出高位
      		else si=0;
      		delay1(1);
		sck=1;                   // 使SCK为1
		var<<=1;	
		delay1(1);	
		sck=0; 							// 左移1位
	}  
}                  

uchar read_reg(uchar idata SpiAddress)
{
	uchar idata rdata;
	SpiAddress=SpiAddress<<1;
	SpiAddress=SpiAddress | 0x80; 
	spi_cs=0;	
	Send(SpiAddress);
	rdata=rev(0);
	spi_cs=1;		
	return(rdata);
}

#if 0
void read_mult_dat(uchar idata SpiAddress,uchar *dat,uchar n )
{
	uchar i;
	SpiAddress=SpiAddress<<1;
	SpiAddress=SpiAddress | 0x80; 
	spi_cs=0;		
	Send(SpiAddress);
	for(i=0;i<n-1;i++)
		dat[i]=rev(SpiAddress);
	dat[n-1]=rev(0);		
	spi_cs=1;		
}

void write_mult_dat(uchar idata SpiAddress,uchar *dat,uchar n)
{
	uchar i;
	SpiAddress=SpiAddress<<1;
	SpiAddress=SpiAddress &0x7f;
	spi_cs=0; 
	so=0;
	Send(SpiAddress);	
	for(i=0;i<n;i++)
	{
		Send(dat[i]);
	}
	spi_cs=1;	
}
#endif

void write_reg(uchar idata SpiAddress,uchar dat)
{
	SpiAddress=SpiAddress<<1;
	SpiAddress=SpiAddress &0x7f;
	spi_cs=0; 
	so=0;
	Send(SpiAddress);		
	Send(dat);	
	spi_cs=1;
}