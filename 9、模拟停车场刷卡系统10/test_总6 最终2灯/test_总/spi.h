
#ifndef __SPI_H__
#define __SPI_H__


extern unsigned char read_reg(unsigned char idata SpiAddress);
extern void write_reg(unsigned char idata SpiAddress,unsigned char dat);
extern void delay1(unsigned int dlength);

#endif